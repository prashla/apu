%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ENV init
clear;
rng('default');
rng('shuffle');
h = Environment2;
n = h.getNumArms();              % / total number of arms 
w = @weightFunction2;              % weight function for cost distortion
[bestArm, bestValue] = h.getBestArm(@(x)x);
[bestWeightedArm, bestWeightedValue] = h.getBestArm(w);
% horizon = [10, 1e2, 3e2, 5e2, 1e3, 3e3, 5e3, 1e4, 3e4, 5e4];
% horizon = 1e2:5e2:1e4;
horizon = [1e3];
numTrialsPerHorizon = 1;
regretResult = zeros(numTrialsPerHorizon, length(horizon));
regretWeightedResult = zeros(numTrialsPerHorizon, length(horizon));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end init


horizonIndex = 1;
for max_trial = horizon
    for trialnum = 1:numTrialsPerHorizon
        fprintf('Horizon %d \t Trial %d / %d', max_trial, trialnum, numTrialsPerHorizon);
        timerVal = tic;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ALG init
        
        alpha = 0.6;                        % Holder continuity exponent of weight function used by alg
        L = 1;                              % Holder continuity scaling of weight function used by alg
        M = 50;                             % bound on cost function used by alg
        T_arm = zeros(1,n);                 % number of times arm i is played
        pastCosts = zeros(n, max_trial);  % history of costs for arms
        netCost = 0;
        netCostInExpectation = 0;
        netWeightedCost = 0;
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end init
        for T=1:max_trial
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ALG decision
            if T<=n
                armToPlay = T;
            else
                % compute B-values for arms
                Bvalue = zeros(1,n);
                for i=1:n
                    Desorted= sort(pastCosts(i,1:T_arm(i)));
                    for j=1:T_arm(i)-1 % Estimating the distorted integral
                        Bvalue(i)=Bvalue(i) + Desorted(j)*( w( (T_arm(i)-j)/T_arm(i))- w((T_arm(i)-1-j)/T_arm(i)) );
                    end
                end
                Bvalue = Bvalue - (12*(L^2)*(M^2)*reallog(T)./T_arm).^(alpha/2);
                [bestBvalue,idx] = min(Bvalue) ;                     % choose arm that maximizes the B-value
                armToPlay = idx;
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end decision
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ENV generate cost, action: armToPlay
            [cost, costInExpectation, weightedCost] = h.getCosts(armToPlay, w);
            netCost = netCost + cost;
            netCostInExpectation = netCostInExpectation + costInExpectation;
            netWeightedCost = netWeightedCost + weightedCost;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end generate cost
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ALG update
            
            T_arm(armToPlay)= T_arm(armToPlay)+1 ;                  % count of number of times arm i is played
            pastCosts(armToPlay, T_arm(armToPlay)) = cost;      % update observed history
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end update
        end
        
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ENV process results
        netRegret = -max_trial*bestValue + netCostInExpectation;
        netWeightedRegret = -max_trial*bestWeightedValue + netWeightedCost;
        regretResult(trialnum, horizonIndex) = netRegret; 
        regretWeightedResult(trialnum, horizonIndex) = netWeightedRegret;
        elapsedTime = toc(timerVal);
        fprintf('\t Time: %f s\n', elapsedTime);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end process results
    end
    horizonIndex = horizonIndex + 1;
end

regretResult;