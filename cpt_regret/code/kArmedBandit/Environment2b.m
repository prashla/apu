% Example 1, modified from tversky92cpt.pdf

function fh = Environment2b
fh.getNumArms = @getNumArms;
fh.getBestArm = @getBestArm;
fh.getCosts = @getCosts;
end

function n = getNumArms()
n = 2;
end

function [r, rE, wR] = getCosts(i, w)
if (i < 0) && (i > 2)
    r = -1;
    rE = -1;
    wR = -1;
    error('invalid arm index');
else
    if i == 1
        bit = rand <= 0.2;
        r = bit;
        rE = 0.2;
        wR = w(0.2);
    else 
        bit = rand <= 0.7;
        r = bit;
        rE = 0.7;
        wR = w(0.7);
    end        
end
end

function [i, r] = getBestArm(w)
m = getNumArms();
expCost = zeros(1,m);
for j = 1:m
    [r, rE, wR] = getCosts(j, w);
    expCost(j) = wR;
end
[r,i] = min(expCost);
end
