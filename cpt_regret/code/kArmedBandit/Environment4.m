% Example 4, modified from tversky92cpt.pdf

function fh = Environment4
fh.getEnvName = @getEnvName;
fh.getNumArms = @getNumArms;
fh.getBestArm = @getBestArm;
fh.getCosts = @getCosts;
fh.getMaxCost = @getMaxCost;
end

function s = getEnvName()
s = mfilename;
end


function n = getNumArms()
n = 2;
end

function [r, rE, wR] = getCosts(i, w)
if (i < 0) && (i > 2)
    r = -1;
    rE = -1;
    wR = -1;
    error('invalid arm index');
else
    if i == 1
        bit = rand <= 0.01;
        r = bit*100;
        rE = 1;
        wR = w(0.01)*100;
    else 
        bit = rand <= 0.2;
        r = bit*10;
        rE = 2;
        wR = w(0.2)*10;
    end        
end
end

function M = getMaxCost()
M = 100;
end

function [i, r] = getBestArm(w)
m = getNumArms();
expCost = zeros(1,m);
for j = 1:m
    [r, rE, wR] = getCosts(j, w);
    expCost(j) = wR;
end
[r,i] = min(expCost);
end
