function y = weightFunction2(x)
y = x.^(0.61)./(x.^(0.61)+ (1-x).^(0.61));
end