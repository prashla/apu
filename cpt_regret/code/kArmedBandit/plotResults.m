clear all;
close all;
figure('Color',[1.0 1.0 1.0]);
ax = axes();

load(strtrim(ls('weightedBanditUCB--Environment3--15-09-2016--03-33-07.mat')));
% A = shadedErrorBar(horizon, mean(regretWeightedResult, 1), std(regretWeightedResult, 1), 'r');
% A = errorbar(horizon, mean(regretWeightedResult, 1), std(regretWeightedResult, 1), 'r');
A = errorbar(horizon, mean(regretWeightedResult, 1), std(regretWeightedResult, 1)/sqrt(numTrialsPerHorizon), 'r-');
fileID = fopen('karmed-UCB.txt', 'w');
m = [horizon; mean(regretWeightedResult, 1); std(regretWeightedResult, 1)/sqrt(numTrialsPerHorizon)];
fprintf(fileID, '%.3f, %.3f, %3f\n', m);
fclose(fileID);

hold on;
load(strtrim(ls('weightedBanditWUCB2--Environment3--15-09-2016--07-21-45.mat')));
% B = shadedErrorBar(horizon, mean(regretWeightedResult, 1), std(regretWeightedResult, 1), 'b');
% B = errorbar(horizon, mean(regretWeightedResult, 1), std(regretWeightedResult, 1), 'b');
B = errorbar(horizon, mean(regretWeightedResult, 1), std(regretWeightedResult, 1)/sqrt(numTrialsPerHorizon), 'b');
fileID = fopen('karmed-WUCB.txt', 'w');
m = [horizon; mean(regretWeightedResult, 1); std(regretWeightedResult, 1)/sqrt(numTrialsPerHorizon)];
fprintf(fileID, '%.3f, %.3f, %3f\n', m);
fclose(fileID);

set(ax, 'XScale', 'linear');
% legend([A.mainLine,B.mainLine],'UCB','W-UCB');
legend('UCB','W-UCB');
xlabel('Rounds');
ylabel('Cumulative regret');
% title('Cumulative regret w.r.t. minimum cost distorted arm');
set(gca, 'fontsize', 20);
xlim([0,5e4]);
grid on;