function weightedBanditUCB(Environment)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ENV init
rng('default');
rng('shuffle');
randSeed = rng;
h = Environment;
n = h.getNumArms();              % / total number of arms 
w = @weightFunction2;              % weight function for cost distortion
[bestArm, bestValue] = h.getBestArm(@(x)x);
[bestWeightedArm, bestWeightedValue] = h.getBestArm(w);
% horizon = [10, 1e2, 3e2, 5e2, 1e3, 3e3, 5e3, 1e4, 3e4, 5e4];
horizon = 1e2:3e2:5e4;
% horizon = [1e4];
numTrialsPerHorizon = 100;
regretResult = zeros(numTrialsPerHorizon, length(horizon));
regretWeightedResult = zeros(numTrialsPerHorizon, length(horizon));
numPlaysArmOneResult = zeros(numTrialsPerHorizon, length(horizon));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end init


horizonIndex = 1;
for max_trial = horizon
    for trialnum = 1:numTrialsPerHorizon
        fprintf('Horizon %d \t Trial %d / %d', max_trial, trialnum, numTrialsPerHorizon);
        timerVal = tic;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ALG init
        
        M = h.getMaxCost();                             % upper bound on (nonnegative) costs
        T_arm = zeros(1,n);                 % number of times arm i is played
        Xi_t_prev = zeros(1,n);
        netCost = 0;
        netCostInExpectation = 0;
        netWeightedCost = 0;
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end init
        for T=1:max_trial
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ALG decision
            if T<=n
                armToPlay = T;
            else
                Lcb = Xi_t_prev./T_arm - M*sqrt((2*reallog(T))./T_arm);
                [bestLcb,idx] = min(Lcb) ;                     % choose arm that optimizes the UCB criterion
                armToPlay = idx;
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end decision
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ENV generate cost, action: armToPlay
            [cost, costInExpectation, weightedCost] = h.getCosts(armToPlay, w);
            netCost = netCost + cost;
            netCostInExpectation = netCostInExpectation + costInExpectation;
            netWeightedCost = netWeightedCost + weightedCost;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end generate cost
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ALG update
            
            T_arm(armToPlay)= T_arm(armToPlay)+1 ;                            % count of number of times arm i is played
            Xi_t_prev(armToPlay) = Xi_t_prev(armToPlay)+ cost ;             % update cost so far for arm i
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end update
        end
        
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ENV process results
        netRegret = -max_trial*bestValue + netCostInExpectation;
        netWeightedRegret = -max_trial*bestWeightedValue + netWeightedCost;
        regretResult(trialnum, horizonIndex) = netRegret;
        regretWeightedResult(trialnum, horizonIndex) = netWeightedRegret;
        numPlaysArmOneResult(trialnum, horizonIndex) = T_arm(1);
        elapsedTime = toc(timerVal);
        fprintf('\t Time: %f s\n', elapsedTime);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end process results
    end
    horizonIndex = horizonIndex + 1;
end

save([mfilename '--' h.getEnvName() '--' datestr(datetime('now'), 'dd-mm-yyyy--HH-MM-SS')]);
end