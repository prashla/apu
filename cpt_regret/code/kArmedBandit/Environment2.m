% Example 2, modified from tversky92cpt.pdf

function fh = Environment2
fh.getEnvName = @getEnvName;
fh.getNumArms = @getNumArms;
fh.getBestArm = @getBestArm;
fh.getCosts = @getCosts;
fh.getMaxCost = @getMaxCost;
end

function s = getEnvName()
s = mfilename;
end


function n = getNumArms()
n = 2;
end

function [r, rE, wR] = getCosts(i, w)
if (i < 0) && (i > 2)
    r = -1;
    rE = -1;
    wR = -1;
    error('invalid arm index');
else
    if i == 1
        bit = rand <= 0.1;
        r = bit*50;
        rE = 5;
        wR = w(0.1)*50;
    else 
        r = 7;
        rE = 7;
        wR = w(1.0)*7;
    end        
end
end

function M = getMaxCost()
M = 50;
end

function [i, r] = getBestArm(w)
m = getNumArms();
expCost = zeros(1,m);
for j = 1:m
    [r, rE, wR] = getCosts(j, w);
    expCost(j) = wR;
end
[r,i] = min(expCost);
end
