% Example 3, modified from tversky92cpt.pdf

function fh = Environment3
fh.getEnvName = @getEnvName;
fh.getNumArms = @getNumArms;
fh.getBestArm = @getBestArm;
fh.getCosts = @getCosts;
fh.getMaxCost = @getMaxCost;
end

function s = getEnvName()
s = mfilename;
end


function n = getNumArms()
n = 2;
end

function [r, rE, wR] = getCosts(i, w)
if (i < 0) && (i > 2)
    r = -1;
    rE = -1;
    wR = -1;
    error('invalid arm index');
else
    if i == 1
        bit = rand <= 0.01;
        r = bit*500;
        rE = 5;
        wR = w(0.01)*500;
    else 
        bit = rand <= 0.03;
        r = bit*250;
        rE = 7.5;
        wR = w(0.03)*250;
    end        
end
end

function M = getMaxCost()
M = 500;
end

function [i, r] = getBestArm(w)
m = getNumArms();
expCost = zeros(1,m);
for j = 1:m
    [r, rE, wR] = getCosts(j, w);
    expCost(j) = wR;
end
[r,i] = min(expCost);
end
