% test stochastic environment

function fh = Environment1
fh.getNumArms = @getNumArms;
fh.getReward = @getReward;
fh.getRewardInExpectation = @getRewardInExpectation;
fh.getBestArm = @getBestArm;
fh.getWeightedReward = @getWeightedReward;
end

function n = getNumArms()
n = 10;
end

function r = getReward(i)
if (i < 0) && (i > 10)
    r = -1;
    error('invalid arm index');
else
    p = [1:10]/11;
    r = binornd(1, p(i));
end
end

function eR = getRewardInExpectation(i)
if (i < 0) && (i > 10)
    eR = -1;
    error('invalid arm index');
else
    p = [1:10]/11;
    eR = p(i);
end
end

function i = getBestArm()
m = getNumArms;
expRew = zeros(1,m);
for j = 1:m
    expRew(j) = getRewardInExpectation(j);
end
[val,idx] = max(expRew);
i = idx;
end

function wR = getWeightedReward(i, w)
if (i < 0) && (i > 10)
    wR = -1;
    error('invalid arm index');
else
    p = [1:10]/11;
    wR = w(p(i));
end
end