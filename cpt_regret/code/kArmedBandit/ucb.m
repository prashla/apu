%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ENV init
clear;
rng('default');
rng('shuffle');
h = Environment1;
n = h.getNumArms();              % / total number of arms 
bestArm = h.getBestArm();
% horizon = [10, 1e2, 3e2, 5e2, 1e3, 3e3, 5e3, 1e4, 3e4, 5e4];
horizon = 1e2:4e2:1e4;
numTrialsPerHorizon = 50;
regretResult = zeros(numTrialsPerHorizon, length(horizon));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end init



horizonIndex = 1;
for max_trial = horizon
    for trialnum = 1:numTrialsPerHorizon
        fprintf('Horizon %d \t Trial %d / %d\n', max_trial, trialnum, numTrialsPerHorizon);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ALG init
        
        T_arm = zeros(1,n);                 % number of times arm i is played
        Xi_t_prev = zeros(1,n);
        netReward = 0;
        netRewardInExpectation = 0;
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end init
        for T=1:max_trial
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ALG decision
            if T<=n
                armToPlay = T;
            else
                Ucb = Xi_t_prev./T_arm + sqrt((2*reallog(T))./T_arm);
                [bestUcb,idx] = max(Ucb) ;                     % choose arm that maximizes the UCB criteria
                armToPlay = idx;
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end decision
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ENV generate reward, action: armToPlay
            reward = h.getReward(armToPlay);
            rewardInExpectation = h.getRewardInExpectation(armToPlay);
            netReward = netReward + reward;
            netRewardInExpectation = netRewardInExpectation + rewardInExpectation;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end generate reward
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ALG update
            
            T_arm(armToPlay)= T_arm(armToPlay)+1 ;                               % count of number of times arm i is played
            Xi_t_prev(armToPlay) = Xi_t_prev(armToPlay)+ reward ;             % update the reward so far for arm i
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end update
        end
        
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ENV process results
        netRegret = max_trial*h.getRewardInExpectation(bestArm) - netRewardInExpectation;
        regretResult(trialnum, horizonIndex) = netRegret;        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end process results
    end
    horizonIndex = horizonIndex + 1;
end

regretResult;