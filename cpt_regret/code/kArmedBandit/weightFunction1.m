function y = weightFunction1(x)
y = x.^(0.6)./(x.^(0.6)+ (1-x).^(0.6));
end