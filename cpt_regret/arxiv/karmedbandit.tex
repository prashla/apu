Suppose there are $K$ arms with unknown distributions $F_k, k=1,\ldots,K$. 
In each round $m=1,\ldots,n$, the algorithm pulls an arm $I_m \in \{1,\ldots,K\}$ and obtains a sample cost from the distribution $F_{I_m}$ of arm $I_m$. 
%We assume for simplicity that the costs are non-negative. 

The classic objective is to play (or pull) the arm whose expected cost is the least. In this paper, we take a different approach inspired by non-expected utility (EU) approaches and use the weight distorted-cost $\mu_k$ as the performance criterion for any arm $k$. The latter quantity, defined in \eqref{eq:cpt-intro}, can be seen to be equivalent to the following:
\begin{align}
\hspace{-0.5em}\mu_k  := &\intinfinity w(1-F_k(z)) dz - \intinfinity w(F_k(-z)) dz, \label{eq:cpt-general}
\end{align}
where $w:[0,1] \rightarrow [0,1]$ is a weight function that distorts probabilities.

The optimal arm is one that minimizes the weight-distorted cost, i.e., 
$\mu_* = \min_k \mu_k.$ The optimal arm is not necessarily unique, i.e., there may exist multiple arms with the optimal weight-distorted cost $\mu_*$.

With the above notion of weight-distorted cost, we define the cumulative regret $R_n$ as $R_n = \sum_{k=1}^K T_k(n) \mu_k\, - n \mu_*$,
where $T_k(n)= \sum_{m=1}^n I(I_m=k)$ is the number of times arm $k$ is pulled up to time $n$. The expected regret can be written as $\E R_n = \sum_{k=1}^K \E[T_k(n)] \Delta_k$, where $\Delta_k = \mu_k - \mu_*$ denotes the gap between the weight-distorted costs of the optimal arm and of arm $k$. Note that this definition of regret arises from the interpretation that each arm $k$ is associated with a deterministic value $\mu_k$. The least possible cumulative cost that can be suffered in $n$ rounds is thus $n \mu_*$, while that suffered by a given strategy is $\sum_{k=1}^K T_k(n) \mu_k$. Thus, the regret as defined above is a measure of  the rate at which a strategy converges to playing the optimal arm in the sense of weighted or distorted cost. 

We remark that the regret performance measure, as defined above, is explicitly defined within a stochastic model for rewards. Thus, low-regret algorithms designed for the nonstochastic setting, e.g., EXP3 \citep{Auer02NonSto}, are not inherently suitable for this problem, as they do not factor in the distortion caused in (expected) reward. A similar observation holds for conventional stochastic bandit algorithms such as UCB, and algorithms sensitive to arm reward variances such as UCB-V \citep{AudMunSze09:UCBV} --  once weight distortion is incorporated, the algorithm will converge to an arm that is not weight-distorted value optimal. Thus, applying a variance-sensitive algorithm (like UCB-V) will still yield linear regret in the distorted setting.


%
%\footnote{This is a subtle departure from the conventional definition of regret in standard stochastic bandits (where the weight function is the identity function), where the expected regret can also be interpreted as the expected difference in cumulative cost between 
%the optimal fixed-arm strategy and a given strategy. We do not have this equivalence with nontrivial distortion functions as the iterated expectation property no longer applies.}. 


\subsection{W-UCB Algorithm}
\label{sec:W-UCB}
Estimating the weight-distorted cost for any arm $k$ is challenging, and one cannot use a Monte Carlo approach with sample means because weight-distorted cost involves a distorted distribution, whereas the samples come from the undistorted distribution $F_k$. Thus, one needs to estimate the entire distribution, and for this purpose, we adapt the quantile-based approach of \citep{prashanth2015cumulative}.  

\paragraph{Estimating $\mu_k$:}
At time instant $m$, let $Y_{k,1},\ldots, Y_{k,l}$ denote the samples from the cost distribution $F_k$ for arm $k$, where we have used $l$ to denote the number of samples $T_k(m-1)$ for notational convenience.
Order the samples in ascending fashion as $Y_{[k,1]} \leq Y_{[k,2]} \leq \cdots \leq Y_{[k,l_b]} \leq 0 \leq Y_{[k,l_b + 1]} \leq \cdots \leq Y_{[k,l]}$, where $l_b \in \{ 0, 1, 2, \ldots, l \}$ denotes the index of a `boundary' sample after which a sign change occurs. %
% $(Y_{[k,1]},\ldots ,Y_{[k,l]})$.
The first integral in \eqref{eq:cpt-general} is estimated by the quantity
%
%Correct expressions for ordered ascending values $(Y_{[k,1]},\ldots ,Y_{[k,l]})$ *irrespective of sign* (just for future reference)
%\begin{align*}
%\widehat \mu_{k,l}^+ := \sum_{i=1}^l w \left( \frac{i}{l} \right) \left( Y_{[k,l+1-i]}^+ - Y_{[k,l-i]}^+ \right)
%\end{align*}
%
%\begin{align*}
%\widehat \mu_{k,l}^- := \sum_{i=1}^l w \left( \frac{i}{l} \right) \left( (-Y_{[k,i]})^+ - (-Y_{[k,i+1]})^+ \right)
%\end{align*}
%
\begin{align}
\hspace{-0.3em}\widehat \mu_{k,l}^+\!:=\!\sum_{i=l_b+1}^{l} Y_{[k,i]} \left(w\left(\frac{l+1-i}{l}\right)\!-\! w\left(\frac{l-i}{l}\right) \right),
\label{eq:cpt-est+}
\end{align}
while the second integral in \eqref{eq:cpt-general} is estimated by the quantity
\begin{align}
\widehat \mu_{k,l}^-:=\sum_{i=1}^{l_b} Y_{[k,i]} \left(w\left(\frac{i-1}{l}\right)- w\left(\frac{i}{l}\right) \right). 
\label{eq:cpt-est-}
\end{align}
We finally estimate $\mu_k$ as follows:
\begin{align}
\widehat \mu_{k,l} =\widehat \mu_{k,l}^+ - \widehat \mu_{k,l}^-.
\label{eq:cpt-est}
\end{align}
From \eqref{eq:cpt-general} and \eqref{eq:cpt-est} above, it can be seen that $\widehat \mu_{k,l}$ is the weight-distorted cost of the empirical distribution of samples from arm $k$ seen thus far, i.e.,
\begin{align*}
\widehat \mu_{k,l}  &:= \intinfinity w(1-\hat F_{k,l}(z)) dz - \intinfinity w(\hat F_{k,l}(-z)) dz, \label{eq:cpt-est-equiv}
\end{align*}
where $\hat F_{k,l}(x):=\frac{1}{l} \sum_{i=1}^{l} I_{\left[Y_{k,i} \leq x\right]}$ denotes the empirical distribution of  r.v. $\arm_x$. 
In particular, the first and second integral above correspond to \eqref{eq:cpt-est+} and \eqref{eq:cpt-est-}, respectively. 

We next provide a sample complexity result for the accuracy of the estimator $\widehat \mu_{k,l}$ under the following assumptions:\\
\noindent\textbf{(A1)}  
The weight function $w$ is \holder continuous with constant $L$ and exponent $\alpha \in (0,1]$: $\sup_{x \neq y} \frac{| w(x) - w(y) |}{| x-y |^{\alpha}} \leq L.$ \\
\noindent\textbf{(A2)}  
The arms' costs are bounded by $M > 0$ almost surely.

(A1) is necessary to ensure that the weight-distorted value $\mu_k, k=1,\ldots,K$ is finite. Moreover, the popular choice for the weight function, proposed by \cite{tversky1992advances} and illustrated in Figure \ref{fig:weight}, is \holder continuous.

% Further,
% there exists $ \gamma \le \alpha$ such that (s.t.)
% $\int_0^{+\infty} \mathbb{P}^{\gamma} (u^+(X)>z) dz < +\infty$ and $\int_0^{+\infty} \mathbb{P}^{\gamma} (u^-(X)>z) dz < +\infty$,
% where $\mathbb{P}^{\gamma}(\cdot) = \left(\mathbb{P}(\cdot)\right)^{\gamma}$.

% in estimating the true weight-distorted cost of arm $k$.
\begin{theorem}[\textbf{\textit{Sample complexity of estimating distorted cost}}]
\label{thm:cpt-est}
Assume (A1)-(A2). Then, for any $\epsilon >0$ and any $k\in \{1,\ldots,K\}$, we have 
$
P(\left |\widehat \mu_{k,m} - \mu_k \right| > \epsilon ) \le 2\exp\left(-2m(\epsilon/LM)^{2/\alpha}\right).
$
\end{theorem}
 For the special case of Lipschitz weight functions $w$, setting $\alpha=1$ in the above theorem, we obtain a sample complexity of order $O\left(1/\epsilon^2\right)$ for accuracy $\epsilon$. 
 \begin{proof}
 See Section \ref{sec:appendix-cpt-est}.
% %The proof proceeds along the lines of \cite[Proposition 2]{prashanth2015cumulative}. The proof relies on the Dvoretzky-Kiefer-Wolfowitz (DKW) inequality, which gives finite-sample exponential concentration of the empirical distribution $\hat{F}_{k,m}$ around the true distribution $F_k$, as measured by the $||\cdot||_\infty$ function norm \cite[Chapter 2]{wasserman2006}.
 \end{proof}

\paragraph{B-values (weighted UCB values):}
At instant $m$, define the B-value for any arm $k$, as a function of the number of samples $l$ and constants $\alpha \in [0,1], L > 0, M > 0$, as: $B_{m,l}(k) = \widehat \mu_{k,l} - \gamma_{m,l}, \text{ where } 
\gamma_{m,l} := LM \left(\dfrac{3\log m}{2l}\right)^{\frac{\alpha}{2}}.$
% \label{eq:bval}
% As before, $m$ is the number of times that arm $k$ has been played in the past up to instant $n$. 
The r.v. $\widehat \mu_{k,l}$, defined by \eqref{eq:cpt-est}, is an estimate of $\mu_k$ that uses the $l=T_k(m-1)$ sample costs of arm $k$ seen so far and $\gamma_{m,l}$ is the confidence width, which together with $\widehat \mu_{m,l}$ ensures that the true weight-distorted value $\mu_k$ lies within  $[\widehat \mu_{k,l} - \gamma_{m,l}, \widehat \mu_{k,l} + \gamma_{m,l}]$ with high probability, i.e., for $k=1,\ldots,K$, both $P\left(\widehat \mu_{k,l} + \gamma_{m,l} \le \mu_k \right) \le 2m^{-3}$ and $P\left(\widehat \mu_{k,l} - \gamma_{m,l} \ge \mu_k \right) \le 2m^{-3}$.

Using the B-values defined above, the W-UCB algorithm chooses the arm $I_m$ at instant $m$ as follows:
\begin{align}
% \text{{\bf W-UCB algorithm:}} \quad \quad 
&\text{If $m \leq K$, then play $I_m = m$ (initial round-robin phase)}, \nonumber \\
&\text{Else, play} \; I_m = \argmin_{k=\{1,\ldots,K\}} B_{m,T_k(m-1)}(k). 
\end{align}   



\subsection{Main results}
\begin{theorem}[\textbf{\textit{Regret bound}}]
\label{thm:BasicHolderRegretGap}
Under (A1)-(A2), the expected cumulative regret $R_n$ of W-UCB is bounded as follows:
$$ \E R_n \le \sum\limits_{\{k:\Delta_k>0\}} \dfrac{3(2LM)^{2/\alpha}\log n}{2\Delta_k^{2/\alpha - 1}} + MK\left(1 + \dfrac{2\pi^2}{3} \right).
$$ 
\end{theorem} 
 \begin{proof}
 See Section \ref{sec:appendix-gapdependentregret}.
 \end{proof}
The theorem above involves the gaps $\Delta_k$. We next present a gap-independent regret bound in the following result:
\begin{corollary}[{\bf \textit{Gap-independent regret}}]
\label{cor:BasicHolderRegretNoGap}
  Under (A1)-(A2), the expected cumulative
  regret $R_n$ of W-UCB satisfies the following gap-independent
  bound. There exists a universal constant $c>0$ such that for all $n$,
  $ \E R_n \le M K^{\alpha/2} \left(\frac{3}{2}(2L)^{2/\alpha} \log n + c \right)^{\frac{\alpha}{2}} \; n^{\frac{2-\alpha}{2}}.$ 
\end{corollary}
\begin{proof}
See Section \ref{sec:appendix-gapindependentregret}.
\end{proof}

\begin{remark}\textbf{\textit{(Lipschitz weights)}}
\label{remark:lipschtiz}
We can recover the $O(\sqrt{n})$ regret bound (or same dependence on the gaps) as in regular UCB for the case when $\alpha=1$, i.e., Lipschitz weights. On the other hand, when $\alpha <1$, the regret bounds are weaker than $O(\sqrt{n})$. \end{remark}

% An interesting question here is if the regret bounds for W-UCB are the best achievable. 
The following result shows that one cannot hope to obtain better regret than that of W-UCB (Theorem \ref{thm:BasicHolderRegretGap}) over the class of \holderNS-continuous weight functions, i.e., weight functions satisfying (A1)-(A2), by exhibiting a matching lower bound. 

\begin{theorem}[\textbf{\textit{Regret lower bound}}]
\label{thm:karmedlowerbdInformal}
For any learning algorithm with sub-polynomial regret in the time horizon, there exists (1) a weight function which is monotone increasing and $\alpha$-\holder continuous with constant $L$, and (2) a set of cost distributions for the arms with support bounded by $M$, for which the algorithm's regret satisfies\\
\centerline{${\expect{R_n}} = \Omega \left( \sum_{\{k: \Delta_k > 0\}} \dfrac{(LM)^{2/\alpha} \log n}{4\Delta_k^{2/\alpha - 1}} \right)$.}
\end{theorem}
We defer a more precise statement of the above result to Section \ref{sec:appendix-regretlowerbound}.

\begin{remark}\textbf{\textit{(CPT adaptation)}}
\label{remark:cptKarmed}
As remarked in the introduction, a popular approach using a weight function to distort probabilities is the so-called \textit{cumulative prospect theory} (CPT) \citep{tversky1992advances}. In addition to the weight function, a salient feature of CPT is to employ different utility functions, say $u^+$ and $u-$, to handle gains and losses, respectively. The B-values and the theoretical guarantees can be easily extended to incorporate CPT-style utility functions, and the reader is referred to Section \ref{sec:appendix-karmedcpt} for details. 
\end{remark}
