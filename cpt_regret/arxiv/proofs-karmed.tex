\subsubsection{Proof of Theorem \ref{thm:cpt-est}}
\label{sec:appendix-cpt-est}
The proof is very similar to that of Proposition 2 in \citep{prashanth2015cumulative}. The proof relies on the Dvoretzky-Kiefer-Wolfowitz (DKW) inequality, which gives finite-sample exponential concentration of the empirical distribution $\hat{F}_{k,m}$ around the true distribution $F_k$, as measured by the $||\cdot||_\infty$ function norm \cite[Chapter 2]{wasserman2006}.

\subsubsection{Proof of Theorem \ref{thm:BasicHolderRegretGap}}
\label{sec:appendix-gapdependentregret}
\begin{proof}
As in the case of regular UCB, we bound the number of times a sub-optimal arm is pulled using the technique from \cite{munos2014survey}. 

Let $k_*$ denote the optimal arm. Suppose that a sub-optimal arm $k$ is pulled at time $m$, which implies that 
$$\widehat \mu_{k,T_k(m-1)} - \gamma_{m,T_k(m-1)} \le  \widehat \mu_{k_*,T_{k_*}(m-1)} - \gamma_{m,T_{k_*}(m-1)}.$$
The B-value of arm $k$ can be smaller than that of $k_*$ {\em only if} one of the following three conditions holds:
\begin{align}
 \mu_* < \widehat \mu_{k_*,T_{k_*}(m-1)} &- \gamma_{m,T_{k_*}(m-1)}, \label{eq:optarmmeanoutside}\\
&\text{ or } \nonumber\\
\widehat \mu_{k,T_k(m-1)} + \gamma_{m,T_k(m-1)}& < \mu_k, \label{eq:karmmeanoutside}\\
&\text{ or } \nonumber\\
\quad \mu_k \,-\, 2 \,\gamma_{m,T_{k}(m-1)}& \le \mu_*. \label{eq:cond3}  
\end{align}
Note that condition (\ref{eq:cond3}) above is equivalent to requiring $T_{k}(m-1) \le  \dfrac{2^{2/\alpha -1} 3 (LM)^{2/\alpha}\log m}{\Delta_k^{2/\alpha}}$. 

Let $\kappa = \dfrac{2^{2/\alpha -1} 3 (LM)^{2/\alpha}\log n}{\Delta_k^{2/\alpha}} + 1$. When $T_{k}(m-1) \ge  \kappa$, i.e., the condition in \eqref{eq:cond3} does not hold, then either (i) arm $k$ is not pulled at time $m$, or 
(ii) \eqref{eq:optarmmeanoutside} or \eqref{eq:karmmeanoutside} occurs.
Thus, we have  
\begin{align}
T_k(n) \le &\kappa + \sum_{m=\kappa+1}^n I(I_m=k; T_k(m) > \kappa) \\
\le & \kappa + \sum_{m=\kappa+1}^n I(\eqref{eq:optarmmeanoutside} \text{ or }\eqref{eq:karmmeanoutside} \text{ occur}). \label{eq:tkn}
\end{align}
From Theorem \ref{thm:cpt-est}, we can upper bound the probability of occurence of \eqref{eq:optarmmeanoutside} as follows:
$$ P(\exists 1 < l < m \text{ such that } B_{l,T_{k_*}(l-1)}(k_*) < \mu_* ) \le \sum_{l=1}^m \frac{2}{m^{3}} \le \frac{2}{m^{2}}.$$
A similar argument works for \eqref{eq:karmmeanoutside}.  
Plugging the bounds on the events governed by \eqref{eq:optarmmeanoutside} and \eqref{eq:karmmeanoutside} into \eqref{eq:tkn} and taking expectations, we obtain
  \begin{align}
	\E[T_k(n)] \leq & \kappa + 2 \sum_{m=\kappa+1}^n \frac{2}{m^{2}}\\
	 \le & \dfrac{2^{2/\alpha -1} 3 (LM)^{2/\alpha}\log n}{\Delta_k^{2/\alpha}} + \left(1 + \dfrac{2\pi^2}{3} \right).
	\end{align}
  The claim follows by plugging the inequality above into the definition of expected regret, and using the fact that $\Delta_k \leq M$ for all arms $k$, as it follows that the weight-distorted cost of any distribution bounded by $M$ again admits an upper bound of $M$. 
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Proof of Corollary \ref{cor:BasicHolderRegretNoGap}}
\label{sec:appendix-gapindependentregret}
\begin{proof}
  We have, by the analysis in the proof of Theorem
  \ref{thm:BasicHolderRegretGap}, that for all $k$ with
  $\Delta_k > 0$,
  $$ \E[T_k(n)] \leq \dfrac{3(2LM)^{2/\alpha}\log n}{2\Delta_k^{2/\alpha}} +\left(1 + \dfrac{2\pi^2}{3} \right).
$$
  Thus, we can write
  \begin{align*}
    \E R_n &= \sum_{k} \Delta_k \; \E [T_k(n)] = \sum_{k} \left( \Delta_k \; \E [T_k(n)]^{\frac{\alpha}{2}}  \right) \left( \E [T_k(n)]^{1-\frac{\alpha}{2}} \right) \\
    &\leq \left( \sum_k \Delta_k^{2/\alpha} \; \E [T_k(n)] \right)^{\frac{\alpha}{2}} \left( \sum_k \E [T_k(n)] \right)^{1 - \frac{\alpha}{2}} \\
    &\text{(H\"{o}lder's inequality with exponents $2/\alpha$ and $2/(2-\alpha)$)} \\
    & \leq \left( \dfrac{3K(2LM)^{2/\alpha}\log n}{2} + K M^{2/\alpha}\left(1 + \dfrac{2\pi^2}{3} \right)  \right)^{\frac{\alpha}{2}} n^{\frac{2-\alpha}{2}}, \quad \quad \text{(using $\Delta_k^{2/\alpha} \leq M^{2/\alpha}$)}. 
  \end{align*}
  This proves the result.
\end{proof}

\subsubsection{Formal Regret Lower Bound} 
\label{sec:appendix-regretlowerbound}
%We define the {\em relative entropy} or {Kullback-Leibler divergence} $D(p || q)$ between a Bernoulli($p$) and a Bernoulli($q$) probability distribution as 
%\[ D(p || q) = p \log \frac{p}{q} + (1-p) \log \frac{1-p}{1-q}. \]
We will need the following definition. For two probability distributions $p$ and $q$ on $\mathbb{R}$, define the {\em relative entropy} or {\em Kullback-Leibler (KL) divergence} $D(p || q)$ between $p$ and $q$ as 
\[ D(p || q) = \int \log \left(\frac{dp}{dq}(x)\right) dp(x) \]
if $p$ is absolutely continuous w.r.t. $q$ (i.e., $q(A) = 0 \Rightarrow p(A) = 0$ for all Borel sets $A$), and $D(p || q) = \infty$ otherwise. For instance, if $p$ and $q$ are Bernoulli distributions with parameters $a \in (0,1)$ and $b \in (0,1)$, respectively, then a simple calculation gives that $D(p || q) = a \log \frac{a}{b} + (1-a) \log \frac{1-a}{1-b}$. 
We will often use $D(F || G)$ to mean the KL divergence between distributions represented by their cumulative distribution functions $F$ and $G$, respectively. 

\begin{theorem}
\label{thm:karmedlowerbd}[Regret lower bound]
Consider a learning algorithm for the $K$-armed weight-distorted bandit problem with the following property. For any weight distortion function $w:[0,1] \to [0,1]$, any set of cost distributions with costs bounded by $M$, any $a > 0$ and any arm $k$ with $\Delta_k > 0$, the expected number of plays of arm $k$ satisfies $\expect{T_k(n)} = o(n^a)$. \\
Then, for any constants $L > 0$ and $\alpha \in (0,1]$, there exists a weight function $w$ which is monotone increasing and $\alpha$-\holder continuous with constant $L$, and a set of cost distributions bounded by $M$, for which the algorithm's regret satisfies
\[  \liminf_{n \to \infty} \frac{\expect{R_n}}{\log n} \geq  \sum_{\{k: \Delta_k > 0\}} \dfrac{(LM)^{2/\alpha}}{4\Delta_k^{2/\alpha - 1}}. \]
\end{theorem}

\begin{proof}
We first show the result for costs bounded by $M = 1$, the extension to general $M$ follows. 

The key ingredient in the proof is the seminal lower-bound on the number of suboptimal arm plays derived by \cite{Lai+Robbins:1985}. Their result shows that for any algorithm that plays suboptimal arms only a sub-polynomial number of times in the time horizon, 
\[ \liminf_{n \to \infty} \frac{\expect{T_k(n)}}{\log n} \geq \frac{1}{D(F_k || F_{k^*})} \]
for any suboptimal arm $k$, where $k^*$ denotes the optimal arm. (Note that the argument at its core uses only a change-of-measure idea and the sub-polynomial regret hypothesis, and is thus unaffected by the fact that we measure regret by distorted, i.e., non-expected, costs.)

Using this along with the definition of regret, we get
\begin{align}
	\liminf_{n \to \infty} \frac{\expect{R_n}}{\log n} &= \liminf_{n \to \infty} \frac{\sum_{\{k: \Delta_k > 0\}} \E[T_k(n)] \Delta_k}{\log n} \nonumber \\
	&\geq \sum_{\{k: \Delta_k > 0\}} \Delta_k \cdot \liminf_{n \to \infty} \frac{\E[T_k(n)] }{\log n} \nonumber \\
	&\geq \sum_{\{k: \Delta_k > 0\}} \frac{\Delta_k}{D(F_k || F_{k^*})} \nonumber \\
	&= \sum_{\{k: \Delta_k > 0\}} \dfrac{(2LM)^{2/\alpha}}{\Delta_k^{2/\alpha - 1}} \cdot \frac{(\Delta_k/2LM)^{2/\alpha}}{D(F_k || F_{k^*})}. \label{eq:liminfbound}
\end{align}

(Recall that $\Delta_k = \mu_k - \mu_{k^*}$ represents the difference between the weight-distorted values of arms $k$ and $k^*$.) \\

We now design a set of cost distributions for the arms, for which the limiting property claimed in the theorem holds. In fact, we will show that it enough to design Bernoulli distributions for the arms' costs, i.e., arm $k$'s cost is Ber($p_k$), or equivalently, $F_k(x) = (1-p_k)\mathbf{1}_{[0,1)}(x) + \mathbf{1}_{[1,\infty)}(x)$. This gives a simple expression for the weight-distorted cost of any arm $k$ as $\mu_k = \int_0^1 w(p_k) dz = w(p_k)$, and, consequently, $\Delta_k = w(p_k) - w(p_{k^*})$, for any weight function $w: [0,1] \to [0,1]$ with $w(0) = w(1) - 1 = 0$.

Consider now a weight function $w:[0,1] \to [0,1]$ which is monotone increasing from $0$ to $1$, \holder continuous with any desired constant $L > 0$ and exponent $\alpha \in (0,1]$, and, moreover, satisfies the following ``\holder continuity property from below'': for some $\tilde{p} \in (0,1)$ (say, $\tilde{p} = 1/2$), $|w(p) - w(\tilde{p})| \geq L |p - \tilde{p}|^\alpha$. Such a weight function can always be constructed, e.g., for $\alpha = 1/2, L = 1$, take the function $w(x) = \frac{1}{2} - \frac{1}{\sqrt{2}}\sqrt{\frac{1}{2} - x}$ for $x \in [0, 1/2]$, and $w(x) = \frac{1}{2} + \frac{1}{\sqrt{2}}\sqrt{x - \frac{1}{2}}$ for $x \in (1/2, 1]$. (This is essentially formed by gluing together two inverted and scaled copies of the function $\sqrt{x}$, to make an S-shaped function infinitely steep at $x = 1/2$.)

For such a weight function, putting $p_{k^*} = \tilde{p} = 1/2$ and $p_k > p_{k^*}$, $k \neq k^*$, we can use the standard bound\footnote{This results from using $\log x \leq x - 1$.} 
\begin{align*} D(F_k || F_{k^*}) &= p_k \log \frac{p_k}{p_{k^*}} + (1-p_k) \log \frac{1-p_k}{1-p_{k^*}}  \\
&\leq \frac{(p_k - p_{k^*})^2}{p_{k^*}(1 - p_{k^*})} = 4{(p_k - p_{k^*})^2} \\
&\leq 4 \left( \frac{w(p_k) - w(p_{k^*})}{L} \right)^{2/\alpha} \quad \quad \mbox{(by the ``lower-\holder'' property of $w$)} \\
&= 4 \left( \frac{\Delta_k}{L} \right)^{2/\alpha}. 
\end{align*}
Since $M = 1$ for the Bernoulli family of cost distributions, this implies, together with \eqref{eq:liminfbound}, that 
\[ \liminf_{n \to \infty} \frac{\expect{R_n}}{\log n} \geq  \sum_{\{k: \Delta_k > 0\}} \dfrac{L^{2/\alpha}}{4\Delta_k^{2/\alpha - 1}}. \]
For general $M$, one can modify this construction and consider arms' cost distributions to be Bernoulli scaled by the constant $M$ (i.e., equal to $M$ with probability $p$ and $0$ otherwise). This completes the proof. 

\end{proof}

\subsubsection{Cumulative prospect theory (CPT) for K-armed bandit}
\label{sec:appendix-karmedcpt}
\input{cpt-appendix.tex}





