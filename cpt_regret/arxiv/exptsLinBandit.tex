\subsection{Experiments for Linear Bandit}
\label{sec:exptsLinBandit}

We study the problem of optimizing the route choice of a human traveler using Green light district (GLD) traffic simulation software \cite{GLDSim}. In this setup, a source-destination pair is fixed in a given road network. Learning proceeds in an online fashion, where the algorithm chooses a route in each round and the system provides the (stochastic) delay for the chosen route. The objective is to find the ``best'' path that optimizes some function of delay, while not exploring too much. While traditional algorithms minimized the expected delay, in this work, we consider the distorted value (as defined in \eqref{eq:cpt-lb}) as the performance metric. 

\begin{figure}
%%%%%%% Lin-bandit figure goes here
  \begin{tabular}{cc}
\scalebox{0.75}{
\tikzset{roads/.style={line width=0.1cm}}

  \tabl{c}{\begin{tikzpicture}
  \filldraw (0,0) node[color=white,font=\bfseries]{1} circle (0.4cm);
  \filldraw (1.5,0) node[color=white,font=\bfseries]{2} circle (0.4cm);
  \filldraw (3,0) node[color=white,font=\bfseries]{3} circle (0.4cm);

  \filldraw (0,-6.0) node[color=white,font=\bfseries]{7} circle (0.4cm);
  \filldraw (1.5,-6.0) node[color=white,font=\bfseries]{8} circle (0.4cm);
  \filldraw (3,-6.0) node[color=white,font=\bfseries]{9} circle (0.4cm);
  
  \filldraw (-1.5,-0-1.5) node[color=white,font=\bfseries]{10} circle (0.4cm);
  \filldraw (-1.5,-1.5-1.5) node[color=white,font=\bfseries]{11} circle (0.4cm);
  \filldraw (-1.5,-3-1.5) node[color=white,font=\bfseries]{12} circle (0.4cm);
  
  \filldraw (4.5,-0-1.5) node[color=white,font=\bfseries]{4} circle (0.4cm);
  \filldraw (4.5,-1.5-1.5) node[color=white,font=\bfseries]{5} circle (0.4cm);
  \filldraw (4.5,-3-1.5) node[color=white,font=\bfseries]{6} circle (0.4cm);
    
  \foreach \x in {0,1.5,3}
  {
  \draw[roads,] (\x,-0.25) -- (\x,-5.75);
  \draw[roads] (-1.25,-\x-1.5) -- (4.25,-\x-1.5);
  }
  
  \draw[dotted,blue,line width=0.1cm] (4.25,-4.35) -- (3.15,-4.35) -- (3.15,-1.6) -- (4.25,-1.6);
  \draw[dotted,green,line width=0.1cm] (3,-2.85) -- (1.65,-2.85) -- (1.65, -1.6) -- (3, -1.6);
  
  \node[draw=none] at  (4.5,-3-1.5-1.5) (a1) {\textbf{\large\color{darkgreen}src}};
  \draw[thick,->] (a1) -- (4.5,-3-2);
  \node[draw=none] at  (4.5,-1.5+1.5) (a2) {\textbf{\large\color{darkgreen}dst}};
  \draw[thick,->] (a2) -- (4.5,-1);
  \end{tikzpicture}\\[1ex]}}
  &
  \hspace{4em}
  \begin{tabular}{c|c|c}
  \toprule 
   & \textbf{Expected value }& \textbf{Distorted value }\\
   & \textbf{$x_{\text{alg}}\tr\hat\theta_{\text{off}}$} &  \textbf{$\mu_{x_{\text{alg}}}(\hat\theta_{\text{off}})$}\\\midrule
   OFUL & $51.71$ & $-2.9$ \\\midrule
   WOFUL & $57.86$ & $-6.75$\\\bottomrule
  \end{tabular}
  \end{tabular}
  \caption{Expected value $x_{\text{alg}}\tr\hat\theta_{\text{off}}$ and weight-distorted value $\mu_{x_{\text{alg}}}(\hat\theta_{\text{off}})$ for OFUL and WOFUL algorithms on a 3x3-grid network. Here $\hat\theta_{\text{off}}$ is a ridge regression based estimate of the true parameter $\theta$ (see \eqref{eq:linban-depnoise}) that is obtained by running an independent simulation and $x_{\text{alg}}$ is the route that the respective algorithm converges. $x_{\text{\tiny OFUL}}$ is the blue-dotted route in the figure, while $x_{\text{\tiny WOFUL}}$ includes the green-dotted detour.}
  \label{fig:linban-perf}
\end{figure}

We implement both OFUL and WOFUL algorithms for this problem.
% , with the following weight function\footnote{The weight function we employ is recommended by the seminal CPT paper \cite{tversky1992advances}. In particular, the weight function there was estimated by experiments on human subjects.}: $w(p) = \frac{p^{0.61}}{{(p^{0.61}+ (1-p)^{0.61})}^{\frac{1}{0.61}}}$, underlying the distorted value \eqref{eq:cpt-lb}. 
% We consider two grid networks with close to $200$ and $4000$ paths between the chosen source-destination tuple. 
% While implementation of OFUL is straightforward, we remark that WOFUL can be implemented efficiently as well. In particular, the intensive step in WOFUL is computing the distorted value for each route $x$ and parameter $\theta'$ in the confidence ellipsoid. However, for a fixed $x$, we have $\argmin\limits_{\theta' \in C_{m}} \mu_x(\theta') = \argmin\limits_{\theta' \in C_{m}} x\tr\theta' = \hat{\theta}_m - D_m A_m^{-1}x/\norm{x}_{A^{-1}}$ (the first equality is by translation monotonicity of CPT), and hence, $x_m = \argmin \{ \mu_{x_1}(\hat\theta_m), \ldots, \mu_{x_{|\X|}}(\hat\theta_m)\}$, where $\X$ is the set of routes. 
Since the weight function $w$ is non-linear, a closed form expression for $\mu_x(\hat\theta_m)$ is not available and we employ the empirical distribution scheme, described for the $K$-armed bandit setting (see \eqref{eq:cpt-est}), for estimating the weight-distorted value. For this purpose, we simulate $25000$ samples of the Gaussian distribution, as defined in \eqref{eq:linban-depnoise}. 




% \subsection{Setting the benchmark $\hat\theta_{\text{off}}$}
For computing a performance benchmark to evaluate OFUL/WOFUL, we perform an experiment to learn a linear (additive) relationship between the delay of a route and the delays along each of its component lanes. This is reasonable considering the fact the individual delays along each lane stablize when the traffic network reaches steady state and the delay incurred along any route $x$ would be centered around $x\tr\theta$.
% This is based on the assumption of a linear (additive) relationship between the delay of a route and the delays along each of its component lanes, in steady state. 
For learning the linear relationship, we simulate $100000$ steps of the traffic simulator to collect the delays along any route of the source-destination pair. Using these samples, we perform ridge regression to obtain $\hat\theta_{\text{off}}$, i.e., we solve
\begin{align}\label{eq:regression}
 \hat\theta_{\text{off}} = \argmin_{\theta}  \dfrac{1}{2} \sum\limits_{i=1}^{\tau} (c_i - \theta\tr x_i)^2 + \lambda \left\| \theta \right\|^2,
\end{align}
where $c_i$ is the $i$th delay sample corresponding to a route choice $x_i$ and $\tau$ is total number of delay samples for the source-destination pair considered. Further, $\lambda$ is a regularization parameter. It is well known that $\hat \theta_{\text{off}} =A^{-1} b$, where 
$A =  \sum\limits_{i=1}^{\tau} x_i x_i\tr + \lambda I_d$
and $b =  \sum\limits_{i=1}^{\tau} c_{i} x_i$. Here $I_d$ denotes the $d$-dimensional identity matrix, with $d$ set to the number of lanes in the road network considered. 

% \subsection{Bandit routing algorithms}
We set the various parameters of the problem as well as bandit routing algorithms - OFUL and WOFUL, as follows:\\
\begin{table}[h]
\centering
\begin{tabular}{|c|c|}
\toprule
\multirow{2}{*}{\textbf{Algorithm}} & \multirow{2}{*}{\textbf{Parameters}} \\ 
 &  \\ \hline
\textbf{OFUL} & Regularization parameter $\lambda = 1$, confidence $\delta=0.99$, norm bound $\beta = 400$ \\ \midrule
\textbf{WOFUL} & Regularization parameter $\lambda = 1$, confidence $\delta=0.99$, norm bound $\beta = 400$,\\
& weight function $w(p) = \frac{p^{0.61}}{{(p^{0.61}+ (1-p)^{0.61})}^{\frac{1}{0.61}}}$  \\ \bottomrule
\end{tabular}
\end{table}

% Each algorithm involves simulating  $500000$ steps of the traffic simulator and the results are averaged over $10$ replications. 

% \subsection{Results}
We observed that both OFUL and WOFUL algorithms learn the parameter underlying the linear relationship (Eq. \eqref{eq:linban-depnoise} in the main paper) to a good accuracy. For instance, the normalized difference $\left\| \theta_{\text{alg}} - \hat \theta_{\text{off}} \right\|^2/\left\|  \hat \theta_{\text{off}} \right\|^2 \approx 0.08$ for OFUL (resp. $0.07$ for WOFUL) on the 3x3-grid network.  
\begin{table}
\centering
  \begin{tabular}{|c|c|c|}
  \toprule 
   & \textbf{Expected value }& \textbf{Distorted value }\\
   & \textbf{$x_{\text{alg}}\tr\hat\theta_{\text{off}}$} &  \textbf{$\mu_{x_{\text{alg}}}(\hat\theta_{\text{off}})$}\\\midrule
   OFUL & $56.09$ & $-9.74$ \\\midrule
   WOFUL & $58.54$ & $-22.21$\\\bottomrule
  \end{tabular}
  \caption{Performance comparison of OFUL and WOFUL algorithms on the $11$-junction network shown in Figure \ref{fig:mixedupgld}. The trend observed in Figure \ref{fig:linban-perf} continues on this network as well.}
  \label{tab:mixedup-results}
\end{table}

 \begin{figure}
    \centering
     \begin{tabular}{cc}
     \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=3in,height=2in]{gld_3x3grid.png}
     \caption{A 3x3-grid network} 
     \label{fig:3x3gridgld}
      \end{subfigure}
&		
     \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=3in,height=2in]{mixedup.png}
     \caption{A $11$-junction road network.} 
     \label{fig:mixedupgld}
\end{subfigure}
% \\
%      \begin{subfigure}{0.5\textwidth}
%         \includegraphics[width=3in,height=2in]{results/gld_3x3grid_closer.png}
%      \caption{Zoomed in view of the network in Figure \ref{fig:3x3gridgld}. Note: the source node is labeled $8$ and destination is labeled $6$.} 
%      \label{fig:3x3gridzoom}
% \end{subfigure}
% &		
%      \begin{subfigure}{0.5\textwidth}
%         \includegraphics[width=3in,height=2in]{results/mixedup_closer.png}
%      \caption{Zoomed in view of the network in Figure \ref{fig:mixedupgld}. Note: the source node is labeled $1$ and destination is labeled $4$.} 
%      \label{fig:mixedupzoom}
% \end{subfigure}
\end{tabular}

\caption{Snapshot from GLD simulator of the road networks used for our experiments. The figures show edge nodes that generate traffic, traffic lights with red-dots and  two-laned roads carrying cars.}
\label{fig:road-nets}
\end{figure}


Figure \ref{fig:road-nets} shows  two road networks - a 3x3-grid and a $11$-junction network - used for our experiments. 
Figure \ref{fig:linban-perf} presents the expected and weight-distorted values for OFUL and WOFUL for the 3x3-grid network, while Table \ref{tab:mixedup-results} presents similar results for the $11$-junction network. These values are calculated using a ridge regression-based estimate $\hat\theta_{\text{off}}$ of the true parameter $\theta$.
% , which is obtained by running an independent simulation for $100,000$ steps.
% While the results in Figure \ref{fig:linban-perf} correspond to the 3x3-grid network in Figure \ref{fig:3x3gridgld}, we also present additional results in Table \ref{tab:mixedup-results} from the simulations on the $11$-junction network shown in Figure \ref{fig:mixedupgld}.
  %
%A linear relationship is reasonable considering the fact the individual delays along each lane stable when the traffic network reaches steady-state and the delay incurred along any route $x$ would be centered around $x\tr\theta$.
As expected, OFUL (resp. WOFUL) algorithm recommends a route $x_{\text{\tiny OFUL}}$ (resp. $x_{\text{\tiny WOFUL}}$) with minimum mean delay (resp. weight-distorted value). As shown in Figure \ref{fig:linban-perf}, $x_{\text{\tiny OFUL}}$ is the shortest path, while $x_{\text{\tiny WOFUL}}$ involves a detour. %
% and this difference in routes appeals to the human intuition of picking a detour among routes with comparable delays. 
The lower value of distorted value (delay) for the longer path
preferred by WOFUL, over the shorter path preferred by OFUL, is
presumably due to the fact that the two routes differ in the variance
of the end-to-end delay. This leads to rare events being overestimated
by the weight function, ultimately making the former path more
appealing to the distortion-conscious WOFUL strategy.
%The lower value of distorted value (delay) for the longer path preferred by WOFUL is presumably due to the fact that the longer route has larger variance in end-to-end delays. Thus, the chance that the route's delay is small (a rare event) is overestimated by the weight function, making it more appealing to the distortion-conscious WOFUL strategy. 
