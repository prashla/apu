Consider the following two-armed bandit problem: 
%The rewards of Arm 1 have a mean of $1000000$ and a very small variance, while the rewards of Arm 2 could be either $5000$ or $0$, with equal probability. The expected value of Arm 2 is more than that of Arm 1, but a human being would prefer Arm 1 over Arm 2. To illustrate further, consider another two-armed bandit problem where the 
The rewards of Arm 1 are \$1 million w.p. $1/{10}^6$ and $0$ otherwise, while Arm 2  rewards are \$$1000$ w.p. $1/{10}^3$ and $0$ otherwise. In this case, humans would usually prefer Arm 1 over Arm 2. 
%and would continue to do so even if throw in another arm with a sure reward of $1$. 
The human preferences get flipped if we change to costs, i.e., Arm 1 loses a million with a very low probability of $1/{10}^6$, while Arm 2 loses \$$1000$ w.p. $1/{10}^3$. In this case, Arm 2 is preferred over Arm 1. 
The above example illustrates that traditional expected value falls short in explaining human preferences, and the reader is referred to the classic Allais problem \cite{allais53} that rigorously argues against expected utility theory as a model for human-based decision making systems.   

Violations of the expected value-based preferences in human-based decision making systems can be alleviated by incorporating distortions in the underlying probabilities of the system \cite{starmer2000developments} \cite[Chapter 4]{quiggin2012generalized}. 
Probabilistic distortions have a long history in behavioral science and economics, and we bring this idea to a multi-armed bandit setup. In particular, we base our approach on rank-dependent expected utility (RDEU) \cite{quiggin2012generalized}, which includes the popular \textit{cumulative prospect theory} (CPT) of Tversky and Kahneman \cite{tversky1992advances}. 

\begin{figure}[t]
\centering
\tabl{c}{
  \scalebox{0.65}{\begin{tikzpicture}
  \begin{axis}[width=10cm,height=6cm,legend pos=south east,
           grid = major,
           grid style={dashed, gray!30},
           xmin=0,     % start the diagram at this x-coordinate
           xmax=1,    % end   the diagram at this x-coordinate
           ymin=0,     % start the diagram at this y-coordinate
           ymax=1,   % end   the diagram at this y-coordinate
           axis background/.style={fill=white},
           ylabel={\large Weight $\bm{w(p)}$},
           xlabel={\large Probability $\bm{p}$}
           ]
          \addplot[domain=0:1, blue, thick,smooth,samples=1500] 
             {pow(x,0.69)/pow((pow(x,0.69) + pow(1-x,0.69)),1.44)}; 
             \node at (axis cs:  0.8,0.35) (a1) { $\bm{\frac{p^{0.69}}{(p^{0.69}+ (1-p)^{0.69})^{\frac1{0.69}}}}$};           
             \draw[->] (a1) -- (axis cs:  0.7,0.6);
                 \addplot[domain=0:1, black, thick, dashed]           {x};                      
  \end{axis}
  \end{tikzpicture}}\\[1ex]
}
\caption{Graphical illustration of a typical weight function that inflates low probabilities and deflates large probabilities. The weight function used in the figure is the one recommended by Tversky and Kahneman \cite{tversky1992advances} based on empirical tests involving human subjects.
}
\label{fig:weight}
\end{figure}

The distortions happen via a weight function $w:[0,1] \rightarrow [0,1]$ that transforms probabilities in a nonlinear fashion. As illustrated in Figure \ref{fig:weight}, a typical weight function, say $w:[0,1]\rightarrow [0,1]$, has the inverted-S shape. In other words, $w$ inflates low probabilities and deflates large probabilities and can explain human preferences well. For instance, in the example above, if we choose $w(1/{10}^6) > 1/{10}^6$ and take expectations w.r.t. the $w$-distorted distribution, then Arm 1 would be preferable when the problem is setup with rewards and Arm 2 for the problem with costs. The suitability of this approach, esp. with a inverted-S shaped weight function, to model human decision making (and thus preferences) has been widely documented \cite{prelec1998probability,wu1996curvature,conlisk1989three,camerer1989experimental,camerer1992recent,cherny2009new,gonzalez1999shape}.

We use a traveler's route choice as a running example to illustrate the main ideas in this paper. The setting here is that a human travels from a source (e.g., home) to a destination (e.g., office), both fixed, every day. He/she has multiple routes to choose from, and each route incurs a stochastic delay with unknown distributions. The problem then is to choose a route that minimizes some function of delay. Using expected delay may not lead to a routing choice that is appealing to the human traveler. In addition to the examples mentioned earlier, intuitively humans would prefer a route with a slight excess of delay over another that has a small probability of getting into a traffic jam that takes hours to be resolved. The requirement here is for an automated routing algorithm, say one that sits as an application on the traveler's mobile device, that learns the best route for the traveler. The algorithm is online and uses the delay information for a recommended route as feedback to find the best route. We 
treat this problem in two regimes: first, a setting where 
the number of routes is small, so the traveler can afford to try each of the routes a small number of times before fixing on the ``best'' route; second, a big road network setting that involves a large number of routes, which prohibits an approach that requires trying the bulk of the routes before deciding which is the ``best''.  
% \todop[inline]{Connect more to human-in-the-loop RL (as in your last paper) or "machine-assisted human RL" perhaps? (the automated system is trying to learn which is the best option, in the sense of CPT, to suggest to a human)}

We formalize two probabilistically distorted bandit settings that correspond to the two routing setups mentioned above. The first is the classic $K$-armed setting, while the second is the linear bandit setting. In both settings, we define the weight-distorted value $\mu_x$ for any arm $x$ in the space of arms $\X$ as follows:
\begin{align}
%\mu_x  := &\intinfinity w(1-F_x(z)) dz, \label{eq:cpt-intro} 
%- \intinfinity w^-(P_k(u^-(X_k)>z)) dz, 
\hspace{-0.4em}\mu_x \!=\! \intinfinity w(\prob{\arm_x > z}) dz \!-\! \intinfinity w(\prob{-\arm_x > z}) dz,\label{eq:cpt-intro}
\end{align}
%\todoa[inline]{Im a little confused about this definition now. How are we treating losses and gains separately here? In CPT work, we had $u+$ acting on the positive part and $u-$ on negative part.} 
\noindent where $w$ is the weight function that satisfies $w(0)=0$ and $w(1)=1$ and $\arm_x$ is the random variable (r.v.) corresponding to the stochastic rewards from arm $x \in \X$.
By choosing the identity weight function $w(p)=p$, we obtain 
$\mu_x = \E{ (\arm_x^+) } - \E{ (\arm_x^-) } = \E(\arm_x)$, where $y^+ = \max(y,0)$ and  $y^- = \max(-y,0)$ denote the positive and negative parts of $y\in \R$, respectively. Thus, $\mu_x$ as in \eqref{eq:cpt-intro} generalizes standard expected value. As discussed earlier, $w$ has to be chosen in a non-linear fashion, to capture human preferences, which has strong empirical support. 

In our setting, the goal is find an arm $x_*$ that maximizes \eqref{eq:cpt-intro}. The problem is challenging because
the current bandit solutions, for instance, the popular UCB algorithm, cannot handle distortions. This is because the environment provides samples from the distribution $F_x$ when arm $x$ is pulled, while the integral in \eqref{eq:cpt-intro} involves a distorted distribution. The implication is that a simple sample mean and a confidence term suggested by the Hoeffding inequality is enough to derive the UCB values for any arm in the regular setting involving expected values. On the other hand, one requires a good enough estimate of $F_x$ to estimate $\mu_x$. Just for the sake of example, a ($\alpha$-\holder continuous) weight function such as $w(t) := t^\alpha$, when applied to a Bernoulli($p$) distribution, distorts the mean to 
$p^\alpha$ from $p$, and can introduce an arbitrarily large scaling for arms with real expectations close to $0$. It follows that nonlinear weight distortion can, in fact, change the order of the optimal arm, resulting in a distortion-unaware algorithm like UCB converging to the {\em wrong} arm and incurring linear regret. The W-UCB algorithm that we propose incorporates a empirical distribution-based approach, similar to that of \cite{prashanth2015cumulative}, to estimate $\mu_x$. However, unlike the latter, our algorithm incorporates a confidence term relying on the Dvoretzky-Kiefer-Wolfowitz (DKW) inequality \citep{wasserman2006} that ensures the W-UCB values are a high-probability bound on the true value $\mu_x$.    
We provide upper bounds on the regret of W-UCB, assuming $w$ is \holder continuous and provide empirical demonstrations on a setting from \citep{tversky1992advances}.

Next, we consider a linear bandit setting with weight distortions that can be motivated as follows:
Consider a network graph $G=(V,E)$, $|E| = d$, with a source $s\in V$ and destination $t \in V$. The interaction proceeds over multiple rounds, where in each round $m$, the user picks a route $x_m$ from $s$ to $t$ (a route is a collection of edges encoded by a vector of $0-1$ values in $d$ dimensions) and experiences a stochastic delay $x_m\tr\left(\theta + N_m\right)$. Here $\theta \in \R^d$ is an underlying model parameter and $N_m \in \R^d$ is a random noise vector, both unknown to the learner. The physical interpretation is that the nodes represent geographical locations (say junctions) and the edges are roads that connect nodes. An edge from $i$ to $j$ will have an edge weight $\theta_{ij}$, which quantifies the delay for this edge. 

Notice that the observations include a noise component that scales with the route chosen -- this is unlike the model followed in earlier linear bandit works \citep{abbasi2011improved,dani2008stochastic}, where the noise was independent of the arm chosen. Our noise model makes practical sense because the observed delay in a road traffic network depends on the length of the route, for e.g., one would expect more noise in a detour involving ten roads than in a direct one-road route. The aim is to find a low-delay route that satisfies the user.  The setting is such that the number of routes is large (so the regular $K$-armed bandits don't scale) and one needs to utilize the linearity in the costs (delays) to find the optimal route, where optimality is qualified in 
terms of a weight-distorted expectation.

For the linear bandit setting, we propose a variant of the OFUL algorithm \citep{abbasi2011improved}, that incorporates weight-distorted values in the arm selection step. The regret analysis of the resulting WOFUL algorithm poses novel challenges compared to that in the linear bandit problem, primarily because the instantaneous cost (and hence regret) at each round is in fact a {\em nonlinear} function of the features of the played arm. This occurs due to the distortion in expectation caused by the weight function, although the actual observation (e.g., network delay in the example above) is linear in expectation over the played arm's features. The weight function can not only change the optimal arm but can also potentially amplify small differences in real expected values of arms to much larger values, leading to a blowing up of overall regret. %
%Just for the sake of example, a ($\alpha$-\holder continuous) weight function such as $w(t) := t^\alpha$, when applied to a Bernoulli($p$) distribution, distorts the mean to 
%$p^\alpha$ from $p$, and can introduce an arbitrarily large scaling for arms with real expectations close to $0$. To overcome this, we derive bounds on the \holder-exponent of the distorted mean for the class of \holder-continuous weight functions (widely used in cumulative prospect theory by \cite{tversky1992advances}) under various noise distribution families, which then help us derive regret bounds for WOFUL -- we get $O\left(d^{\frac{\alpha}{2}} n^{\frac{2-\alpha}{2}}\right)$ regret for the class of $\alpha$-\holder continuous weight functions, which not only captures the worsening or amplification due to the weight function, but also generalizes the standard linear bandit regret bound (Theorem 3 of \cite{abbasi2011improved}) with the $1$-\holder continuous identity function.
However, our analysis shows that regret in weight-distorted cost as the performance metric can be controlled at the same rate as that in standard linear bandit models, irrespective of the structure of the distortion function. More specifically, we show, using a careful analysis of the effect of weight distortion, that the regret of the WOFUL algorithm is no more than $O\left(d \sqrt{n} \mbox{ polylog}(n)\right)$ in $n$ rounds with high probability, similar to the guarantee enjoyed by the OFUL algorithm in linear bandits (note however that the identity of the optimal arm may be different due to weight distortion in costs). 

\subsection*{Related work}
The closest related previous contribution is that of \cite{prashanth2015cumulative}, where the authors bring in ideas from CPT to a reinforcement learning (RL) setting. In contrast, we formulate two multi-armed bandit models that incorporate weight-distortions. From a theoretical standpoint, we handle the exploration-exploitation tradeoff via UCB-inspired algorithms, while the focus of \cite{prashanth2015cumulative} was to devise a policy-gradient scheme given biased estimates of a certain CPT-value defined for each policy. Moreover, we provide finite-time regret bounds for both bandit settings, while the guarantees for the policy gradient algorithm of \cite{prashanth2015cumulative} are asymptotic in nature.

Previous works involving RDEU and CPT are huge in number; at a conceptual level, the work in this paper integrates machine learning (esp. bandit learning) with an RDEU approach that involves a probabilistic distortions via a weight function. To the best of our knowledge, RDEU/CPT papers in the literature assume model information, i.e., a setting where the distributions of the arms are known, while we have a \textit{model-free} setting where one can only obtain sample values from the arms' distributions. Our setting makes practical sense; for instance, in the traveler's route choice problem one can only obtain sample delays for a particular route, while the distribution governing the delays for any route is not known explicitly.  
%\todoa{compare against some bandit/lin-bandit refs here}


The rest of the paper is organized as follows: 
In Section~\ref{sec:karmed}, we formalize the $K$-armed bandit problem with weight distortions and present the W-UCB algorithm.
Next, in Section~\ref{sec:linear}, we
formulate the linear bandit problem with arm-dependent noise model and present the WOFUL algorithm. In Section \ref{sec:proofs}, we provide the proofs of the regret bounds for W-UCB as well as WOFUL algorithms.
In Section~\ref{sec:expts}, we present simulation experiments on a synthetic setup and a traffic routing application, respectively. Finally, in Section~\ref{sec:conclusion} we provide the concluding remarks.

