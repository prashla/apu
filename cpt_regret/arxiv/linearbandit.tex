\subsection{Arm-dependent noise setting}
The setting here involves arms that are given as the compact set $\X\subset\R^d$ (each element of $\X$ is interpreted as a vector of features associated with an arm). %
The learning game proceeds as follows. At each round $m = 1, 2, \ldots$, the learner \\
\begin{inparaenum}[\bfseries (a)]
\item plays an arm $x_m \in \X$, possibly depending on the history of observations thus far, and \\
\item observes a stochastic, nonnegative cost given by 
\begin{align}
c_m := x_m\tr\left(\theta + N_m\right),\label{eq:linban-depnoise}
\end{align} 
where $N_m := (N_m^1, \ldots, N_m^d)$ is a vector of i.i.d. standard Gaussian random variables, independent of the previous vectors $N_1, \ldots, N_{m-1}$, and $\theta \in \R^d$ is an underlying model parameter. Both $\theta$ and $N_m$, $m \geq 1$, are unknown to the learner. 
% The noise model above deviates from that in previous linear bandit papers \cite{abbasi2011improved,dani2008stochastic}, but can be motivated from the fact the delay (cost $c_m$ above) . 
\end{inparaenum}


Given a weight function $w: [0,1] \to [0,1]$, we define the weight-distorted cost $\mu(x,\theta)$ for arm $x \in \X$, with underlying model parameter $\theta$, to be the quantity
\begin{align}
%\mu_x(\theta)  := &\intinfinity w(1-F_x^\theta(z)) dz, \label{eq:cpt-lb} 
%- \intinfinity w^-(P_k(u^-(X_k)>z)) dz, 
\mu_x(\theta)  \!:= \!\intinfinity w(1 \!-\! F_x^\theta(z)) dz \!+\! \intinfinity w(F_x^\theta(-z)) dz, \label{eq:cpt-lb} 
\end{align}
where $F_x^\theta(z) := \prob{x\tr(\theta + N) \leq z}$, $z \in \R$, is the cumulative distribution function of
the stochastic cost from playing arm $x \in \X$.  An arm $x$ is said to be optimal if its weight-distorted cost equals the least possible weight-distorted cost achieved across all arms, i.e., if
$ \mu_x = \mu_* :=  \min_{x' \in \X} \mu_{x'}(\theta).$ As in the $K$-armed setting, the performance measure is the cumulative regret $R_n$ over $n$ rounds, defined as 
% the random variable
$R_n =  \sum_{m=1}^n \mu_{x_{m}}(\theta) - n\mu^*,$
%R_n = n \mu_* \sum_{k=1}^K T_k(n) \mu_k,
where $x_{m}$ is the arm chosen by the bandit algorithm in round $m$.
 
\subsection{The WOFUL algorithm}
Algorithm \ref{alg:WOFUL} presents the pseudocode for the proposed algorithm, which
 follows the general template for linear bandit algorithms (cf. ConfidenceBall in \citep{dani2008stochastic} or OFUL in \citep{abbasi2011improved}), but deviates in the step when an arm is chosen. In particular,  in any round $m$ of the algorithm, WOFUL uses $\mu_x(\theta)$ as the decision criterion for any arm $x \in \X$ and $\theta \in C_m$, where $\mu_x(\theta)$ is the weight-distorted value that is defined in \eqref{eq:cpt-lb} and $C_m$ is the confidence ellipsoid that is specified in Algorithm \ref{alg:WOFUL}. This is unlike regular linear bandit algorithms, which use  $x\tr\theta$ as the cost for any arm $x \in \X$ and $\theta \in C_m$.  Note that the ``in-parameter'' or arm-dependent noise model \eqref{eq:linban-depnoise} also necessitates modifying the standard confidence ellipsoid construction of \cite{abbasi2011improved} by rescaling with the arm size (the $A_m$ and $b_m$ variables in Algorithm \ref{alg:WOFUL}). For a positive semidefinite matrix 
$M$ and a vector $x$, we use the notation $\norm{x}_M = \sqrt{x\tr M x}$ to denote the Euclidean norm of $x$ weighted by $M$.   
%%%%%%%%%%%%%%%% alg-custom-block %%%%%%%%%%%%
\algblock{UCBcompute}{EndUCBcompute}
\algnewcommand\algorithmicUCBcompute{\textbf{\em Confidence set computation}}
 \algnewcommand\algorithmicendUCBcompute{}
\algrenewtext{UCBcompute}[1]{\algorithmicUCBcompute\ #1}
\algrenewtext{EndUCBcompute}{\algorithmicendUCBcompute}

\algblock{RecoAndFeedback}{EndRecoAndFeedback}
\algnewcommand\algorithmicRecoAndFeedback{\textbf{\em Arm selection + feedback}}
 \algnewcommand\algorithmicendRecoAndFeedback{}
\algrenewtext{RecoAndFeedback}[1]{\algorithmicRecoAndFeedback\ #1}
\algrenewtext{EndRecoAndFeedback}{\algorithmicendRecoAndFeedback}

\algblock{StatsUpdate}{EndStatsUpdate}
\algnewcommand\algorithmicStatsUpdate{\textbf{\em Update statistics}}
 \algnewcommand\algorithmicendStatsUpdate{}
\algrenewtext{StatsUpdate}[1]{\algorithmicStatsUpdate\ #1}
\algrenewtext{EndStatsUpdate}{\algorithmicendStatsUpdate}

\algtext*{EndUCBcompute}
\algtext*{EndRecoAndFeedback}
\algtext*{EndStatsUpdate}

\begin{algorithm}[t]  
\caption{WOFUL}
\label{alg:WOFUL}
\begin{algorithmic}
\State {\bfseries Input:} regularization constant $\lambda \geq 0$, confidence $\delta \in (0,1)$, norm bound $\beta$, weight function $w$.
\State {\bfseries Initialization:} $A_1=\lambda I_{d \times d}$ ($d \times d$ identity matrix), $b_1=0$, $\hat\theta_1 =0$.
  \For{$m = 1,2,\ldots$}
   \UCBcompute
  \State Set $C_{m}\! := \!\left\{\theta \in \mathbb{R}^d: \norm{\theta - \hat{\theta}_m}_{A_m} \leq D_m \right\}$ and \State $D_m \!:=\! \sqrt{2 \log \left(\frac{\det(A_m)^{1/2} \; \lambda^{d/2} }{\delta} \right)} + \beta\sqrt{\lambda}$.
	%\State $\hat{\theta}_t := A^{-1} b$
   \EndUCBcompute
	
   \RecoAndFeedback       
  \State Let $ (x_{m}, \tilde \theta_{m}) := \argmin\limits_{(x,\theta') \in \X\times  C_{m}} \mu_x(\theta')$.
  
	%, where $\mu(x,\theta)$ is given by \eqref{eq:cpt-lb}
  \State Choose arm $ x_{m}$ and observe cost $c_m$.
   \EndRecoAndFeedback     
  
 	\StatsUpdate
			\State Update $A_{m+1} = A_m + \frac{x_{m}x_{m}\tr}{\norm{x_m}^2}$,
			\State $b_{m+1} = b_m+ \frac{c_m x_{m}}{\norm{x_m}}$, and
		  \State $\hat\theta_{m+1} = A_{m+1}^{-1} b_{m+1}$
 	\EndStatsUpdate
  \EndFor
\end{algorithmic}
\end{algorithm}


\begin{remark}(\textbf{\textit{Computation cost}})
%WOFUL can be implemented with the same computational complexity as OFUL when the number of arms is finite. In particular, 
The computationally intensive step in WOFUL is the optimization of the weight-distorted value over an ellipsoid in the parameter space (the third line in the {\bf for} loop). This can be explicitly solved as follows. For a fixed $x \in \X$, we can let $\bar \theta_{m,x} := \argmin\limits_{\theta' \in C_{m}} \mu_x(\theta') = \argmin\limits_{\theta' \in C_{m}} x\tr\theta' = \hat{\theta}_m - D_m A_m^{-1}x/\norm{x}_{A^{-1}}$ This is because the weight-distorted value is monotone under translation (see Lemma \ref{lemma:cptdiff} below). The cost-minimizing arm is thus computed as $x_m = \argmin \{ \mu_{x_1}(\bar \theta_{m,1}), \ldots, \mu_{x_{|\X|}}(\bar \theta_{m, |\X|})\}$.
\end{remark}

%The parameter $\beta$ in Algorithm \ref{alg:WOFUL}
%represents a known upper bound on the size of $\theta$,
%i.e., %the regret bound of OFUL algorithm \cite{abbasi2011improved}
%$\norm{\theta}_2 \leq \beta$. 
%The following results bound the regret of
%the WOFUL algorithm under mild conditions on the arms' cost distributions. 

\subsection{Main results}
\begin{theorem}[\textbf{\textit{Regret bound for WOFUL}}]
\label{thm:linear-bandit-regret} 
Suppose that the weight function $w$ satisfies $0 \le w(p) \le 1$, $\forall p \in (0,1)$, 
% (b) $\{N_m\}_{m \geq 1}$ are i.i.d., $d$-dimensional vector of standard Gaussian r.v.s, (c) the mean costs are bounded: 
$\forall x \in \X: x\tr \theta \in [-1,1]$, and $\norm{\theta}_2 \leq \beta$. Then, for any $\delta > 0$, the regret $R_n$ of WOFUL, run with parameters $\lambda > 0$, $B$, $\delta$ and $w$,
  satisfies
$P\left(R_n \le \sqrt{32 d n D_n \log n} \; \; \forall n \geq 1 \right) \ge 1-\delta.$
\end{theorem}

 \begin{remark}
 	If for all $x \in X$, $\norm{x}_2 \leq \ell$, then the quantity $D_n$ appearing in the regret bound above is \\$O\left( \sqrt{d \log \left( \frac{n\ell^2}{\lambda \delta}\right)}\right)$ \cite[Lemma 10]{abbasi2011improved}; thus, the overall regret is\footnote{$\tilde O(\cdot)$ is a variant of the $O(\cdot)$ that ignores log-factors.} $\tilde{O}\left( d \sqrt{n} \right)$.
 \end{remark}
  \begin{remark}
	For the identity weight function $w(t) = t$, $0 \leq t \leq 1$ with $L = \alpha = 1$, we recover the stochastic linear bandit setting, and the associated $\tilde{O}\left( d \sqrt{n}\right)$ regret bound for linear bandit algorithms such as $\text{ConfidenceBall}_1$ and $\text{ConfidenceBall}_2$ \citep{dani2008stochastic}, OFUL \citep{abbasi2011improved}. Hence, the result above is a generalization of regret bounds for standard linear bandit optimization to the case where a non-linear weight function of the cost distribution is to be optimized from linearly parameterized observations. The distortion of the cost distribution via a weight function, rather interestingly, does \emph{not} impact the order of the bound on problem-independent regret, and we obtain $\tilde{O}\left( d \sqrt{n}\right)$ here as well. 
 \end{remark}
 \begin{remark}
	Note that the weight function $w$ can be any non-linear function bounded in $[0,1]$; unlike the K-armed setting, we do not impose a \holder continuity assumption on $w$. 
\end{remark}

\begin{remark}
A lower bound of essentially the same order as Theorem \ref{thm:linear-bandit-regret} (O$\left(d \sqrt{n}\right)$) holds for regret in (undistorted) linear bandits \citep{DanKakHay07:LinBandit}. One can show a similar lower bound argument with distortions, implying that the result of the theorem is not improvable (order-wise) across weight functions.
\end{remark}

\begin{remark}
(\textbf{\textit{Linear bandits with arm-independent additive noise}}) An alternative to modelling ``in-parameter'' or arm-dependent noise \eqref{eq:linban-depnoise} is to have independent additive noise, i.e., $c_m := x_m\tr \theta + \eta_m$. This is a standard model of stochastic observations adopted in the linear bandit literature \citep{abbasi2011improved,dani2008stochastic}. The key difference here is that, unlike the setting in \eqref{eq:linban-depnoise}, the noise component $\eta_m$ does \emph{not} depend on the arm played $x_m$. In this case, Lemma \ref{lemma:cptdiff} below shows that $\mu_{X+a} \geq \mu_X$, i.e., the distorted CPT value $\mu$ preserves order under translations of random variables. As a consequence of this fact, the WOFUL algorithm reduces to the OFUL algorithm in the standard linear bandit setting with arm-independent noise. 
\end{remark}


% If $N$ is assumed to be iid Gaussian with unit variance per-component, this is equivalent to the conditional distribution of reward/cost being Gaussian with mean $x\tr\theta$ and variance $\norm{x}_2^2$. But now, it is unclear how to evaluate/compute CPTs of arbitrary Gaussians (translations do not work cleanly). 

\begin{proof}[Proof sketch for Theorem \ref{thm:linear-bandit-regret}]
We upper-bound the instantaneous regret $r_m$ as follows:
Letting $\hat x_m = \frac{x_m}{\norm{x_m}}$ and $\stdnormal$ to be a standard Gaussian r.v. in $d$ dimensions,  we have
\begin{align}
r_m &=  \mu_{x_{m}}(\theta) - \mu_{x_*}(\theta) \le \mu_{x_{m}}(\theta) - \mu_{x_m}(\tilde\theta_m) \nonumber \\
& = \norm{x_m} \left( \mu_{W+ \hat x_{m}\tr\theta} - \mu_{W+ \hat x_{m}\tr\tilde\theta_m} \right) \label{eq:trm2}\\
& \le 2 \norm{x_m} \left| \hat x_{m}\tr(\theta-\tilde\theta_m) \right|, \label{eq:trm3}
\end{align} 
and the rest of the proof uses the standard confidence ellipsoid result that ensures $\theta$ resides in $C_m$ with high probability. 
 A crucial observation necessary to ensure \eqref{eq:trm2} is that, for any r.v. $X$ and any $a\in\R$, the difference in weight-distorted cost $\mu_{X+a} - \mu_X$ is a non-linear function of $a$ (see Lemma \ref{lemma:cptdiff} below). Thus, it is not straightforward to compute the weight-distorted cost after translation and this poses a significant challenge in the analysis of WOFUL for the arm-dependent noise model that we consider here. 

% The proof relies on the following lemma that characterizes the change in weight-distorted value of a r.v. $X$ when shifted by $a \in \R$.  

\begin{lemma}%[CPT value under translation]
\label{lemma:cptdiff}
Let 
$%\mu_{w,X} \equiv 
\mu_X := \intinfinity w(\prob{X > z}) dz - \intinfinity w(\prob{-X > z}) dz.$ Then, for any $a \in \R$, we have 
$\mu_{X+a} = \mu_X + \int_{-a}^0 \left[ w(\prob{X > u}) + w(\prob{X < u})  \right] du.$
Consequently, since $w$ is bounded by 1, we have 
$|\mu_{X+a} - \mu_X| \leq 2|a|, \text{ for any }a\in \R.$
\end{lemma}
The reader is referred to Section \ref{sec:appendix-linear} for the detailed proof. 
\end{proof}

%%% arm-independent noise
%\subsection{Arm-independent noise setting}
%As before, the arms are given as the compact set $\X\subset\R^d$ and the learning game proceeds as in the arm-dependent noise setting. The difference here is that playing an arm $x_m \in \X$ in round $m$ results in a stochastic cost given by $r_m := x_m\tr \theta + \eta_m$. Note that, unlike the previous setting, the noise component $\eta_m$ does not depend on the chosen arm $x_m$. This is a standard model of stochastic observations adopted in the linear bandit literature \cite{abbasi2011improved,dani2008stochastic}. For the sake of simplicity, assume that $\eta_1, \eta_2, \ldots$ is an i.i.d. 
%% \footnote{Our analysis can be generalized to the case when the $\{\eta_m\}_{m \geq 1}$ forms a martingale difference sequence instead of being iid. However, this introduces additional notational complications -- in particular, the weight-distorted cost of an arm can change depending on the noise distribution in a given round, and we avoid this for the sake of simplicity.} 
% sequence of $1$-sub-Gaussian (and hence zero-mean) random variables.
%% \footnote{A random variable $X$ is $b$-sub-Gaussian if for all $t \in \mathbb{R}$, $\expect{e^{tX}} \leq e^{b^2 t^2 / 2}$ \cite{rivasplata2012subgaussian}. Equivalently, there exists $c \equiv c(b)$ such that for all $\lambda > 0$, $\prob{|X| > \lambda} \leq 2 e^{-c \lambda^2}$.} 
%% \footnote{We focus on
%%   $1$-sub-Gaussian noise for simplicity of exposition. All our results can be adapted (scaled) to the case when $\eta$ is
%%   $b$-sub-Gaussian for $b > 0$.}. 
%
%As a corollary to Lemma \ref{lemma:cptdiff}, it is easy to infer that for nonnegative $a$, we have that $\mu_{X+a} \geq \mu_X$, i.e., $\mu$ preserves order under translations of random variables. As a consequence of this fact, the WOFUL algorithm reduces to the LinUCB algorithm in the standard linear bandit setting where the noise in the stochastic costs do not depend on the played arm. The reduction is in the sense that both WOFUL and regular LinUCB algorithms choose the same sequence of arms at time instants $1,\ldots,n$, assuming the same noise values $\eta_1,\ldots,\eta_n$. This is because the arm $x_m$ chosen at instant $m$ is impacted by the ordering of the arms and both weight-distorted value $\mu_x(\theta')$ and $x\tr\theta'$ result in the same order for the arms due to the aforementioned fact regarding translation for $\mu$.