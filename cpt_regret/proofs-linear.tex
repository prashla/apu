We establish a few technical results in the following lemmas that are necessary for the proof of Theorem \ref{thm:linear-bandit-regret}. 
The first step in the proof of Theorem \ref{thm:linear-bandit-regret} is to bound the instantaneous regret $r_m$ at instant $m$, which is the difference in weight-distorted value of arm $x_*$ and the arm $x_{m}$ chosen by the algorithm. For bounding the instantaneous regret, it is necessary to relate the difference in weight-distorted values of $x_*$ and $x_{m}$ to the difference in their means, i.e., $(x_*\tr\theta - x_{m}\tr\theta)$, and Lemma \ref{lemma:cptdiff} provides this connection.

%In the following sections, we use the notation $F_X$, for a random variable $X$, to denote the cumulative distribution function of $X$.

%%%%%%%%%%%%%%%% From notes.tex
\subsection{Proof of Lemma \ref{lemma:cptdiff}}
% 
% Recall that, for a random variable $X$, the weight-distorted value with respect to a weight function $w: [0,1] \mapsto [0,1]$ is defined to be 
% \[ \mu_{w,X} \equiv \mu_X := \intinfinity w(\prob{X > z}) dz - \intinfinity w(\prob{-X > z}) dz.   \]
% 
% \begin{lemma}
% For any $a \in \R$, 
% \[ \mu_{X+a} = \mu_X + \int_{-a}^0 \left[ w(\prob{X > u}) + w(\prob{X < u})  \right] du. \]
% \end{lemma}

\begin{proof}
	\begin{align*}
		\mu_{X+a} &= \intinfinity w(\prob{X+a > z}) dz - \intinfinity w(\prob{-X-a > z}) dz \\
		&= \intinfinity w(\prob{X > z-a}) dz - \intinfinity w(\prob{-X > z+a}) dz \\
		&= \int_{-a}^\infty w(\prob{X > u}) du - \int_{a}^\infty w(\prob{-X > u}) du \\
		&= \int_{-a}^0 w(\prob{X > u}) du - \int_{a}^0 w(\prob{-X > u}) du + \mu_X \\
		&= \int_{-a}^0 w(\prob{X > u}) du + \int_{-a}^0 w(\prob{X < v}) dv + \mu_X.
	\end{align*}
	This proves the main claim. Since $w$ is bounded by 1, it is easy to infer that $|\mu_{X+a} - \mu_X| \leq 2|a|$.
\end{proof}

%%%%%%%%%%%%%% End: notes.tex

\begin{lemma}
\label{lemma:confidenceellipsoid}
Under the hypotheses of Theorem \ref{thm:linear-bandit-regret}, for any $\delta>0$, the following event occurs with probability at least $1-\delta$: $\forall m \ge 1$, $\theta$ lies in the set $C_m$ defined in Algorithm \ref{alg:WOFUL}, i.e., 
\begin{align*}
	C_{m}\! &= \!\left\{\theta \in \mathbb{R}^d: \norm{\theta - \hat{\theta}_m}_{A_m} \leq D_m \right\}, \quad \mbox{where} \\
	D_m \!&=\! \sqrt{2 \log \left(\frac{\det(A_m)^{1/2} \det(\lambda I_{d \times d})^{1/2}}{\delta} \right)} + \beta\sqrt{\lambda}, \\
	A_m &= \lambda I_{d \times d} + \sum_{l=1}^{m-1} \frac{x_l x_l\tr}{\norm{x_l}^2}, \\
	\hat{\theta}_m &= A_m^{-1} b_m, \quad \mbox{and} \quad b_m = \sum_{l=1}^{m-1} \frac{c_l x_k}{\norm{x_l}}.
\end{align*}
\end{lemma}
\begin{proof}
The proof is a consequence of \cite[Theorem 2]{abbasi2011improved}. Indeed, the hypotheses of the stated theorem are satisfied with the sub-Gaussianity parameter $R = 1$ since by (\ref{eq:linban-depnoise}), the stochastic cost $c_m$ normalised by $\norm{x_m}$ at each instant satisfies
\[ \frac{c_m}{\norm{x_m}} = \frac{x_m}{\norm{x_m}} \tr \theta + \frac{x_m}{\norm{x_m}} \tr N_m, \]
whose distribution is Gaussian with mean $\frac{x_m}{\norm{x_m}} \tr \theta$ and variance $1$, and which is thus sub-Gaussian with parameter $R = 1$. 
\end{proof}

\subsection*{Proof of Theorem \ref{thm:linear-bandit-regret}}

We first prove the following lemma which we will need in the proof of Theorem \ref{thm:linear-bandit-regret}. It relates the weight-distorted value $\mu_x(\theta)$ of the stochastic cost from an arm $x \in \X$ to the weight-distorted value of a translated standard normal distribution. 

\begin{lemma}[Weight-distorted value under scaling]
\label{lem:CPTscaling}
If $Z$ denotes a standard normal random variable, then for any arm $x$ and $\theta$, \[ \mu_{x}(\theta) = \norm{x} \cdot \mu_{Z + \frac{x\tr\theta}{\norm{x}}}, \]
where $\mu_{Y}$ denotes the weight-distorted value of a random variable $Y$.
\end{lemma}

\begin{proof}
Let $X$ denote the stochastic cost from arm $x$ with $\theta$ as the parameter; we have that $X$ is normal with mean $x\tr\theta$ and variance $\norm{x}^2$. With $\stdnormal$ being a standard normal $d$-dimensional vector and $\hat x = \frac{x}{\norm{x}}$, we can write
	\begin{align*}  
	 \mu_X &:= \intinfinity w(\prob{X > z}) dz - \intinfinity w(\prob{-X > z}) dz\\
	 & =  \intinfinity w(\prob{x_{m}\tr\theta + x_m\tr\stdnormal > z}) dz - \intinfinity w(\prob{-x_{m}\tr\theta - x_m\tr\stdnormal > z})dz\\
	 & = \intinfinity w(\prob{\hat x_{m}\tr\theta + \hat x_m\tr\stdnormal > \frac{z}{\norm{x_m}}}) dz - \intinfinity w(\prob{-\hat x_{m}\tr\theta - \hat x_m\tr\stdnormal > \frac{z}{\norm{x_m}}})dz\\
	 &= \norm{x_m}\left(\intinfinity w(\prob{\hat x_{m}\tr\theta + \hat x_m\tr\stdnormal > y}) dy - \intinfinity w(\prob{-\hat x_{m}\tr\theta - \hat x_m\tr\stdnormal > y})dy\right)\\
	 &= \norm{x_m} \mu_{Z + \hat x_{m}\tr\theta};
	\end{align*}
\end{proof}

\begin{proof}[Proof of Theorem \ref{thm:linear-bandit-regret}]
Let $r_m =  \mu_{x_{m}}(\theta) -\mu_{x_*}(\theta)$ denote the instantaneous regret incurred by the algorithm that chooses arm $x_{m}\in \X$ in round $m$.
Let $\arm_x$ (resp. $P_x$) denote the r.v. (resp. probability function) governing the rewards of the arm $x \in \X$.

Letting $\hat x_m = \frac{x_m}{\norm{x_m}}$ and $W$ to be a standard Gaussian r.v.,  we upper-bound the instantaneous regret $r_m$ at round $m$ as follows: 
\begin{align}
r_m &=  \mu_{x_{m}}(\theta) - \mu_{x_*}(\theta)  \nonumber\\
& \le \mu_{x_{m}}(\theta) - \mu_{x_m}(\tilde\theta_m) \label{eq:rm1}\\
& = \norm{x_m} \left( \mu_{W+ \hat x_{m}\tr\theta} - \mu_{W+ \hat x_{m}\tr\tilde\theta_m} \right) \label{eq:rm2}\\
& \le 2 \norm{x_m} \left| \hat x_{m}\tr(\theta-\tilde\theta_m) \right| \label{eq:rm3}\\
& = 2 \left| x_{m}\tr(\theta-\tilde\theta_m) \right| \label{eq:rm4}\\
& \le 2 \left(\left| x_{m}\tr(\hat\theta_{m} - \theta)\right|  + \left| x_{m}\tr(\tilde\theta_m - \hat\theta_{m})\right|\right)  \label{eq:rm5} \\
& = 2 \left(\norm{\hat\theta_{m} - \theta}_{A_m}  \norm{x_{m}}_{A_m^{-1}}  + \norm{\tilde\theta_m - \hat\theta_{m}}_{A_m}   \norm{x_{m}}_{A_m^{-1}} \right)  \label{eq:rm6} \\
& \le 4 w_m \sqrt{D_m}  \quad \text{whenever }\theta \in C_m, \label{eq:rm7}
\end{align} 
where $w_m = \sqrt{x_m\tr A_m^{-1}x_m}$ and $D_m$ is as defined in Algorithm \ref{alg:WOFUL}.
The inequalities above are derived as follows:
\begin{itemize}
	\item \eqref{eq:rm1} follows from the choice of $x_{m}$ and $\tilde \theta_m$ in Algorithm \ref{alg:WOFUL};
	\item \eqref{eq:rm2} follows from the scaling lemma (Lemma \ref{lem:CPTscaling});
	\item \eqref{eq:rm3} follows by applying the translation lemma (Lemma \ref{lemma:cptdiff}); 
	%with the r.v. $X = W + x_m\tr \theta$ 
	%(note that $x \mapsto \prob{X > x}$ is integrable over $[0, \infty)$ since $\eta_1$ is sub-Gaussian) 
	%and $a$ = 2, an upper bound on the absolute differences between the expected rewards of arms;
	\item \eqref{eq:rm4} follows from the definition of $\hat x_{m}$;
	\item \eqref{eq:rm6} is by the Cauchy-Schwarz inequality applied to the pair of dual norms $\norm{\cdot}_{A_m}$ and $\norm{\cdot}_{A_m^{-1}}$ on $\R^d$;
%	
%	follows by first noticing that 
%	\begin{align*}
%	\left| x_{m}\tr(\hat\theta_{m} - \theta)\right| &= \left| (\hat\theta_{m} - \theta)\tr A_m^{1/2} A_m^{-1/2} x_{m}\right|
%	 = \left| \left(A_m^{1/2}(\hat\theta_{m} - \theta)\right)\tr  A_m^{-1/2} x_{m}\right| \le \norm{A_m^{1/2}(\hat\theta_{m} - \theta)}\norm{A_m^{-1/2} x_{m}},
%	\end{align*}
%where the last inequality uses Cauchy-Schwarz; 
	\item \eqref{eq:rm7} follows from the definition of the confidence ellipsoid $C_m$ in Algorithm \ref{alg:WOFUL}, as $\hat\theta_{m}$, $\tilde \theta_m$ and $\theta$ belong to $C_m$. Note that Lemma \ref{lemma:confidenceellipsoid} guarantees that this is true with probability at least $1-\delta$.
\end{itemize}

The cumulative regret $R_n$ of WOFUL can now be bounded as follows. With probability at least $1-\delta$, 
\begin{align}
R_n &= \sum_{m=1}^n r_m \nonumber\\
& \le  \sum_{m=1}^n 4 \sqrt{D_m} w_m \nonumber \\
& \le 4 \sqrt{D_n}  \sum_{m=1}^n w_m \nonumber \\
& \le 4 \sqrt{D_n}  \left(n \sum_{m=1}^n w_m^2\right)^{1/2} \label{eq:Rn3}\\
& \le \sqrt{32 d n D_n \log n}. \label{eq:Rn4}
%& \le C (d \log (\lambda + nL/d))^{\alpha/2} n^{\frac{2-\alpha}{2}}.  \text{ for some } C<\infty. \label{eq:Rn4}
\end{align} 
Inequality \eqref{eq:Rn3} follows by an application of Cauchy-Schwarz inequality, while the inequality in \eqref{eq:Rn4} follows from (the standard by now) Lemma 9 in \cite{dani2008stochastic}, which shows that $\sum_{m=1}^n w_m^2 \le  2d \log n$. 
The claim follows.
\end{proof}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
