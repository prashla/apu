\begin{figure}[h]
\centering
% \includegraphics[scale=0.42]{a-typical-weight-function.eps}
\scalebox{0.75}{\begin{tikzpicture}
  \begin{axis}[width=10cm,height=6cm,legend pos=south east,
           grid = major,
           grid style={dashed, gray!30},
           xmin=0,     % start the diagram at this x-coordinate
           xmax=1,    % end   the diagram at this x-coordinate
           ymin=0,     % start the diagram at this y-coordinate
           ymax=1,   % end   the diagram at this y-coordinate
           axis background/.style={fill=white},
           ylabel={\large Weight $\bm{w(p)}$},
           xlabel={\large Probability $\bm{p}$},
%            legend pos=outer north east,
           ]
          \addplot[domain=0:1, red, thick,smooth,samples=1500] 
             {pow(x,0.61)/pow((pow(x,0.61) + pow(1-x,0.61)),1.63)};
             \addlegendentry{CPT weight};
                 \addplot[domain=0:1, black, thick, dashed]           {x};
                 \addlegendentry{Identity};
  \end{axis}
  \end{tikzpicture}}
\caption{A typical weight function that overweights low probabilities and underweights high probabilities. We have used  $w(p)= \frac{p^{0.61}}{(p^{0.61} + (1-p)^{0.61})^{1/0.61}}$, recommended by Tversky and Kahneman \cite{tversky1992advances}.}
\label{Fig:a-typical-weight-function}
\end{figure}

\section{CPT Preliminaries}
\label{Sec:Preliminaries}
We begin by defining the CPT-value of a random variable (r.v.) $X$, a functional of its cumulative distribution function (CDF) $F_X(\cdot).$ Let $w:~[0,1]~\rightarrow~[0,1]$ be a (given) weight function such that $w(0) = 0$ and $w(1) = 1.$ The CPT-value $\mu_X$ for a r.v. $X$ is defined as
\begin{equation}
\label{eq:CPT1}
\mu_X = \int\limits_{0}^\infty w \left( \mathbb{P} \left[ X > z \right] \right) dz -\! \int\limits_{0}^\infty w \left( \mathbb{P} \left[ -X > z \right] \right) dz. 
\end{equation}
It is easy to see that, if we take $w(x) = x, \,\, \forall x \in [0,1],$ i.e., the identity function, then we get $\mu_X = \mathbb{E}(X).$ Hence, the definition~\eqref{eq:CPT1} generalizes standard expected value.
On the other hand, a non-trivial choice for the weight function can transform the probabilities in a non-linear manner leading to a significant difference between CPT-value and the classic expected value. Based on experiments involving humans, CPT suggests using a weight function which inflates low probabilities and deflates high probabilities, as illustrated in Figure \ref{Fig:a-typical-weight-function}. 

\begin{algorithm}  
\caption{CPT-value estimator}
\label{alg:cpt-estimator}
\begin{algorithmic}
\State {\bfseries Input:} $l$ i.i.d samples, $\left( X_t \right)_{t=1}^l,$ drawn from $F_X(\cdot),$ weight function $w$
\State Sort the samples in ascending order as follows: $X_{[1]} \leq X_{[2]} \leq \dots X_{[l_b]} \leq 0 \leq X_{[l_b+1]} \leq \dots \leq X_{[l]},$ where $l_b \in \lbrace 1, 2, \dots, l \rbrace.$   
\State Let $ \hat{\mu}_{X, l}^+  := \sum\limits_{k=l_b+1}^l X_{[k]}\left[ w \left( \frac{l+1-k}{l} \right) - w \left( \frac{l-k}{l} \right) \right]$ and  
\State $\hat{\mu}_{X, l}^-  := \sum\limits_{k=1}^{l_b} X_{[k]}\left[ w \left( \frac{k - 1}{l} \right) - w \left( \frac{k}{l} \right) \right].$ 
\State $\text{Estimate of CPT-value of $X$}: \hat{\mu}_{X, l} = \hat{\mu}_{X, l}^+ - \hat{\mu}_{X, l}^-.$
\State {\bfseries Output:} Return $\hat{\mu}_{X, l}$.
\end{algorithmic}
\end{algorithm}

For convenience, we briefly recall a CPT-value estimation scheme, originally proposed by Prashanth et al~\cite{prashanth2015cumulative}, as Algorithm~\ref{alg:cpt-estimator}. Note that, $\hat{\mu}_{X, l}^+$ and $\hat{\mu}_{X, l}^-$ are simply the empirical estimates of the first and second integrals in the definition of CPT-value \eqref{eq:CPT1}. Under a H\"older-continuity assumption on the weight function (see [A1] in Section~\ref{sec-k-armed-bandits}), the authors in \cite{prashanth2015cumulative} established that this estimator requires $O \left( 1/ \epsilon^{(2/\alpha)} \right)$ samples to estimate the CPT-value up to accuracy $\epsilon > 0,$ where $\alpha$ is the H\"older exponent of the weight function. 
