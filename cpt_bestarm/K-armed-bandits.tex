\section{BCAI under $K$-armed bandits}
\label{sec-k-armed-bandits}
In this section, we study the BCAI problem for $K$-armed bandits under the fixed confidence and fixed budget settings. We assume that the observations are bounded by $M > 0$ almost surely\footnote{This models a variety of settings where each arms' distributions are supported on a bounded set, e.g., the well-studied Bernoulli-armed bandit where $M = 1$.}.   In this section, we assume that the weight function $w$, which is used in defining the CPT-value \eqref{eq:CPT1}, satisfies the following assumption:\\[0.5ex]
\noindent\textbf{(A1).} The CPT weight function $w(\cdot)$ is H\"older continuous with parameters $L>0$ and exponent $\alpha \in (0, 1],$ i.e., $\sup\limits_{x \neq y} \frac{\vert w(x)-w(y) \vert}{\vert x-y \vert^\alpha} \leq L.$
%\begin{equation}
%\sup\limits_{x \neq y} \frac{\vert w(x)-w(y) \vert}{\vert x-y \vert^\alpha} \leq L.
%\end{equation}
\subsection{BCAI with Fixed Confidence}  
We introduce the CPT-Lower Upper Confidence Bound (CPT-LUCB) algorithm, which is a non-trivial adaptation of the well-known LUCB algorithm~\cite{kalyanakrishnan2012pac}, for best expected-value arm identification. Unlike the latter algorithm, CPT-LUCB incorporates CPT-value estimates and confidence intervals for arms to find the arm with the highest CPT-value. 

Algorithm~\ref{alg:cpt-lucb} presents the pseudocode of CPT-LUCB. 
A high-level description of the CPT-LUCB algorithm is as follows: In any round $t$, we calculate Lower Confidence Bound (LCB) and Upper Confidence Bound (UCB) on the CPT values of each arm, obtained by adding and subtracting a confidence term $\left( \gamma_{(\cdot), (\cdot)} \right)$ to the CPT-value estimate $\left( \hat{\mu}_{(\cdot), (\cdot)} \right)$ of the arm. Then, we find the arm, denoted $h_t$ in Algorithm \ref{alg:cpt-lucb}, with the highest CPT-value estimate and its LCB.  Next, we find the arm, say $l_t$, which has the highest UCB among all arms excluding $h_t$. We sample from the distributions of $h_t$ and $l_t$, unless $LCB(h_t) > UCB(l_t)$, when the algorithm terminates to return $h_t$. This is justified because the CPT-value of $h_t$ lies within its confidence interval with high probability and none of the other arms can possibly have a higher CPT-value, since their confidence intervals do not intersect with that of $h_t$.  
% If the above calculated LCB is greater than the highest UCB, we stop the algorithm, and return the arm with the highest CPT-value estimate as the optimal arm. Otherwise, we choose the arms corresponding to the highest CPT-value estimate and the highest UCB mentioned above. 

%For a given H\"older continuous weight function, $K$-armed bandit problem and $\delta \in (0, 1),$ let $\gamma_{t,s} = LM \left[ \frac{1}{2s} \log \left( \frac{t^4 K a}{\delta} \right) \right]^{\alpha/2}$ where $a = \pi^2/3 + 1$ is a universal constant. %and  
%\begin{equation}
%\label{eq:H-alpha}
%H_\alpha = \sum\limits_{i \in \mathcal{A}} \left( \frac{2LM}{\Delta_i} \right)^{2/\alpha}.
%\end{equation} 

\begin{algorithm}[!h]  
\caption{CPT-LUCB}
\label{alg:cpt-lucb}
\begin{algorithmic}
\State {\bfseries Input:} $\delta \in (0, 1)$,  weight function $w$
\State {\bfseries Initialization:} 
Sample each arm once. 
\For{$t=K+1, K+2, \dots$}
\State  ETop$_t = h_t = \arg \max\limits_{i \in \mathcal{A}} \hat{\mu}_{i, T_i(t-1)}$,
where $T_i(t-1)$ denotes the number of times the algorithm has chosen arm $i$ up to round $t,$ and $\hat{\mu}_{i, T_i(t-1)}$ is the CPT-value estimate given by Algorithm \ref{alg:cpt-estimator} 
\State  EBot$_t = \mathcal{A} \setminus$ ETop$_t$ 
\State  $l_t = \arg \max\limits_{i \in EBot_t} \hat{\mu}_{i, T_i(t-1)} + \gamma_{t, T_i(t-1)}$,
where  %$\hat{\mu}_{i, T_i(t-1)}$ is the CPT-value estimate given by Algorithm \ref{alg:cpt-estimator} and 
$\gamma_{t,T_i(t-1)} = LM \left[ \frac{1}{2T_i(t-1)} \log \left( \frac{t^4 K \left(\pi^2/3 + 1\right)}{\delta} \right) \right]^{\alpha/2}$ is the confidence term.
%\Comment{{\tt here, $\hat{\mu}_{i, T_i(t-1)}$ is calculated by using Algorithm \ref{alg:cpt-estimator}}}
\If{\small{$\left( \hat{\mu}_{l_t, T_{l_t}(t-1)}\! +\! \gamma_{t, T_{l_t}(t-1)} \!<\! \hat{\mu}_{h_t, T_{h_t}(t-1)} \!-\! \gamma_{t, T_{h_t}(t-1)} \right)$} \normalsize} 
\State  Terminate and output $h_t$
\Else
\State  Sample $h_t$ and $l_t$
\EndIf
\EndFor
\State {\bfseries Output:} Return $h_t$.
\end{algorithmic}
\end{algorithm}
%\todoa[inline]{RK: Can $ETop_t$ and $EBot_t$ be non-italicized in Alg. 2?}

%\subsubsection{Main results}
The main results concerning CPT-LUCB algorithm are given below.
\begin{theorem}[\textbf{\textit{Correctness of CPT-LUCB}}]
\label{Thm:correctness}
Assume (A1) and let $\delta \in (0, 1)$. If the CPT-LUCB algorithm terminates, then
\begin{equation*}
\mathbb{P} \left( \text{returning a sub-optimal arm} \right) < \delta.
\end{equation*}
\end{theorem}
%\begin{proof}
%Refer Appendix~\ref{sec:appendix-proofs-fc}.
%\end{proof}
%Let $H^t$ be the information available with the algorithm in round-$t$ \emph{i.e.,} it contains all chosen arms and the corresponding observations from round 1 to $n,$ and any external randomization.\\
%\emph{Remark 1.} Let $H^t$ be the information available with the algorithm in round-$t.$ Theorem~\ref{Thm:correctness} holds for more generic policies than the CPT-LUCB algorithm. It holds for any CPT-LUCB style algorithm with arbitrary sampling strategy $S: H^t \rightarrow \mathcal{A}$ and any $\gamma_{t,s}$ such that $\sum\limits_{t=1}^\infty \sum\limits_{s=1}^t 2\exp \left( -2s \left( \frac{\gamma_{t,s}}{LM} \right)^{2/\alpha} \right) < \frac{\delta}{K}.$  

%It is easy to verify that the $\gamma_{t,s}$ used in the CPT-LUCB algorithm satisfies equation~\ref{eq:confbnd_condition}, and hence the above theorem holds for CPT-LUCB algorithm. 
%Sample complexity of an algorithm is defined as the number of rounds it takes to terminate on a given sample path. Let $N^{CL}$ be a r.v. which denotes the sample complexity of CPT-LUCB algorithm. The following result presents an upper bound on the expected sample complexity of CPT-LUCB algorithm. 
\begin{theorem}[\textbf{\textit{Sample complexity bound}}]
\label{Thm:LUCB6}
For a given $\delta \in (0,1)$, let $N^{CL}$ denote the time instant when CPT-LUCB terminates. Then,
\begin{equation*}
\mathbb{E} \left[ N^{CL} \right] \!=\! O \left(\! H_\alpha \log \frac{H_\alpha}{\delta} \!\right),\text{where} \, H_\alpha \!=\! \sum\limits_{i \in \mathcal{A}} \left[ \frac{2LM}{\Delta_i} \right]^\frac{2}{\alpha}.
\end{equation*}  
%where $H_\alpha = \sum\limits_{i \in \mathcal{A}} \left( \frac{2LM}{\Delta_i} \right)^{2/\alpha}.$ %is given in equation~\eqref{eq:H-alpha}. 
\end{theorem}
%\begin{proof}
%Refer Appendix \ref{sec:appendix-proofs-fc}.
%\end{proof}
Theorems \ref{Thm:correctness}--\ref{Thm:LUCB6} together imply that, for any given $\delta \in (0, 1),$ CPT-LUCB uses $N^{CL}$ number of samples (across arms) in expectation before terminating and returns the optimal arm with probability at least $(1-\delta)$.

The key principle behind the algorithm and its performance guarantees (Theorems \ref{Thm:correctness} and \ref{Thm:LUCB6}) is as follows: In any round, we design the LCB and UCB so that at any time, the probability that the CPT-value of an arm lies below (resp. above) its UCB (resp. LCB) is overwhelming high. Note that standard concentration of measure bounds for the sample mean as an estimator of the expected value (such as Hoeffding's inequality) do not help to control estimates of the more complex CPT-value functional. Hence, we leverage a concentration inequality for the CPT-value estimate of Algorithm \ref{alg:cpt-estimator} \cite{aditya2016weighted}, relying on the Dvoretzky-Kiefer-Wolfowitz (DKW) inequality for concentration of the empirical CDF around the true CDF \cite{wasserman2006}.


The classic expected-value optimizing algorithm LUCB requires $O \left( \sum\limits_{i \in \mathcal{A}} 1/ \Delta_i^2 \right)$ number of samples. In comparison, CPT-LUCB requires $O \left( \sum\limits_{i \in \mathcal{A}} \left( L/ \Delta_i\right)^{2/\alpha} \right)$ number of samples to find the arm with the highest CPT-value. If $\alpha=1$, then the complexities match. However, the weight function recommended by CPT (see Figure \ref{Fig:a-typical-weight-function}) has $\alpha < 1$, leading to increased sample complexity for CPT-LUCB. 

A natural question to ask is whether the upper bound on the expected sample complexity of CPT-LUCB algorithm can be improved. We now present an algorithm-independent lower bound on the expected sample complexity that answers in the negative. \\%To proceed further, we need the following assumption which is standard in the literature. \\
%\noindent\textbf{[A2].} For any weight function $w~:~[0,~1]~\rightarrow~[0, 1]$ and any arm $i$ with $\Delta_i > 0,$ the algorithm satisfies $\mathbb{E} [T_i(n)] = o(n^a)$ $ \forall a > 0.$
\begin{theorem}[\textbf{\textit{Lower bound on sample complexity}}]
\label{Thm:FCSBLB}
Let $\delta \in (0, 1).$ For a given $L>0$ and $\alpha \in (0,1),$ there exists a monotonic, increasing, H\"older continuous weight function with parameters $L, \alpha,$ and a set of arm distributions, such that for any algorithm $A$ with success probability at least $1-\delta$,
\begin{equation*}
\mathbb{E} \left[ N^A \right] \geq O \left( H_\alpha \log \left( \frac{1}{2.4 \delta} \right) \right),
\end{equation*} 
where $N^A$ is the sample complexity of algorithm $A.$
\end{theorem}
%\begin{proof}
%Refer Appendix~\ref{sec:appendix-proofs-fc}.
%\end{proof}
 Theorems~\ref{Thm:LUCB6} and~\ref{Thm:FCSBLB} imply that CPT-LUCB is optimal in expected sample complexity up to a factor of $\log \left( H_\alpha \right)$.
\subsection{BCAI with Fixed Budget}
\label{Sec:SBFB}
\begin{algorithm}[t]  
\caption{CPT-SR}
\label{alg:cpt-sr}
\begin{algorithmic}
\State {\bfseries Input:}  Budget $n$ of plays,  weight function $w$ 
\State {\bfseries Initialization:} Let $\mathcal{A}_1 = \mathcal{A},$ $\overline{\log} K = \frac{1}{2} + \sum\limits_{i=2}^K \frac{1}{i}$ and $n_0 = 0.$ 
\For{each phase $k = 1, 2, \cdots K-1 $}
\State Define $n_k$ as follows: $n_k = \Big\lceil \frac{1}{\overline{\log} K} \frac{n-K}{K+1-k} \Big\rceil$
\State Play each arm in $\mathcal{A}_k$ for $(n_k - n_{k-1})$ times 
\State Estimate CPT values $\left( \hat{\mu}_{i, (\cdot)} : i \in \mathcal{A}_k \right)$ for all arms in $\mathcal{A}_k$ using Algorithm~\ref{alg:cpt-estimator}
\State $I_k = \arg\min\limits_{i \in \mathcal{A}_k} \hat{\mu}_{i, (\cdot)}$
%\State $I_k = \arg\min\limits_{i \in \mathcal{A}_k} \text{Estimate of arm $i$'s CPT-value}$ 
\State Set $\mathcal{A}_{k+1} = \mathcal{A}_k \setminus I_k$
\EndFor
\State {\bfseries Output:} Return $J_n = \mathcal{A}_K.$
\end{algorithmic}
\end{algorithm}
%\todoa[inline]{The 3rd and 4th lines in the for loop of CPT-SR need to be made mathematically precise, as in CPT-LUCB}

We begin by introducing the CPT-Successive Rejects (CPT-SR) algorithm for the BCAI problem with a fixed budget (Algorithm~\ref{alg:cpt-sr}), inspired from the Successive Rejects (SR) algorithm of Audibert and Bubeck~\cite{audibert2010best}.  A high level idea of the CPT-SR algorithm is as follows: We divide the given fixed budget into $K-1$ phases. In each phase, we play all available arms equal number of times and eliminate the arm with the lowest CPT-value estimate. Note that, the phase lengths are designed such that the optimal arm survives across all $(K-1)$ phases with overwhelming probability. Despite the phase lengths in both algorithms being the same, an arm which will get eliminated at the end of a phase in the CPT-SR algorithm depends on CPT-value estimates of arms rather than empirical means of arms.

%Let $J_n$ be a random variable which denotes the index of the arm returned by the CPT-SR algorithm after a budget of $n$ rounds. 
The following result provides an  upper bound on the probability of incorrect identification by CPT-SR. 

%Recall that, w.l.o.g we assumed that arm-1 is the optimal arm, and $\mu_1 > \mu_2 \geq \mu_3 \cdots, \mu_K.$ Let $\Delta_i = \mu_1 - \mu_i$ for $i \geq 2,$ and 
%\begin{equation}
%H_{2, \alpha} = \max\limits_{i\neq 1} \frac{i}{\Delta_i^{2/\alpha}}.
%\end{equation}
\begin{theorem}[\textbf{\textit{Probability of error bound}}]
\label{Thm:SR1}
For a given budget $n \in \mathbb{N},$ the arm $J_n$ returned by the CPT-SR algorithm satisfies:
%the probability of error of CPT-SR algorithm satisfies:
\begin{equation*}
\mathbb{P} \left( J_n \neq 1 \right) \leq \frac{K(K-1)}{2} \exp \left( - \frac{n-K}{\overline{\log} K} \frac{1}{M^{2/\alpha}} \frac{1}{H_{2, \alpha}} \right),
\end{equation*}
where $H_{2, \alpha} = \max\limits_{i\neq 1} i\left( \frac{L}{\Delta_i} \right)^{2/\alpha}.$
\end{theorem}
%\todoa[inline]{What is $\bar{\log}$? Doesn't seem to be defined}
%\begin{proof}
%Refer Appendix~\ref{sec:appendix-proofs-fb}.
%\end{proof}
Notice that the dependence on the underlying gaps in $H_{2, \alpha}$ is $O\left(1/\Delta_2^{2/\alpha}\right)$, while the corresponding complexity term in classic SR is $O\left(1/\Delta_2^2\right)$.  
In order to show that the dependence of CPT-SR on the underlying gaps is optimal, we proceed to present a lower bound on the probability of incorrect identification. To that end, we construct $K$ transformations of a $K$-armed bandit, and show that any algorithm must return a sub-optimal arm with a sufficiently high probability on at least one of these transformations.  

We consider a $K$-armed Bernoulli bandit problem with $p_1 = 1/2, p_j \in [1/4, 1/2)$ for $2 \leq j \leq K$.
Following the measure change technique from~\cite{kaufmann2015complexity}, we consider $K$ transformations of a $K$-armed bandit problem. 
%In the $i^{th}$ transformation, arm $i$'s distribution is modified such that it has the highest CPT-value. 
For $1 \leq i \leq K,$ the transformation-$i$ corresponds to a $K$-armed bandit with Bernoulli arm distributions with parameters $p_1, p_2, \cdots, p_{i-1}, 1 - p_i, p_{i+1}, \cdots p_K$. We denote transformation-$i$ as $MAB_i.$ It is easy to see that for a H\"older continuous and monotonic increasing weight function $w(\cdot)$, the CPT of a Bernoulli random variable with parameter $p$ is $w(p)$. Hence, arm $i$ is the optimal arm under $MAB_i.$ Let $\mathbb{P}_i$ and $\mathbb{E}_i$ denote the joint probability distribution and expectation under $MAB_{i}.$ 
%Consider the following $K$ number of $K$-armed bandit problems. For $1 \leq i \leq K,$ the bandit problem-$i$ corresponds to a $K$-armed bandit with Bernoulli arm distributions with parameters $p_1, p_2, \cdots, p_{i-1}, 1 - p_i, p_{i+1}, \cdots p_K$ such that $p_1 = 1/2, p_j \in [1/4, 1/2)$ for $2 \leq j \leq K.$ We denote bandit problem-$i$ as $MAB_i.$ It is easy to see that for a Holder continuous and monotonic increasing weight function $w(\cdot)$, the CPT of a Bernoulli random variable with parameter $p$ is $w(p)$. Hence, arm-$i$ is the best CPT arm under $MAB_i.$ Let $\mathbb{P}_i$ and $\mathbb{E}_i$ denote the joint probability distribution and expectation under $MAB_{i}.$ 

Let $\left( \Delta^i_k \right)_k$ denotes the gaps between the best CPT arm and arm $k$ under $MAB_i,$ which are defined as follows: 
\begin{align*}
&\Delta^i_k := 
\begin{cases}
1-p_i - p_k, \hspace{0.85cm} \text{for $i \neq k,$}\\
1/2 - p_2,  \hspace{1.25cm} \text{for $i=k=1,$} \\
1/2 - p_i, \hspace{1.25cm} \text{for $2 \leq i=k \leq K.$}
\end{cases}
\\
&\textrm{Let } H(i,\alpha) = \sum\limits_{1 \leq k \leq K, k\neq i} \left( \frac{L}{\Delta^i_k} \right)^{2/\alpha}, \\ 
&H_{1,\alpha} \!=\! \max\limits_i H(i, \alpha), \text{ and }
h^* \!=\! \sum\limits_{2 \leq i \leq K} \frac{1}{H(i,\alpha)} \left(\frac{ L}{\Delta^1_i} \right)^{\frac{2}{\alpha}}.
\end{align*}
It is easy to verify that, $H_{1, \alpha} \leq H_{2,\alpha} \leq 2H_{1,\alpha}.$ In the following result, we provide a lower bound on the probability of returning a sub-optimal arm by any algorithm on one of the $K$ number of transformations described above.
\begin{theorem}[\textbf{\textit{Lower bound on probability of error}}]
\label{Thm:FBSBLB1}
Let $n$ be the given budget. Then, there exists a H\"older continuous weight function with parameters $\alpha$ and $L$ and arm distributions bounded by $M$ such that any algorithm for the BCAI problem under the fixed budget satisfies the following:
\begin{align*}
\!\max\limits_{1 \leq i \leq K}\! \mathbb{P}_i (J_n \neq i) &\geq \frac{1}{6} \exp\!\left[ \frac{-60 n}{H_{1,\alpha}} - 2 \sqrt{n \log(6nK)} \right], \\
\!\max\limits_{1 \leq i \leq K}\! \mathbb{P}_i (J_n \neq i) &\geq \frac{1}{6} \exp\!\left[ \frac{-60 n}{h^*\! H(i,\alpha)} \!-\! 2 \sqrt{n \log(6nK)} \right]\!.
\end{align*}
\end{theorem}
%\begin{proof}
%Refer Appendix~\ref{sec:appendix-proofs-fb}.
%\end{proof}
A key observation from Theorems~\ref{Thm:SR1} and \ref{Thm:FBSBLB1} is that CPT-SR algorithm is optimal, since the upper bound in Theorem~\ref{Thm:SR1} and the lower bound in Theorem~\ref{Thm:FBSBLB1} are of the same order, in terms of their dependence on underlying gaps between the optimal and sub-optimal arms.