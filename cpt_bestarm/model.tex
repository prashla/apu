\section{Model}
\label{Sec:Model}
In this section, we  formulate the BCAI problem under $K$-armed and linear bandits, respectively. 
\subsection{BCAI for $K$-armed bandits}
We consider stochastic multi-armed bandit setting with the finite set of arms $\mathcal{A} = \lbrace 1, 2, \cdots, K \rbrace$.  Each arm is associated with a distribution unknown a priori to the agent. Let $\mu_i$ denote the CPT-value of arm $i$ and $a^* = \argmax\limits_{i=1,\ldots,K}~\mu_i$ denote the optimal arm. We assume that $\mu_1 > \mu_2 \geq \cdots \geq \mu_K$, i.e., arm 1 is the unique optimal arm \footnote{The ordering assumption is without loss of generality, and without uniqueness the problem can only get easier.}. For $i \geq 2,$ let $\Delta_i = \mu_1 - \mu_i$ denote the gap between the CPT values of the optimal arm and an arm $i,$ and $\Delta_1 = \mu_1 - \mu_2.$ 


The bandit algorithm interacts with the environment as follows:
In each round $t=1,2,\ldots$, the algorithm chooses an arm $i_t \in \{1,\ldots,K\}$ and receives an independent sample from the distribution corresponding to arm $i_t$. 
%Let $X_{i,k}$ be the observation received by the agent when he chooses arm~$i$ for the $k^{th}$ time. 
%Let $\hat{\mu}_{i,l}$ denote the estimate of $\mu_i$ after having access to $l$ i.i.d. samples of arm $i,$ which is calculated using Algorithm~\ref{alg:cpt-estimator}. 
%To choose an arm in round $t$, the algorithm can use the entire history of arms chosen and samples received prior to round $t$. 
The objective is to find the arm with the highest CPT-value and we study this problem under the following popular variants: 

\vspace{-2ex}

\begin{description}
\item[Fixed Confidence (FC):] Here, a confidence parameter $\delta \in (0,1)$ is given. At each round, an algorithm can either (a) stop and declare an arm as optimal, or (b) continue and choose an arm. The algorithm must ensure that the arm declared upon stopping is indeed optimal with a probability of at least $(1 - \delta),$ and its performance is quantified by the number of rounds before it stops. 

\vspace{-1ex}

\item[Fixed Budget (FB):] Here, a budget of $n$ rounds is given, with the stipulation that the algorithm must declare an arm as optimal at the end of $n$ rounds. The algorithm's goal is to minimize the probability of declaring a sub-optimal arm as optimal.
\end{description}
%In Section~\ref{sec-k-armed-bandits}, we propose algorithms to achieve the goals mentioned under FC and FB variants of the BCAI problem.  
\subsection{BCAI for linear bandits} 
In a linearly parameterised bandit instance, the set of arms is $\mathcal{A} \subseteq \mathbb{R}^d$, with $\vert~\mathcal{A}~\vert~=~K$ (typically, $K \gg 1$).  Let $\theta^* \in \mathbb{R}^d$ be a model parameter which is unknown to the algorithm. Let $r_t$ be the stochastic observation upon choosing arm $a_t \in \mathcal{A}$ in round $t.$ The linear parametric assumption is that\\[0.5ex] 
\centerline{
$r_t \equiv r_t(a_t) = a_t^T \theta^* + \eta_t$,}\\[0.5ex]
where $\eta_t$ (noise) is a standard Gaussian random variable independent of $\eta_1, \eta_2 \cdots, \eta_{t-1}$.
We assume that $\Vert \theta^* \Vert_2 \leq \beta$, for some known upper bound $\beta$.

 For any arm $a \in \mathcal{A}$ and parameter $\theta$, the CPT-value $\mu_a \left( \theta \right)$ is defined using the r.v. $Z = a^T \theta + \eta$, with $\eta \sim \N(0,1)$, as follows:
\begin{align}
\label{eq:CPT-definition-LinBand}
\!\!\mu_a \left( \theta \right) \!\!=\!\! \int\limits_{0}^\infty\! w \left( \mathbb{P} \left( Z > z \right) \right) dz 
-\!\! \int\limits_{0}^\infty \! w \left( \mathbb{P} \left( - Z > z \right) \right) dz,
\end{align}
where $w$ is a weight function as defined earlier in Section \ref{Sec:Preliminaries}. 
 We assume that a unique arm $a^*$ attains the highest CPT-value, i.e., $\argmax\limits_{a \in \mathcal{A}}~\mu_a \left( \theta^* \right) = \{a^*\}.$
%\begin{equation*}
%\arg\max\limits_{a \in \mathcal{A}}~\mu_a \left( \theta^* \right) = \{a^*\}.
%\end{equation*}
%\todoa[inline]{RK: Do not number any equation that is not referenced later. You can make unnumbered eqns. inline to save space.}

We study the BCAI problem under linear bandits in the fixed confidence setting, i.e., for a given $ \delta \in (0, 1),$ the goal is to find the optimal arm with probability at least $(1-\delta)$, while minimizing the number of sample observations.  