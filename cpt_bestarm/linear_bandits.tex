\section{BCAI under Linear bandits}
\label{Sec:LB}
%In this section, we study the BCAI problem for linear bandits under the fixed confidence setting. We first present the development of the proposed CPT-G algorithm in detail. Then, we present the complete CPT-G algorithm and study its performance. Note that, CPT-G algorithm is inspired from the G-allocation algorithm in~\cite{soare2014best}.   
%\subsection{Development of CPT-G algorithm}
In this section, we develop and study an algorithm, called CPT-G, for the BCAI problem under linear bandits in the fixed confidence setting. The CPT-G algorithm is adapted from the G-allocation algorithm in~\cite{soare2014best} to incorporate CPT-based criteria.
\subsection{Notation}
Denote $\cC(a)$ as the set of parameters $\theta \in \mathbb{R}^d$ for which arm~$a$ is the optimal arm, i.e., $\mathcal{C}(a) = \lbrace \theta \in \mathbb{R}^d: \mu_a ( \theta) \geq \mu_{a'}(\theta)\,\, \forall a' \in \mathcal{A} \rbrace.$ Here $\mu_a(\theta)$, defined in \eqref{eq:CPT-definition-LinBand}, is the CPT-value of arm $a$ with underlying parameter $\theta$. For any $x \in \mathbb{R}^d,$ and any positive semi-definite matrix $A$ of size $d\times d,$  we denote $\Vert x \Vert_A$ as $\sqrt{x^T A x}.$ 

Recall that, $\theta^*$ is the true unknown model parameter. Let $\Delta_a = \mu_{a^*}( \theta^*) - \mu_a( \theta^*),$ and $\Delta_{\min} = \min\limits_{a \in \mathcal{A} \setminus a^*} \Delta_a.$ 
We assume that $\Delta_{\min} > 0.$
For a given sequence of arms ${\bf{a_n}} = \lbrace a_t \rbrace_{t=1}^n$ and $\lambda > 0,$ let $A_{\bf{a_n}} =\lambda I_{d\times d} + \sum\limits_{t=1}^n a_t a_t^T.$ For notational convenience, we write $A_{\bf{a_n}}$ as $A_n$ whenever the sequence of actions can be understood from the context. Let $\hat{\theta}_n$ be the ridge regression estimate of $\theta^*$ using $n$ observations, i.e., $\hat{\theta}_n = A_n^{-1} b_n,$ where $b_n = \sum\limits_{t=1}^n a_t r_t.$ Let $\Delta(a, a') = \mu_a(\theta^*) - \mu_{a'}( \theta^*).$ 

Now, we define a confidence ellipsoid $\hat{\mathcal{S}}_n$ for $\theta^*$ by using the observations collected up to round $n$. For a given confidence parameter $ \delta \in (0, 1),$ define 
\begin{equation}
\label{eq:ConfEllipsoid}
\hat{\mathcal{S}}_n = \lbrace \theta \in \mathbb{R}^d : \Vert \hat{\theta}_n - \theta \Vert_{A_n} \leq D_n \rbrace.
\end{equation}
In the above, $D_n = \sqrt{2 \log \left( \frac{\det(A_n)^{1/2} \lambda^{-d/2}}{\delta}\right)}  + \beta \sqrt{\lambda}$, where $\beta$ is an upper bound on $\Vert \theta^* \Vert_2$.  
%It can be shown that $\theta^* \in \hat{\mathcal{S}}_n$ with probability at least $(1-\delta),$ cf.~\cite[Theorem~2]{abbasi2011improved}. 
\subsection{Stopping condition and arm selection in CPT-G}
%We now derive a stopping condition that will be used in the CPT-G algorithm.
%, based on which we develop an arm selection strategy used in the CPT-G algorithm, in any round-$t.$
%\noindent\textbf{Stopping condition:} 
Since $\theta^*$ lies within the confidence ellipsoid $\hat{\mathcal{S}}_n$ with high probability (cf.~\cite[Theorem~2]{abbasi2011improved}), a reasonable condition to stop the algorithm is when the confidence ellipsoid is contained in $\cC(a)$, for some $a \in \mathcal{A}$.  
%We stop the algorithm whenever $\exists a \in \mathcal{A}$ such that $\hat{\mathcal{S}}_n \subseteq \mathcal{C}(a)$ is satisfied in any round-$n,$ and return the arm $a$ as the output. 
If $\hat{\mathcal{S}}_n \subseteq \mathcal{C}(a)$ in some round $n$, then
\[\forall \theta \in \hat{\mathcal{S}}_n, \mu_a(\theta) \geq \mu_{a'}(\theta),\,\, \forall a' \in \mathcal{A},\] 
or equivalently, %$\forall \theta \in \hat{\mathcal{S}}_n,$
\begin{multline}
\label{eq:Stop1}
\!\!\!\!\! \left[ \mu_a(\hat{\theta}_n) - \mu_a(\theta) \right]\! -\! 
\left[ \mu_{a'}(\hat{\theta}_n) - \mu_{a'}(\theta) \right] \leq \hat{\Delta}_n(a, a'),
\end{multline}
where $\hat{\Delta}_n(a, a') = \mu_a( \hat{\theta}_n) - \mu_{a'}(\hat{\theta}_n)$.
Note that, verifying~\eqref{eq:Stop1} is difficult in practice, and hence we derive a simpler sufficient condition as follows:
%In Lemma~5 of~\cite{aditya2016weighted}, the authors have showed an upper bound of $2\vert a \vert$ on the absolute difference between CPT-values of $Z+a$ and $Z,$ where $Z$ is a r.v. and $a \in \mathbb{R}.$ Hence, we can get the following by using that lemma:
%\begin{align*}
%\vert \mu_{a'}(\hat{\theta}^n) - \mu_a(\theta) \vert &\leq \vert a^T \left( \hat{\theta}^n - \theta \right) \vert, \\
%\vert \mu_{a'}(\hat{\theta}^n) - \mu_{a'}(\theta) \vert &\leq \vert a'^T \left( \hat{\theta}^n - \theta \right) \vert.
%\end{align*} 
\begin{align}
& \left[ \mu_a(\hat{\theta}_n) - \mu_a(\theta) \right] - \left[ \mu_{a'}(\hat{\theta}_n) - \mu_{a'}(\theta) \right] \nonumber\\
& \leq \left\vert a^T \left( \hat{\theta}_n - \theta \right) \right\vert + \left\vert a'^T \left( \hat{\theta}_n - \theta \right) \right\vert, \label{eq:lemma5app} \\
& \leq \Vert a \Vert_{A^{-1}_n} \Vert \hat{\theta}_n - \theta \Vert_{A_n} +\Vert a' \Vert_{A^{-1}_n} \Vert \hat{\theta}_n - \theta \Vert_{A_n}, \label{eq:a} \\
& = \left[ \Vert a \Vert_{A^{-1}_n} + \Vert a' \Vert_{A^{-1}_n} \right] \Vert \hat{\theta}_n - \theta \Vert_{A_n}, \label{eq:Stop2}
\end{align}
where \eqref{eq:lemma5app} follows from the fact that $\vert \mu_{Z+a} - \mu_Z \vert \leq 2 \vert a \vert$ for any r.v. $Z$ and $a \in \mathbb{R}$ (cf. \cite[Lemma~5]{aditya2016weighted}), while \eqref{eq:a} is due to Cauchy-Schwartz inequality. From~\eqref{eq:Stop2}, a sufficient condition for~\eqref{eq:Stop1} is given by 
\begin{multline*}
\forall \theta \in \hat{\mathcal{S}}_n, \exists a \in \mathcal{A} \,\, \text{s.t.} \,\, \left[ \Vert a \Vert_{A^{-1}_n} + \Vert a' \Vert_{A^{-1}_n} \right] \Vert \hat{\theta}_n - \theta \Vert_{A_n} \\
\leq \hat{\Delta}_n(a, a'), \,\, \forall a' \in \mathcal{A}
\end{multline*}
Using the fact that $\Vert \hat{\theta}_n - \theta \Vert_{A_n} \leq D_n$ for any $\theta \in \hat{\mathcal{S}}_n,$ we have that condition~\eqref{eq:Stop1} holds if
\begin{multline*}
\exists a \in \mathcal{A} \,\, \text{s.t.} \,\, \left[ \Vert a \Vert_{A^{-1}_n} + \Vert a' \Vert_{A^{-1}_n} \right] D_n \\
 \leq \hat{\Delta}_n(a, a'), \,\, \forall a' \in \mathcal{A}. 
\end{multline*}
The final simplication that implies ~\eqref{eq:Stop1} uses the fact that for any $a \in \mathcal{A},$ $\Vert a \Vert_{A^{-1}_n} \leq \max\limits_{a \in \mathcal{A}} \Vert a \Vert_{A^{-1}_n},$ to obtain 
\begin{equation}
\label{eq:Stop100}
\exists a \in \mathcal{A} \, \text{ s.t. } \, 2\max\limits_{a \in \mathcal{A}} \Vert a \Vert_{A^{-1}_n} D_n \leq \hat{\Delta}_n(a, a'), \, \forall a' \in \mathcal{A}. 
\end{equation}
Our objective is to meet the above stopping condition in minimum possible rounds. In order to achieve that, ideally, we need to choose a sequence of actions which minimizes the left hand side in \eqref{eq:Stop100}, i.e.,  %To that end, we devise CPT-G algorithm as the one which chooses a sequence of actions which minimizes the left hand side in the above equation \emph{i.e.,}\\
%\emph{Arm selection of CPT-G Algorithm:} 
\begin{equation}
\label{eq:arm-selection-cpt-g-1}
{\bf{a_n^G}} = \arg\min\limits_{ {\bf{a_n}} }  \max\limits_{a \in \mathcal{A}} \Vert a \Vert_{A_{\bf{a_n}}^{-1}}.
\end{equation}
The arm selection strategy above is for the first $n$ number of rounds. However, $n$ is not known a priori.  So, we employ an incremental version of the arm selection strategy in \eqref{eq:arm-selection-cpt-g-1}, as shown in Algorithm~\ref{alg:cpt-g}, where we present the pseudocode for CPT-G. 
%Let $a_t$ be the arm chosen in round-$t$ under CPT-G algorithm which is given as follows.\\
%%\noindent\textbf{Arm selection of CPT-G algorithm:} 
%\begin{equation}
%\label{eq:arm-selection-cpt-g-2}
%a_t= \arg\min\limits_{a' \in \mathcal{A} }  \max\limits_{a \in \mathcal{A}} \Vert a \Vert_{\left( A_{\bf{a_{t-1}}} + a' a'^T \right)^{-1}} .
%\end{equation}
%\subsection{CPT-G algorithm and its analysis}
 \begin{algorithm}[h]  
\caption{CPT-G}
\label{alg:cpt-g}
\begin{algorithmic}
\State {\bfseries Input:} $\delta \in (0, 1),$ $\beta,$ $\lambda,$ and CPT weight function $w$
\While{(Stopping condition~\eqref{eq:Stop100} does not hold)}
\State Choose $a_t
= \arg\min\limits_{a' \in \mathcal{A} }  \max\limits_{a \in \mathcal{A}} \Vert a \Vert_{\left( A_{\bf{a_{t-1}}} + a' a'^T \right)^{-1}}.$
\State Receive observation $r_t$. 
\State Update $A_{\bf{a_t}} = A_{\bf{a_{t-1}}} + a_t a_t^T,$ $b_t = b_{t-1} + a_t r_t,$ $\hat{\theta}_t = A_{\bf{a_t}}^{-1} b_t$ and $\hat{\mathcal{S}}_t$ using \eqref{eq:ConfEllipsoid}
\EndWhile
\State {\bfseries Output:} Return arm $a$ which satisfies~\eqref{eq:Stop100}. 
\end{algorithmic}
\end{algorithm}

 We now present an upper bound on the sample complexity of CPT-G algorithm below. %Let $N^G$ be a r.v. which denotes the sample complexity of CPT-G algorithm. Let $\Pi(t)$ be a random variable which denotes the index of the arm returned by the CPT-G at termination in round-$t$. %Sample complexity of an algorithm is defined as the number of rounds it takes to terminate for a given sample path.
\begin{theorem}[\textbf{\textit{Sample complexity bound}}]
\label{Thm:LinBdt1}
For any $\delta \in (0, 1),$ let $N^G$ be the time instant when CPT-G algorithm stops and $\Pi \left( N^G \right)$ be the arm returned at $N^G.$ Then, we have  
%Let $\delta \in (0, 1)$ be the given confidence parameter. For CPT-G algorithm, the following holds:
\begin{equation}
\label{eq:CPT-Gbound}
\mathbb{P} \left( N^G \leq \frac{ c D_n^2 d}{\Delta_{\min}^2}, \,\, \Pi\left( N^G \right) = a^* \right)\geq (1 - \delta),
\end{equation}
where $d$ is the dimension of the space in which $\theta^*$ lies, and $c$ is a universal constant.
\end{theorem}
%\begin{proof}
%Refer Appendix~\ref{sec:appendix-proofs-linear-bandits}.
%\end{proof}
Note that, the upper bound on $N^G$ in~\eqref{eq:CPT-Gbound} is independent of the parameters of the weight function and the number of arms $(K)$, but depends on the dimension of the underlying space. %Also note that, this upper bound is almost same as the upper bound on the sample complexity of G-allocation algorithm in~\cite{soare2014best} for the problem of identifying the arm with the highest mean in linear bandits. 
Also note that, in terms of dependence on the underlying gaps of arms, this upper bound is of the same order as the upper bound on the sample complexity of G-allocation in~\cite{soare2014best} for the problem of identifying the arm with the highest mean in linear bandits. The bounds differ only in  $\Delta_{min}$ term; here it is a function of CPT-values of arms, whereas in~\cite{soare2014best}, it is a function of expected values of arms. 
  



