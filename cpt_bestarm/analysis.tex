\section{Analysis of CPT-LUCB}
\label{sec:analysis}

%\subsection{Proofs for fixed confidence setting}
%\label{sec:proof-fc}
Here, we give a proof sketch for Theorem~\ref{Thm:LUCB6}. Proofs of all other results can be found in Appendix. Recall that, Theorem~\ref{Thm:LUCB6} gives an upper bound on the expected sample complexity of the CPT-LUCB algorithm. Let $Term_t$ denote the event that the algorithm terminates in a round-$t.$The Expression for expected sample complexity is given as follows:
\begin{equation}
\mathbb{E}[N^{CL}] = \mathbb{E} \left[ \sum\limits_{t} \mathbb{I} \lbrace \sim Term_t \rbrace \right] = \sum\limits_t \mathbb{P} \left( \sim Term_t \right).
\end{equation}
We establish Theorem~\ref{Thm:LUCB6} by deriving an upper bound on $\mathbb{P} \left( \sim Term_t \right).$ To that end, we require the following three lemmas. To proceed further, we need the following notation. Let $ c = \frac{\mu_1 + \mu_2}{2}.$ It is easy to see that $\frac{\Delta_a}{2} \leq \vert c - \mu_a \vert \leq \Delta_a$ for any arm $a \in \mathcal{A} = \lbrace 1, 2, \cdots, K \rbrace.$ In round-$t,$ define
\begin{align*}
Above_t &= \lbrace i \in \mathcal{A} : \underbrace{\hat{\mu}_{i,T_i(t-1)} - \gamma_{t,T_i(t-1)}}_{LCB_i} > c \rbrace, \\
 Below_t &= \lbrace i \in \mathcal{A} :  = \underbrace{\hat{\mu}_{i,T_i(t-1)} + \gamma_{t,T_i(t-1)}}_{UCB_i} < c \rbrace, \text{and} \\
 Needy_t & = \mathcal{A} \setminus \left( Above_t \cup  Below_t \right)
\end{align*}
Intuitively, we expect the best CPT arm to lie in $Above_t$ or $Needy_t$ and all non best CPT arms to lie in $Below_t$ or $Needy_t.$ Now, we define an event $Cross_t^i$ which captures the scenario when the aforementioned does not occur. 
$$
Cross^i_t =
\begin{cases}
i \in Below_t \hspace{0.45cm} \text{if arm-$i$ is the best CPT arm},\\
i \in Above_t \hspace{0.45cm} \text{if arm-$i$ is non best CPT arm}
\end{cases}
$$
Let $Cross_t$ be the event when atleast one arm-$i$ satisfies $Cross^i_t.$
\begin{equation}
Cross_t = \exists i \in \mathcal{A} : Cross^i_t \,\,\text{occured}
\end{equation}
\begin{lemma}
\label{Thm:LUCB2}
For any $t > 1,$ CPT-LUCB satisfies the following:
\begin{multline}
\sim Cross_t \cap \sim Term_t \subseteq \\
 \lbrace h_t \in Needy_t \rbrace \cup \lbrace l_t \in Needy_t \rbrace,
\end{multline}
where $h_t$ and $l_t$ are defined in the Algorithm~\ref{alg:cpt-lucb}.
\end{lemma}
\begin{proof}
Refer Appendix~\ref{sec:appendix-proofs-fc}.
\end{proof}
The above lemma shows that if $Cross_t$ does not occur and the CPT-LUCB does not terminate, then either arm $h_t$ or arm $l_t$ is in $Needy_t.$ We also infer that if the algorithm does not terminate in round-$t,$ then either $Cross_t$ occurs or $h_t$ or $l_t$ is in $Needy_t.$ In the next two lemmas, we provide upper bounds on $\mathbb{P} \left( Cross_t \right)$ and $\mathbb{P}  \left( \lbrace h_t \in Needy_t \rbrace \cup \lbrace l_t \in Needy_t \rbrace \right).$  
\begin{lemma}
\label{Thm:LUCB3}
For any $t > 1,$ CPT-LUCB algorithm satisfies the following:
\begin{equation*}
\mathbb{P} \left( Cross_t \right) \leq \frac{2 \delta}{a t^3},
\end{equation*}
where $\delta \in (0, 1)$ and $a = \pi^2/3.$
\end{lemma}
\begin{proof}
Refer Appendix~\ref{sec:appendix-proofs-fc}.
\end{proof}
Let $d_\alpha = \frac{1}{\left( 1 - \left( \frac{1}{4} \right)^{\alpha/2} \right)^{2/\alpha}}.$ For any arm-$i$ and round-$t,$ let $\beta \left( i, t \right) = \lceil \frac{1}{2 d_\alpha} \left( \frac{2LM}{\Delta_i}\right)^{2/\alpha} \log \left( \frac{t^4 K a}{\delta} \right) \rceil.$ Note that, if $s > \beta(i,t),$ then $\gamma_{t,s} \leq \frac{\Delta_i}{2}.$ \\
\begin{lemma}
\label{Thm:LUCB4}
Under CPT-LUCB, 
\begin{multline}
\mathbb{P} \left( \exists i \in \mathcal{A} : T_i(t-1) > 4 \beta (i,t) \cap \lbrace i \in Needy_t \rbrace \right) \\\
\leq \frac{H_\alpha d_\alpha \delta}{2t^4 K a}, 
\end{multline}
where $H_\alpha$ is defined in equation~\eqref{eq:H-alpha}.
\end{lemma}
\begin{proof}
Refer Appendix~\ref{sec:appendix-proofs-fc}.
\end{proof}
In the following result, we upper bound $\mathbb{P} \left( \sim Term_t \right)$ for a sufficiently large~$t,$ using the above results.
\begin{theorem}
\label{Thm:LUCB5}
Under CPT-LUCB and for any $t > 150 H_\alpha \log \left( \frac{H_\alpha}{\delta} \right),$
\begin{equation*}
\mathbb{P} \left( \sim Term_t \right) \leq O \left( \frac{1}{t^2} \right)
\end{equation*}
\end{theorem}
\begin{proof}
Refer Appendix~\ref{sec:appendix-proofs-fc}.
\end{proof}
With the aid of the above result, we present the proof of Theorem~\ref{Thm:LUCB6} here.
\begin{proof}
 Recall that, for any $t > K,$ CPT-LUCB algorithm pulls two arms in any round. Due to Theorem~\ref{Thm:LUCB5}, we get 
\begin{align*}
& \mathbb{E} (\text{Sample complexity} ) \leq \\
&  2 \times 150 H_\alpha \log \left( \frac{H_\alpha}{\delta} \right) + \sum\limits_{t > 150 H_\alpha \log \left( \frac{H_\alpha}{\delta} \right)} O \left( \frac{1}{t^2} \right) \\
& = 300 H_\alpha \log \left( \frac{H_\alpha}{\delta} \right) + \text{constant} = O \left( H_\alpha \log \frac{H_\alpha}{\delta} \right),
\end{align*}
which completes the proof.
\end{proof}
%In the proof of Theorem~\ref{Thm:FCSBLB}, we construct a particular weight function and a $K$-armed bandit then analyse any algorithm's sample complexity on the same. We use change of distributions and few results from~\cite{kaufmann2015complexity} in order to establish the result. 
%\subsection{Proofs for fixed budget setting}
%\label{sec:proofs-fb}

%\subsection{Proofs for linear bandits setting}
%\label{sec:proofs-lb}