In this section, we formally present the models for linear bandits under regret minimization and best arm identification settings. We first introduce the required notation. 

Let $\X \subseteq \mathbb{R}^d$ be the set of arms available to the learner. Let $\theta_* \in \mathbb{R}^d$ be the underlying parameter unknown to the learner. In round-$m,$ if the learner chooses arm $x_m$ from $\X,$ then the learner receives a stochastic reward given by
\begin{align}
r_m := x_m\tr\left(\theta_* + N_m\right),\label{eq:linban-depnoise}
\end{align} 
where $N_m := (N_m^1, \ldots, N_m^d)$ is a vector of i.i.d. standard Gaussian random variables\footnote{Our results continue to hold when the noise distribution is sub-Gaussian --- see Remark~\ref{remark:sub-gauss-linear}.}, independent of the previous vectors $N_1, \ldots, N_{m-1}.$ We assume that the learner does not have the knowledge of $N_m.$ Given a weight function $w: [0,1] \to [0,1]$ with $w(0) = 0$ and $w(1) = 1,$ we define the weight-distorted reward $\mu_x(\theta_*)$ for arm $x \in \X$, with underlying model parameter $\theta_*$, to be the quantity
\begin{align}
\mu_x(\theta_*)  := \intinfinity w(1 - F_x^{\theta_*}(z)) dz - \intinfinity w(F_x^{\theta_*}(-z)) dz, \label{eq:cpt-lb} 
\end{align}
where $F_x^{\theta_*}(z) := \prob{x\tr(\theta_* + N) \leq z}$, $z \in \R$, is the cumulative distribution function of the stochastic reward from playing arm $x \in \X$. An arm $x$ is said to be optimal if its weight-distorted reward equals the highest possible weight-distorted reward achieved across all arms, i.e., if
$ \mu_x = \mu_* :=  \max_{x' \in \X} \mu_{x'}(\theta_*),$ and we use $x_*$ to denote the optimal arm. 
\begin{remark}
For purely notational convenience and ease of readability, we have used the same weight function for both positive and negative rewards in~\eqref{eq:cpt-lb}, but our results still hold for the setup where two different H\"{o}lder-continuous weight functions are considered for positive and negative rewards. 
\end{remark}

\subsection{Regret minimization with weight-based distortions}
\label{sec:model-linear-regret}
The setting here involves arms that are given as the compact set $\X\subset\R^d$ (each element of $\X$ is interpreted as a vector of features associated with an arm). 
The learning game proceeds as follows. At each round $m = 1, 2, \ldots, n$, the learner 
\begin{itemize}
\item[(a)] plays an arm $x_m \in \X$, possibly depending on the history of observations thus far, and 
\item[(b)] observes a stochastic, non-negative reward given by $r_m := x_m\tr\left(\theta_* + N_m\right).$ 
\end{itemize}

As in the $K$-armed setting, the performance measure is the cumulative regret $R_n$ over $n$ rounds, defined as $R_n =  \sum_{m=1}^n \mu_{x_{m}}(\theta_*) - n\mu^*,$ where $x_{m}$ is the arm chosen by the bandit algorithm in round~$m$. Our goal is to propose algorithms that have small cumulative regrets. 

\subsection{Best arm identification with weight-based distortions}
\label{sec:model-linear-bestarm}
Here, we assume that the arms are given by a set $\X$ with $\vert \X \vert = K$ (typically $K \gg 1).$ In each round-$m$, the learner chooses an arm $x_m$ and receives a stochastic reward $r_m.$ Similar to the $K$-armed bandit setting, here, the learner's goal is to identify the optimal or best arm. We study this problem with fixed confidence setting, \emph{i.e.,}  for a given $ \delta \in (0, 1),$ the learner's goal is to find the optimal arm w.p. at least $(1-\delta)$, while minimizing the number of sample observations. As mentioned in Section~\ref{sec:model-karmed-bestarm}, here too an algorithm's performance is quantified by its sample complexity, which is nothing but the number of samples it uses before it stops.