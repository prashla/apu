In this section, we develop and study an algorithm, called W-G, for the best arm identification problem for linear bandits with the fixed confidence setting. The W-G algorithm is adapted from the G-allocation algorithm in~\cite{soare2014best} to incorporate weight-distorted reward criteria.
\subsubsection{Notation}
For any arm $x \in \X,$ denote by $S(x)$ the set of parameters $\theta \in \mathbb{R}^d$ for which arm~$x$ is the optimal arm, i.e., $S(x) = \lbrace \theta \in \mathbb{R}^d: \mu_x ( \theta) \geq \mu_{x'}(\theta)\,\, \forall x' \in \mathcal{X} \rbrace.$ Here $\mu_x(\theta)$, defined in \eqref{eq:cpt-lb}, is the weight-distorted reward of arm $x$ with underlying parameter $\theta$. %For any $x \in \mathbb{R}^d,$ and any positive semidefinite matrix $A$ of size $d\times d,$  we denote $\Vert x \Vert_A$ as $\sqrt{x^T A x}.$ 
Recall that, $\theta_*$ is the true unknown model parameter and $x_*$ is the optimal arm. For any $x \in \X,$ let $\Delta_x = \mu_{x_*}( \theta_*) - \mu_x( \theta_*),$ and $\Delta_{\min} = \min_{x \in \mathcal{X} \setminus x^*} \Delta_x.$ We assume that $\Delta_{\min} > 0.$
For a given sequence of arms ${\bf{x_n}} = \lbrace x_m \rbrace_{m=1}^n$ and $\lambda > 0,$ let $A_{\bf{x_n}} =\lambda I_{d\times d} + \sum\limits_{m=1}^n \frac{x_m x_m^T}{\Vert x_m \Vert^2}.$ For notational convenience, we write $A_{\bf{x_n}}$ as $A_n$ whenever the sequence of actions can be understood from the context. Let $\hat{\theta}_n$ be the ridge regression estimate of $\theta^*$ using $n$ observations, i.e., $\hat{\theta}_n = A_n^{-1} b_n,$ where $b_n = \sum\limits_{m=1}^n \frac{x_m r_m}{\Vert x_m \Vert}.$ For any $x, x' \in \X,$ let $\Delta(x, x') = \mu_x(\theta^*) - \mu_{x'}( \theta^*).$ 

\subsubsection{Stopping condition and arm selection in W-G}
Recall the confidence ellipsoid, $C_m,$ defined in Algorithm~\ref{alg:WOFUL}. Since $\theta_*$ lies within the confidence ellipsoid $C_n$ with high probability \cite[Theorem~2]{abbasi2011improved}, a reasonable condition to stop the algorithm is when the confidence ellipsoid is contained in $S(x)$, for some $x \in \mathcal{X}$. 
If $C_n \subseteq S(x)$ in some round $n$, then
\[\forall \theta \in C_n, \mu_x(\theta) \geq \mu_{x'}(\theta),\,\, \forall x' \in \mathcal{X},\] 
or equivalently, 
\begin{equation}
\label{eq:Stop1}
\left[ \mu_x(\hat{\theta}_n) - \mu_x(\theta) \right] - 
\left[ \mu_{x'}(\hat{\theta}_n) - \mu_{x'}(\theta) \right] \leq \hat{\Delta}_n(x, x'),
\end{equation}
where $\hat{\Delta}_n(x, x') = \mu_x( \hat{\theta}_n) - \mu_{x'}(\hat{\theta}_n)$.
Note that verifying~\eqref{eq:Stop1} is difficult in practice, and hence we derive a simpler sufficient condition as follows:
\begin{align}
\left[ \mu_x(\hat{\theta}_n) - \mu_x(\theta) \right] - \left[ \mu_{x'}(\hat{\theta}_n) - \mu_{x'}(\theta) \right] & \leq \left\vert x^T \left( \hat{\theta}_n - \theta \right) \right\vert + \left\vert x'^T \left( \hat{\theta}_n - \theta \right) \right\vert, \label{eq:lemma5app} \\
& \leq \Vert x \Vert_{A^{-1}_n} \Vert \hat{\theta}_n - \theta \Vert_{A_n} +\Vert x' \Vert_{A^{-1}_n} \Vert \hat{\theta}_n - \theta \Vert_{A_n}, \label{eq:a} \\
& = \left[ \Vert x \Vert_{A^{-1}_n} + \Vert x' \Vert_{A^{-1}_n} \right] \Vert \hat{\theta}_n - \theta \Vert_{A_n}, \label{eq:Stop2}
\end{align}
where \eqref{eq:lemma5app} follows from the fact that $\vert \mu_{Z+a} - \mu_Z \vert \leq 2 \vert a \vert$ for any r.v. $Z$ and $a \in \mathbb{R}$~(due to Lemma~\ref{lemma:cptdiff}), while \eqref{eq:a} is due to the Cauchy-Schwartz inequality. From~\eqref{eq:Stop2}, a sufficient condition for~\eqref{eq:Stop1} is given by 
\begin{equation*}
\forall \theta \in C_n, \exists x \in \mathcal{X} \,\, \text{s.t.} \,\, \left[ \Vert x \Vert_{A^{-1}_n} + \Vert x' \Vert_{A^{-1}_n} \right] \Vert \hat{\theta}_n - \theta \Vert_{A_n} \leq \hat{\Delta}_n(x, x'), \,\, \forall x' \in \mathcal{X}.
\end{equation*}
Using the fact that $\Vert \hat{\theta}_n - \theta \Vert_{A_n} \leq D_n$ for any $\theta \in C_n,$ we have that condition~\eqref{eq:Stop1} holds if
\begin{align}
\exists x \in \mathcal{X} \,\, \text{s.t.} \,\, \left[ \Vert x \Vert_{A^{-1}_n} + \Vert x' \Vert_{A^{-1}_n} \right] D_n \leq \hat{\Delta}_n(x, x'), \,\, \forall x' \in \mathcal{X}.\label{eq:some12} 
\end{align}
Using the fact that for any $x \in \mathcal{A},$ $\Vert x \Vert_{A^{-1}_n} \leq \max\limits_{x \in \mathcal{A}} \Vert x \Vert_{A^{-1}_n},$ it is easy to see that the following condition implies \eqref{eq:some12}: 
\begin{equation}
\label{eq:Stop100}
\exists x \in \mathcal{X} \, \text{ s.t. } \, 2\max\limits_{x \in \mathcal{X}} \Vert x \Vert_{A^{-1}_n} D_n \leq \hat{\Delta}_n(x, x'), \, \forall x' \in \mathcal{A}. 
\end{equation}
Thus, we have arrived at a condition that can be easily verified as compared to \eqref{eq:Stop1}, which is a computationally intensive operation as the latter involves checking an inequality for each candidate $\theta$ within the ellipsoid $C_n$. 

\begin{algorithm}[h]  
	\caption{W-G}
	\label{alg:cpt-g}
	\begin{algorithmic}
		\State {\bfseries Input:} $\delta \in (0, 1),$ $\beta,$ $\lambda,$ and weight function $w$
		\While{(Stopping condition~\eqref{eq:Stop100} does not hold)}
		\State Choose $x_m
		= \arg\min\limits_{x' \in \mathcal{X} }  \max\limits_{x \in \mathcal{X}} \Vert x \Vert_{\left( A_{\bf{x_{m-1}}} + x' x'^T \right)^{-1}},$ and observe reward $r_m.$ 
		\State Update $A_{\bf{x_m}} = A_{\bf{x_{m-1}}} + \frac{x_m x_m^T}{\Vert x_m \Vert^2},$ $b_m = b_{m-1} + \frac{x_m r_m}{\Vert x_m \Vert },$ $\hat{\theta}_m = A_{\bf{x_m}}^{-1} b_m$
		\EndWhile
		\State {\bfseries Output:} Return arm $x$ which satisfies~\eqref{eq:Stop100}. 
	\end{algorithmic}
\end{algorithm}

We now present an algorithm that finds an arm $x$ that satisfies \eqref{eq:Stop100}, while minimizing the number of the sample observations. Notice that, in order to meet the stopping condition, we need to choose a sequence of actions which minimizes the left hand side in \eqref{eq:Stop100}, i.e.,  %To that end, we devise CPT-G algorithm as the one which chooses a sequence of actions which minimizes the left hand side in the above equation \emph{i.e.,}\\
%\emph{Arm selection of CPT-G Algorithm:} 
\begin{equation}
\label{eq:arm-selection-cpt-g-1}
{\bf{x_n^G}} = \arg\min\limits_{ {\bf{x_n}} }  \max\limits_{x \in \mathcal{X}} \Vert x \Vert_{A_{\bf{x_n}}^{-1}}.
\end{equation}
The arm selection strategy above is for the first $n$ rounds. However, $n$ is not known a priori.  So, we employ an incremental version of the arm selection strategy in \eqref{eq:arm-selection-cpt-g-1}, as shown in Algorithm~\ref{alg:cpt-g}, where we present the pseudocode for W-G. 
As in the case of W-OFUL, the W-G algorithm requires the knowledge of the form of the underlying distribution (see Remark \ref{remark:woful-form}), and W-G uses this knowledge to calculate the weight-distorted reward in the stopping condition.

We now present an upper bound on the sample complexity of W-G algorithm below.
\begin{theorem}[\textit{Sample complexity bound}]
\label{Thm:LinBdt1}
Consider a weight function $w : [0, 1] \rightarrow [0, 1]$ with $w(0) = 0$ and $w(1) = 1.$ For any $\delta \in (0, 1),$ let $N^G$ be the round number at which W-G algorithm stops and $\Pi \left( N^G \right)$ be the arm returned at $N^G.$ Then, we have  
%Let $\delta \in (0, 1)$ be the given confidence parameter. For CPT-G algorithm, the following holds:
\begin{equation}
\label{eq:CPT-Gbound}
\mathbb{P} \left( N^G \leq \frac{ c D_n^2 d}{\Delta_{\min}^2}, \,\, \Pi\left( N^G \right) = a^* \right)\geq 1 - \delta,
\end{equation}
where $d$ is the dimension of the space in which $\theta^*$ lies, and $c$ is a universal constant.
\end{theorem}
\begin{proof}[Proof sketch for Theorem~\ref{Thm:LinBdt1}]
First, using Lemma~\ref{lemma:cptdiff} and the Cauchy-Schwartz inequality we show that: 
\begin{align}
\hat{\Delta}_n \left( x^*, x \right) \geq \Delta \left( x^*, x \right) -2 \Vert \hat{\theta}_n - \theta^* \Vert_{A_n} \left[ \Vert x^{*T} \Vert_{A_n^{-1}} +  \Vert x^T \Vert_{A_n^{-1}} \right].
\end{align}
Then using the above, a high probability concentration bound on the confidence ellipsoids, Lemma~\ref{lemma:confidenceellipsoid} in the Appendix, and $\rho^G_n = n \max\limits_{x \in \mathcal{X}} x^T A^{-1}_n x$ we show that the following holds with probability at least $(1 - \delta)$ 
\begin{align}
\hat{\Delta}_n \left( x^*, x \right) \geq  \Delta_{\min} -2 D_n \sqrt{\frac{\rho^G_n}{n}}.
\end{align}
Using a sufficient condition on stopping rule given in equation~\eqref{eq:Stop100} and the above, we show that 
\begin{align}
\Delta_{\min}^2 \geq \frac{ 9 D_n^2 \rho^G_n}{n}.
\end{align}
Finally, the theorem is established by choosing a suitable value for the horizon $n.$
The reader is referred to Appendix~\ref{sec:proof-W-G-upperbound} for a detailed proof.
\end{proof}

{\color{blue}
\begin{remark}
The upper bound on $N^G$ in~\eqref{eq:CPT-Gbound} is independent of the parameters of the weight function and the number of arms $(K)$, but depends on the dimension of the underlying space~$(d).$ Also note that, in terms of dependence on the underlying gaps of arms, this upper bound is of the same order as the upper bound on the sample complexity of G-allocation in~\cite{soare2014best} for the problem of identifying the arm with the highest mean in linear bandits. The bounds differ only in the $\Delta_{min}$ term; here it is a function of weight-distorted rewards of arms, whereas in~\cite{soare2014best}, it is a function of expected values of arms. 
\end{remark}
}