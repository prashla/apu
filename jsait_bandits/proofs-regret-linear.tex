We establish a few technical results in the following lemmas that are necessary for the proof of Theorem \ref{thm:linear-bandit-regret}. 
The first step in the proof of Theorem \ref{thm:linear-bandit-regret} is to bound the instantaneous regret $ir_m$ at instant $m$, which is the difference in the weight-distorted rewards of arm $x_*$ and the arm $x_{m}$ chosen by the algorithm. For bounding the instantaneous regret, it is necessary to relate the difference in weight-distorted rewards of $x_*$ and $x_{m}$ to the difference in their means, i.e., $(x_*\tr\theta_* - x_{m}\tr\theta_*)$, and Lemma \ref{lemma:cptdiff} provides this connection.
\subsubsection{Proof of Lemma \ref{lemma:cptdiff}}
\label{sec:weight-distortion-undert-translation}
Notice that
%\begin{proof}
	\begin{align*}
		\mu_{X+a} &= \intinfinity w(\prob{X+a > z}) dz - \intinfinity w(\prob{-X-a > z}) dz \\
		&= \intinfinity w(\prob{X > z-a}) dz - \intinfinity w(\prob{-X > z+a}) dz \\
		&= \int_{-a}^\infty w(\prob{X > u}) du - \int_{a}^\infty w(\prob{-X > u}) du \\
		&= \int_{-a}^0 w(\prob{X > u}) du - \int_{a}^0 w(\prob{-X > u}) du + \mu_X \\
		&= \int_{-a}^0 w(\prob{X > u}) du + \int_{-a}^0 w(\prob{X < v}) dv + \mu_X.
	\end{align*}
	This proves the main claim. For the case when $w$ is bounded by 1, it is easy to infer that $|\mu_{X+a} - \mu_X| \leq 2|a|$.
	\hfill{$\blacksquare$}
%\end{proof}
\begin{lemma}
\label{lemma:confidenceellipsoid}
Under the hypotheses of Theorem \ref{thm:linear-bandit-regret}, for any $\delta>0$, the following event occurs w.p. at least $1-\delta$: $\forall m \ge 1$, $\theta_*$ lies in the set $C_m$ defined in Algorithm \ref{alg:WOFUL}, i.e., 
\begin{align*}
	C_{m} &= \left\{\theta \in \mathbb{R}^d: \norm{\theta - \hat{\theta}_m}_{A_m} \leq D_m \right\}, \quad \mbox{where} \\
	D_m & = \sqrt{2 \log \left(\frac{\det(A_m)^{1/2} \det(\lambda I_{d \times d})^{1/2}}{\delta} \right)} + \beta\sqrt{\lambda}, \\
	A_m &= \lambda I_{d \times d} + \sum_{l=1}^{m-1} \frac{x_l x_l\tr}{\norm{x_l}^2}, \\
	\hat{\theta}_m &= A_m^{-1} b_m, \quad \mbox{and} \quad b_m = \sum_{l=1}^{m-1} \frac{r_l x_l}{\norm{x_l}}.
\end{align*}
\end{lemma}
\begin{proof}
The proof is a consequence of \cite[Theorem 2]{abbasi2011improved}. Indeed, the hypotheses of the stated theorem are satisfied with the sub-Gaussianity parameter $R = 1$ since by (\ref{eq:linban-depnoise}), the stochastic reward $r_m$ normalised by $\norm{x_m}$ at each instant satisfies
\[ \frac{r_m}{\norm{x_m}} = \frac{x_m}{\norm{x_m}} \tr \theta + \frac{x_m}{\norm{x_m}} \tr N_m, \]
whose distribution is Gaussian with mean $\frac{x_m}{\norm{x_m}} \tr \theta$ and variance $1$, and which is thus sub-Gaussian with parameter $R = 1$. 
\end{proof}

\subsubsection{Proof of Theorem \ref{thm:linear-bandit-regret}}
\label{thm:woful_linear_regret_bound_proof}
We first prove the following lemma which we will need in the proof of Theorem \ref{thm:linear-bandit-regret}. It relates the weight-distorted reward $\mu_x(\theta)$ of the stochastic reward from an arm $x \in \X$ to the weight-distorted reward of a translated standard normal distribution. 

\begin{lemma}[Weight-distorted value under scaling]
\label{lem:CPTscaling}
If $Z$ denotes a standard normal random variable, then for any arm $x$ and $\theta$, \[ \mu_{x}(\theta) = \norm{x} \cdot \mu_{Z + \frac{x\tr\theta}{\norm{x}}}, \]
where $\mu_{Y}$ denotes the weight-distorted reward of a random variable $Y$.
\end{lemma}

\begin{proof}
Let $X$ denote the stochastic reward from arm $x$ with $\theta$ as the parameter; we have that $X$ is normal with mean $x\tr\theta$ and variance $\norm{x}^2$. With $\stdnormal$ being a standard normal $d$-dimensional vector and $\hat x = \frac{x}{\norm{x}}$, we can write
	\begin{align*}  
	 \mu_X &:= \intinfinity w(\prob{X > z}) dz - \intinfinity w(\prob{-X > z}) dz\\
	 & =  \intinfinity w(\prob{x\tr\theta + x\tr\stdnormal > z}) dz - \intinfinity w(\prob{-x\tr\theta - x\tr\stdnormal > z})dz\\
	 & = \intinfinity w(\prob{\hat x\tr\theta + \hat x\tr\stdnormal > \frac{z}{\norm{x}}}) dz - \intinfinity w(\prob{-\hat x\tr\theta - \hat x\tr\stdnormal > \frac{z}{\norm{x}}})dz\\
	 &= \norm{x}\left(\intinfinity w(\prob{\hat x\tr\theta + \hat x\tr\stdnormal > y}) dy - \intinfinity w(\prob{-\hat x\tr\theta - \hat x\tr\stdnormal > y})dy\right)\\
	 &= \norm{x} \mu_{Z + \hat x\tr\theta};\stepcounter{equation}\tag{\theequation}\label{eq:t121}
	\end{align*}
\end{proof}

\begin{remark}\label{remark:sub-gauss-linear}
Note that the above proof holds even when $\stdnormal$ is a vector of sub-Gaussian random variables with parameter $1$ (cf. \cite[Chapter 2]{wainwright2019high}). Hence, the results for the linear bandit setting can be generalized to the case of sub-Gaussian noise.  
\end{remark}

\begin{proof}[Proof of Theorem \ref{thm:linear-bandit-regret}]
Let $ir_m = \mu_{x_*}(\theta_*) - \mu_{x_{m}}(\theta_*)$ denote the instantaneous regret incurred by the algorithm that chooses arm $x_{m}\in \X$ in round $m$.
Let $\arm_x$ (resp. $P_x$) denote the r.v. (resp. probability function) governing the rewards of the arm $x \in \X$.

Letting $\hat x_m = \frac{x_m}{\norm{x_m}}$ and $W$ to be a standard Gaussian r.v.,  we upper-bound the instantaneous regret $ir_m$ at round $m$ as follows: 
\begin{align}
ir_m =  \mu_{x_*}(\theta_*) - \mu_{x_{m}}(\theta_*) & \le \mu_{x_m}(\tilde\theta_m) - \mu_{x_{m}}(\theta_*) \label{eq:rm1}\\
& = \norm{x_m} \left( \mu_{W+ \hat x_{m}\tr\tilde\theta_m} - \mu_{W+ \hat x_{m}\tr\theta_*} \right) \label{eq:rm2}\\
& \le 2 \norm{x_m} \left| \hat x_{m}\tr(\tilde\theta_m - \theta_*) \right| \label{eq:rm3}\\
& = 2 \left| x_{m}\tr(\tilde\theta_m - \theta_*) \right| \label{eq:rm4}\\
& \le 2 \left(\left| x_{m}\tr(\hat\theta_{m} - \theta_*)\right|  + \left| x_{m}\tr(\tilde\theta_m - \hat\theta_{m})\right|\right)  \label{eq:rm5} \\
& = 2 \left(\norm{\hat\theta_{m} - \theta_*}_{A_m}  \norm{x_{m}}_{A_m^{-1}}  + \norm{\tilde\theta_m - \hat\theta_{m}}_{A_m}   \norm{x_{m}}_{A_m^{-1}} \right)  \label{eq:rm6} \\
& \le 4 w_m \sqrt{D_m}  \quad \text{whenever }\theta_* \in C_m, \label{eq:rm7}
\end{align} 
where $w_m = \sqrt{x_m\tr A_m^{-1}x_m}$ and $D_m$ is as defined in Algorithm \ref{alg:WOFUL}.
The inequalities above are derived as follows: \eqref{eq:rm1} follows from the choice of $x_{m}$ and $\tilde \theta_m$ in Algorithm~\ref{alg:WOFUL}; \eqref{eq:rm2} follows from the scaling lemma (Lemma \ref{lem:CPTscaling}); \eqref{eq:rm3} follows by applying the translation lemma (Lemma \ref{lemma:cptdiff}); \eqref{eq:rm4} follows from the definition of $\hat x_{m}$ \eqref{eq:rm6} is by the Cauchy-Schwarz inequality applied to the pair of dual norms $\norm{\cdot}_{A_m}$ and $\norm{\cdot}_{A_m^{-1}}$ on $\R^d$; \eqref{eq:rm7} follows from the definition of the confidence ellipsoid $C_m$ in Algorithm \ref{alg:WOFUL}, as $\hat\theta_{m}$, $\tilde \theta_m$ and $\theta_*$ belong to $C_m$. Note that Lemma \ref{lemma:confidenceellipsoid} guarantees that this is true w.p. at least $1-\delta$.

The cumulative regret $R_n$ of W-OFUL can now be bounded as follows. With probability at least $1-\delta$, 
\begin{align}
R_n = \sum_{m=1}^n ir_m \le  \sum_{m=1}^n 4 \sqrt{D_m} w_m &\le 4 \sqrt{D_n}  \sum_{m=1}^n w_m \nonumber \\
& \le 4 \sqrt{D_n}  \left(n \sum_{m=1}^n w_m^2\right)^{1/2} \label{eq:Rn3}\\
& \le \sqrt{32 d n D_n \log n}. \label{eq:Rn4}
%& \le C (d \log (\lambda + nL/d))^{\alpha/2} n^{\frac{2-\alpha}{2}}.  \text{ for some } C<\infty. \label{eq:Rn4}
\end{align} 
Inequality \eqref{eq:Rn3} follows by an application of the Cauchy-Schwarz inequality, while the inequality in \eqref{eq:Rn4} follows from (the standard by now) Lemma 9 in~\cite{dani2008stochastic}, which shows that $\sum_{m=1}^n w_m^2 \le  2d \log n$. 
The claim follows.
\end{proof}