\label{sec-experiments}
\textit{Basic methodology.} The simulations presented in this section are performed using MATLAB, and all results are averaged over 100 independent runs. The expected value for each arm is obtained analytically, while the weight-distorted reward is estimated numerically through Algorithm \ref{alg:cpt-estimator} using $50000$ samples from the underlying distribution of the individual arms.
\subsection{$K$-armed bandits}
We consider the following three-armed bandit problem to test the fixed confidence and budget algorithms: Arm~1 takes values $50$ and $0$ with probabilities $0.1$ and $0.9$, respectively; Arm~2 takes values $30$ and $0$ with probabilities $0.2$ and $0.8$; and  Arm~3 takes value $7$ with probability $1$. Under the weight function  $w(x) = \frac{x^{0.61}}{\left( x^{0.61} + (1-x)^{0.61} \right)^\frac{1}{0.61}}$, arm~1 has the highest weight-distorted reward. On the other hand, the expected value of arm~3 is the highest. We implement LUCB \cite{kalyanakrishnan2012pac}, SR \cite{audibert2010best} and their variants that incorporate weight-distorted reward. The latter algorithms are W-LUCB and W-SR, described in Section \ref{sec:algos-karmed-bestarm}. 

We ran SR and our W-SR algorithm for simulation budgets ranging from $10^4$ samples to $5 \times 10^4$ samples on the aforementioned three-armed bandit problem. We observed that SR and W-SR converge to arm 3 and arm 1, respectively, whose expected and weight-distorted rewards values are listed in Table~\ref{tab:sr-results}. It is evident that a specialized algorithm such as W-SR is necessary to optimize weight-distortion-based criteria, and the regular SR algorithm that optimizes expected value cannot be used as a surrogate to find a weight-distortion optimal solution.         
\begin{table}
  \caption{Expected and weight-distorted rewards for a three-armed bandit problem. W-SR converges to arm $1$, while SR converges to arm $3$; the expected/weight-distorted rewards of these arms are tabulated below.}
  \label{tab:sr-results}
 \centering
 \begin{tabular}{c|c|c}
  \toprule 
   & \textbf{Expected reward}& \textbf{Weight-distorted reward }\\\midrule
   SR & \textbf{$7$} & $7$ \\\midrule
   W-SR & $5$ & \textbf{$9.304$}\\\bottomrule
  \end{tabular}
  \end{table}
Next, we compare the performance of LUCB and our W-LUCB algorithm for various confidence parameters and present the results in Figure~\ref{Fig:FC-1}. For a given confidence parameter, the empirical probability of incorrect identification of an algorithm is calculated by the fraction of runs on which the algorithm returned a sub-optimal arm. It is easy to see that W-LUCB's empirical probability of incorrect identification is always lower than the given confidence parameters, while that of LUCB is above the given confidence parameters. Thus, it is very likely that LUCB returned a sub-optimal arm, while W-LUCB found the optimal arm. As in the case of fixed confidence algorithms, the results in Figure \ref{Fig:FC-1} justify the need for an algorithm like W-LUCB, as LUCB does not find the weight-distortion optimal solution.
\begin{figure}[h]
\centering
\begin{tikzpicture}
\begin{axis}[ylabel style={align=center}, xlabel={{\small Expected sample complexity}}, ylabel={{\small Confidence/probability of}\\ {\small incorrect identification}},width=7cm,height=6cm,grid,tick scale binop={\times},
% legend pos = south east,
legend style={font=\small},
legend style={at={(0,0)},anchor=north,at={(axis description cs:0.5,-0.26)},legend columns=2},
x label style={at={(axis description cs:0.5,-0.0025)},anchor=north},
every x tick scale label/.style={
    at={(1,0)},xshift=-1pt,yshift=-1.2em,anchor=south west,inner sep=0pt
}
]
\addplot+[only marks,mark=o,mark options={scale=1.2}] file {results/expected_sample_complexity_LUCB_delta.txt};
\addlegendentry{Confidence (LUCB)}
\addplot+[only marks,mark=square*,mark options={scale=1.2}] file {results/expected_sample_complexity_LUCB_proberror.txt};
\addlegendentry{Probability of incorrect identification (LUCB)}
\addplot+[only marks,mark=o,mark options={scale=1.2}] file {results/expected_sample_complexity_CPTLUCB_delta.txt};
\addlegendentry{Confidence (W-LUCB)}
\addplot+[only marks,mark=square*,color=green,mark options={scale=1.2}] file {results/expected_sample_complexity_CPTLUCB_proberror.txt};
\addlegendentry{Probability of incorrect identification (W-LUCB)}
\end{axis}
\end{tikzpicture}
\caption{Comparison of LUCB and W-LUCB algorithms in fixed confidence setting using the probability of incorrect identification in identifying the arm with the highest weight-distorted reward.}
\label{Fig:FC-1}
\end{figure}  

We now compare the performance of LUCB, W-LUCB and WE-LUCB, where the latter is an adaptation of classic LUCB to fixed confidence setting by using $W(\textrm{empirical mean})$ as a surrogate for the weight-distorted value estimate. We use the same Setup(*) that is considered in Section~\ref{sec:experiments-regret-appendix} here. Figure~\ref{fig:setup_2} shows the comparison of LUCB, WE-LUCB and our W-LUCB for various confidence ($\delta$) parameters. We have run the algorithms for 100 runs and calculated expected sample complexity as the average sample complexity across all the runs. Similarly, probability of incorrect identification is calculated as the fraction of runs where the algorithm has returned a sub-optimal arm. It is easy to observe from Figure~\ref{fig:setup_2} that our algorithm W-LUCB outperforms both LUCB and WE-LUCB in this very simple set-up.

\begin{figure}[htb]
\center{\includegraphics[scale=0.6]{setup_2.eps}}
\caption{\label{fig:setup_2} Comparison of LUCB, WE-LUCB and W-LUCB for best arm identification in stochastic bandits with fixed confidence setting.}
\end{figure}

\subsection{Linear bandits}  
% \begin{figure}
% \begin{center}
% \begin{tikzpicture}[thick, scale=0.35,abraca/.style={draw,thick,circle,fill=blue!20}]	
% \draw[]  (0,0) circle (.5); \node at (0,0) {\tiny 7};
% \draw[] (4,0) circle (.5); \node at (4,0) {\tiny 8};
% \draw[] (8,0) circle (.5); \node at (8,0) {\tiny 9};
% \draw[] (0,4) circle (.5); \node at (0,4) {\tiny 4};
% \draw[] (4,4) circle (.5); \node at (4,4) {\tiny 5};
% \draw[] (8,4) circle (.5); \node at (8,4) {\tiny 6};
% \draw[] (0,8) circle (.5); \node at (0,8) {\tiny 1};
% \draw[] (4,8) circle (.5); \node at (4,8) {\tiny 2};
% \draw[] (8,8) circle (.5); \node at (8,8) {\tiny 3};
% \draw[line width=.05cm] [-](0.5,0)--(3.5,0);
% \draw[line width=.05cm] [-](4.5,0)--(7.5,0);
% \draw[line width=.05cm] [-](0,0.5)--(0,3.5);
% \draw[line width=.05cm] [-](0,4.5)--(0,7.5);
% \draw[line width=.05cm] [-](0.5,8)--(3.5,8);
% \draw[line width=.05cm] [-](4.5,8)--(7.5,8);
% \draw[line width=.05cm] [-](8,7.5)--(8,4.5);
% \draw[line width=.05cm] [-](8,3.5)--(8,0.5);
% \draw[line width=.05cm] [-](0.5,4)--(3.5,4);
% \draw[line width=.05cm] [-](4.5,4)--(7.5,4);
% \draw[line width=.05cm] [-](4,0.5)--(4,3.5);
% \draw[line width=.05cm] [-](4,4.5)--(4,7.5);
% \node at (2,8.5) {$l_1$}; \node at (6,8.5) {$l_2$};
% \node at (-0.5,6) {$l_3$}; \node at (3.5,6) {$l_4$}; \node at (7.5,6) {$l_5$};
% \node at (2,3.5) {$l_6$}; \node at (6,3.5) {$l_7$};
% \node at (-0.5,2) {$l_8$}; \node at (3.5,2) {$l_9$}; \node at (7.25,2) {$l_{10}$};
% \node at (2,-0.75) {$l_{11}$}; \node at (6,-0.75) {$l_{12}$};
% \end{tikzpicture}
% \end{center}
% \caption{3 $\times$ 3 grid network}
% \label{Fig:grid-network}
% \end{figure}

\begin{figure}
\centering
\scalebox{0.75}{
 \begin{tikzpicture}[font=\sffamily, every matrix/.style={ampersand replacement=\&,column sep=2cm,row sep=2cm}, process/.style={draw,thick,circle,fill=blue!20}]
\matrix{
\node[process] (a1) {1}; \&    \node[process] (a2) {2};
      \& \node[process] (a3) {3}; \& \\

   \node[process] (a4) {4};
      \& \node[process] (a5) {5};   \& \node[process] (a6) {6};\\
   \node[process] (a7) {7};
      \& \node[process] (a8) {8};   \& \node[process] (a9) {9};\\
  };
 \draw[green!40!black] (a1) --node[above]{$l_1$} (a2);
 \draw[green!40!black] (a2) --node[above]{$l_2$} (a3);
 \draw[green!40!black] (a4) --node[above]{$l_6$} (a5);
 \draw[green!40!black] (a5) --node[above]{$l_7$} (a6);
 \draw[green!40!black] (a7) --node[above]{$l_{11}$} (a8);
 \draw[green!40!black] (a8) --node[above]{$l_{12}$} (a9);
 \draw[green!40!black] (a1) --node[right]{$l_3$} (a4);
 \draw[green!40!black] (a2) --node[right]{$l_4$} (a5);
 \draw[green!40!black] (a3) --node[right]{$l_5$} (a6);
 \draw[green!40!black] (a4) --node[right]{$l_8$} (a7);
 \draw[green!40!black] (a5) --node[right]{$l_9$} (a8);
 \draw[green!40!black] (a6) --node[right]{$l_{10}$} (a9);
% 
% \draw[->,red] (a5) to[parallel segment, segment label=]      (a6);
% \draw[->,red] (a4) to[parallel segment, segment label=, segment labelpos=left,   segment distance=2.5mm]      (a6);
% \draw[->,red] (a2) to[parallel segment, segment label=, segment labelpos=left,   segment distance=2.5mm]      (a3);
% \draw[->,red] (a1) to[parallel segment, segment label=, segment distance=2.5mm]      (a3);
% \draw[->,red] (a3) to[parallel segment, segment label=, segment labelpos=left,   segment distance=2mm]      (a5);
\end{tikzpicture}}
\caption{A $3\times3$-grid network}
\label{Fig:grid-network}
\end{figure}
As shown in Figure~\ref{Fig:grid-network}, we consider a $3\times3$-grid network with nodes labelled $1$ and $9$ as the source and destination, respectively. Each link $l_i$ is associated with a parameter $\theta_i$ that indicates the mean delay for the link. We consider the following six paths as the arms of the linear bandit: $x_1 = (l_1, l_2, l_5, l_{10}),$ $x_2 = (l_1, l_4, l_9, l_{12}),$ $x_3 = (l_3, l_8, l_{11}, l_{12}),$ $x_4 = (l_3, l_6, l_7, l_{10}),$ $x_5 = (l_3, l_6, l_9, l_{12}),$ $x_6 = (l_1, l_4, l_7, l_{10}).$ We set $\theta = \left[ 30\,10\,30\,5\,1\,20\,15\,7\,20\,30\,1\,30 \right]$, leading to mean delays of $71, 85, 69, 95, 100, 80$ for arms $1$ to $6$, respectively. 

A road user traversing any path will experience a sample delay which is the sum of mean overall  delay together with a noise element that captures the stochastic nature of the routing problem.  The noise element uses the following model: arms $1, 2, 4, 5$ and $6$ have standard Gaussian noise, while the noise element for arm 3 takes values $100$ and $0$ with probabilities $0.01$ and $0.99$, respectively. Thus, arm $3$ has the lowest expected delay.

For the weight-distorted reward, we used a baseline delay of $80$, corresponding to arm $6$. The rationale is that road users currently take arm $6$ and any routing improvement is relative to the status quo (or) if a new path results in a overall delay of $70$, then it corresponds to a gain of $10$, while a delay of $100$ corresponds to a loss of $20$. The goal is to maximize the weight-distorted reward of the delay gain, where the gain is defined relative to baseline (arm $6$). In this setting, arm $1$ has the highest weight-distorted reward.
Intuitively, arm $3$ is less appealing to a human road user as it has a small probability of resulting in a very high delay, even though its mean delay is the lowest. On the other hand, arm $1$ is slightly sub-optimal in the mean delay, but has no chance of inordinately high delays.
We compare G-allocation \cite{soare2014best} that attempts to find a path with the lowest mean delay against our W-G algorithm that incorporates weight-distortion based criteria to find a path that is more appealing to a human road user.   
    \begin{table}
  \caption{Expected delay and weight-distorted rewards for the linear bandit problem corresponding to 3$\times3$ grid network shown in Figure~3. W-G converges to arm $1$, while G-allocation converges to arm $3;$ the expected delay and weight-distorted rewards of these arms are tabulated below.}
  \label{tab:cpt-g-results}
  \centering
 \begin{tabular}{c|c|c}
  \toprule 
   & \textbf{Expected delay}& \textbf{Weight-distorted reward}\\\midrule
   G-allocation & \textbf{$69$} & $7.795$ \\\midrule
   W-G & $71$ & \textbf{$8.865$}\\\bottomrule
  \end{tabular}
  \end{table} 
 Table \ref{tab:cpt-g-results} presents the results from simulation runs of W-G and G-allocation, with  $\delta = 0.01$. As the reader might expect, W-G algorithm converges to arm $1$, while G-allocation in~\cite{soare2014best} to arm $3$.  Therefore,  W-G algorithm is more conducive in identifying a path that emulates human decision making.
%On the other hand, arm $1$ is slightly sub-optimal in the mean delay, but has no chance of inordinately high delays and hence, a CPT-G algorithm is more practical in finding a human-appealing path, which classic algorithms sans CPT do not.

  
  
  
