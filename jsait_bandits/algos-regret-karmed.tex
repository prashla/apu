In this section, we first introduce an estimator for weight-distorted reward of an arm, given a set of i.i.d. samples drawn from the associated arm's distribution and a weight function. Then, we present algorithms for $K$-armed bandits under regret minimization and best arm identification settings.  
\subsection{Estimating the weight-distorted reward}
Estimating the weight-distorted reward of any arm $k$ is non-trivial; one cannot just use a Monte Carlo approach with sample means, because weight-distorted reward involves a distorted distribution, whereas the samples come from the undistorted distribution $F_k$. Thus, one needs to estimate the entire distribution, and, for this purpose, we adapt the quantile-based approach, originally proposed in~\cite{prashanth2015cumulative}.  For convenience, we briefly recall this estimation scheme for weight-distorted reward of an arm $k$ in Algorithm~\ref{alg:cpt-estimator}.
\begin{algorithm}  
\caption{Weight-distorted reward estimator}
\label{alg:cpt-estimator}
\begin{algorithmic}
\State {\bfseries Input:} $l$ i.i.d samples of arm $k$, $Y_{k,1},\ldots, Y_{k,l},$ weight function $w$.

\vspace{1ex}

\State Sort the samples in ascending order as follows:\\ $Y_{[k, 1]} \leq Y_{[k, 2]} \leq \dots Y_{[k, l_b]} \leq 0 \leq Y_{[k, l_b+1]} \leq \dots \leq Y_{[k, l]},$ where $l_b \in \lbrace 0, 1, 2, \dots, l \rbrace.$   

\vspace{1ex}

\State Let $ \widehat{\mu}_{k, l}^+  := \sum\limits_{i=l_b+1}^l Y_{[k,i]}\left[ w \left( \frac{l+1-i}{l} \right) - w \left( \frac{l-i}{l} \right) \right]$ and  
$\widehat{\mu}_{k, l}^-  := \sum\limits_{i=1}^{l_b} Y_{[k, i]}\left[ w \left( \frac{i - 1}{l} \right) - w \left( \frac{i}{l} \right) \right].$ 

\vspace{1ex}

 \State Set $\widehat{\mu}_{k, l} = \hat{\mu}_{k, l}^+ - \hat{\mu}_{k, l}^-.$
%$\text{Estimate of $\mu_k$}: \widehat{\mu}_{k, l} = \hat{\mu}_{k, l}^+ - \hat{\mu}_{k, l}^-.$

\vspace{1ex}

\State {\bfseries Output:} Return $\hat{\mu}_{k, l}$.
\end{algorithmic}
\end{algorithm}

It can be seen that $\widehat \mu_{k,l}$ is the weight-distorted reward of the empirical distribution of samples from arm $k$ seen thus far, i.e.,
\begin{align*}
\widehat \mu_{k,l}  &:= \intinfinity w(1-\hat F_{k,l}(z)) dz - \intinfinity w(\hat F_{k,l}(-z)) dz, \label{eq:cpt-est-equiv}
\end{align*}
where $\hat F_{k,l}(x):=\frac{1}{l} \sum_{i=1}^{l} I_{\left[Y_{k,i} \leq x\right]}$ denotes the empirical estimate of the distribution $F_k.$ In particular, the first and second integrals above correspond to $\widehat{\mu}_{k, l}^+$ and $\widehat{\mu}_{k, l}^-,$ respectively. 

We next provide a sample complexity result for the accuracy of the estimator $\widehat \mu_{k,l}$ under the following assumptions:\\
\noindent\textbf{(A1)}  
The weight function $w : [0, 1] \rightarrow [0, 1]$ with $w(0) = 0$ and $w(1) =1$, and is H\"{o}lder-continuous with constant $L > 0$ and exponent $\alpha \in (0,1]$, i.e., $\sup\limits_{x \neq y} \frac{| w(x) - w(y) |}{| x-y |^{\alpha}} \leq L.$ \\
\noindent\textbf{(A2)}  
The arms' rewards are bounded by $M > 0$ almost surely.

(A1) is necessary to ensure that the weight-distorted reward $\mu_k,$ for $k \in \{ 1,\ldots,K \}$ is finite. Moreover, the popular choice for the weight function, proposed in~\cite{tversky1992advances} and illustrated in Figure \ref{fig:weight}, is H\"{o}lder-continuous.
\begin{theorem}[\textit{Sample complexity of estimating weight-distorted reward}]
\label{thm:cpt-est}
\ \\
Assume (A1)-(A2). Then, for any $\epsilon >0$ and any $k\in \{1,\ldots,K\}$, we have 
\begin{equation*}
\mathbb{P}(\left |\widehat \mu_{k,m} - \mu_k \right| > \epsilon ) \le 2\exp\left(-2m(\epsilon/LM)^{2/\alpha}\right).
\end{equation*}
\end{theorem}
\begin{proof}
The proof is very similar to that of~\cite[Proposition 2]{prashanth2015cumulative}. The proof relies on the Dvoretzky-Kiefer-Wolfowitz (DKW) inequality, which gives finite-sample exponential concentration of the empirical distribution $\hat{F}_{k,m}$ around the true distribution $F_k$, as measured by the $||\cdot||_\infty$ function norm \cite[Chapter 2]{wasserman2006} and~\cite{massart1990tight}.
\end{proof}
\begin{remark}
Weight-distorted reward estimation is easier when the form of the underlying distribution is known, as illustrated by the following example: Consider a Bernoulli distribution with parameter $p$.
%, and Algorithm~\ref{alg:cpt-estimator} is aware of the type of the distribution. 
The weight-distorted reward for this distribution is $w(p)$, and the latter can be estimated as $w(\hat{p})$, where $\hat{p}$ is an estimate of $p$. %Specifically, given $l$ i.i.d. samples drawn from Bernoulli distribution with some parameter and a weight-function $w(\cdot)$, then the weight-distorted reward calculated by Algorithm~\ref{alg:cpt-estimator} can be written as $w(s/l)$, where $s$ is the number of ones in the given $l$ samples.
\end{remark}

\begin{remark}
For the special case of distributions with bounded support, using DKW inequality, together with the H\"{o}lder-continuity of the weight function, allowed the derivation of the concentration result in Theorem~\ref{thm:cpt-est}. However, we do not see this argument work for distributions with unbounded support, e.g., sub-Gaussian.
%{\color{blue} In the following, we provide a concentration result for the sub-Gaussian distributions using truncation based approach.
For the case of sub-Gaussian distributions, an approach that works uses truncation to arrive at a concentration result, which is given in the below proposition.
\end{remark}

{\color{blue} Suppose the underlying distribution is $\sigma$-sub-Gaussian. We define the following truncated estimator for weight-distorted value as follows:
\begin{align*}
\mu_n = \int_{0}^{M_n}w(1-F_{n}(z))\mathrm{d}z - \int_{0}^{M_n}w( F_{n}(-z))\mathrm{d}z,
\end{align*}
where $M_n = \sigma\sqrt{\log n}$.

We now present a concentration bound for the truncated estimator defined above.
\begin{proposition}
\label{prop:sub-gaussian-concentration}
	Let $Y$ be a $\sigma$-sub-Gaussian r.v. Let $\mu(Y)$ denote the weight-distorted value associated with r.v. $Y$, and let $\mu_n$ be the truncated estimator, formed from $n$ i.i.d. samples of the distribution underlying $Y$. Then, for any $\epsilon >  \frac{8L}{\alpha n^{\alpha/2}}$, we have
	\begin{align*}
	\Prob{\mu_n - \mu(Y) > \epsilon} \le  2\exp\left( -2 n \left(\frac{2}{L^2\log n}\right)^{\frac1{\alpha}} \left(\epsilon - \frac{8L}{\alpha n^{\alpha/2}} \right)^{\frac{2}{\alpha}}  \right).
	\end{align*}
\end{proposition}
\begin{proof}
See Appendix~\ref{sec:subgauss-conc}.
\end{proof}

\begin{remark}
In Proposition~3 of~\cite{prashanth2017cptlong}, the authors provide a tail bound of the order \\ $\left(2n e^{-n^\frac{\alpha}{2+\alpha}} 
+ 2 e^{-n^{\frac{\alpha}{2+\alpha}}\left(\frac{\epsilon}{2L}\right)^{\frac{2}{\alpha}}}\right)$ for weight-distorted value estimation in the sub-Gaussian case, for all $n \geq \left(\frac{\ln2-\ln\epsilon}{2\alpha}\right)^{\alpha+2}$. In contrast, the tail bound provided in the above proposition is of the order $\left(2 e^{-c n \left(\epsilon - \frac{8L}{\alpha n^{\alpha/2}}\right)^{\frac{2}{\alpha}}}\right)$ holds for all $n\ge 1$. Our bound exhibits improved dependence on the number of samples $n$, while having a similar dependence on the accuracy $\epsilon$. 
\end{remark}
}

% For the special case of Lipschitz weight functions $w$, setting $\alpha=1$ in the above theorem, we obtain a sample complexity of order $O\left(1/\epsilon^2\right)$ for accuracy $\epsilon$. 
% \begin{proof}
% See Appendix \ref{sec:appendix-cpt-est}.
% %The proof proceeds along the lines of \cite[Proposition 2]{prashanth2015cumulative}. The proof relies on the Dvoretzky-Kiefer-Wolfowitz (DKW) inequality, which gives finite-sample exponential concentration of the empirical distribution $\hat{F}_{k,m}$ around the true distribution $F_k$, as measured by the $||\cdot||_\infty$ function norm \cite[Chapter 2]{wasserman2006}.
% \end{proof}
\subsection{Regret minimization with weight-based distortions}
\label{sec:algos-karmed-regret}
We now propose and study algorithms for $K$-armed bandits under the regret minimization setting. 
Recall, from (A1) and (A2), that  $\alpha \in (0, 1]$ denote the \holder exponent, $L > 0$ the \holder constant and $M > 0$ the bound on the stochastic rewards from any arm. 
For $m \in \mathbb{N}$ and $l \in \mathbb{N},$ define $\gamma_{m,l} := LM \left(\dfrac{3\log m}{2l}\right)^{\frac{\alpha}{2}}.$ The r.v. $\widehat \mu_{k,l}$, defined by Algorithm \ref{alg:cpt-estimator}, is an estimate of $\mu_k$ that uses the $l=T_k(m-1)$ sample rewards of arm $k$ seen so far and $\gamma_{m,l}$ is the confidence width, which together with $\widehat \mu_{m,l}$ ensures that the true weight-distorted value $\mu_k$ lies within  $[\widehat \mu_{k,l} - \gamma_{m,l}, \widehat \mu_{k,l} + \gamma_{m,l}]$ with high probability, i.e., 
\[\mathbb{P} \left(\mu_k \le \widehat \mu_{k,l} + \gamma_{m,l} \right) \le \dfrac{2}{m^{3}} \textrm{ and } \mathbb{P}\left(\widehat \mu_k  \ge \mu_{k,l} - \gamma_{m,l} \right) \le \dfrac{2}{m^{3}},\,\, k=1,\ldots,K.\] 

\begin{algorithm}[h]  
	\caption{W-UCB}
	\label{alg:w-ucb}
	\begin{algorithmic}
		\State {\bfseries Input:} Weight function $w$.
		%\State {\bfseries Initialization: $1 \le m \le K$} 
		%\For{$m=1,\ldots,K$}
		\State In the first $K$ rounds, play each arm once.%$I_m = m$.
		%\EndFor 
		%\For{round $m=K+1,\ldots$}
		\State In round $m\ge K$, play arm $I_m = \argmax\limits_{k \in \{1,\ldots,K\}} \left[ \widehat \mu_{k, T_k(m-1)} + \gamma_{m, T_k(m-1)} \right]$,\\
		where $T_k(m-1)$ is the number of plays of arm $k$ up to round~$m,$ $\widehat\mu_{k, T_k(m-1)}$ is given in Algorithm~\ref{alg:cpt-estimator}, and $\gamma_{m,l} = LM \left(\frac{3\log m}{2l}\right)^{\frac{\alpha}{2}}.$
		
		%\EndFor
	\end{algorithmic}
\end{algorithm}

Using the empirical estimates $\widehat \mu_{k,l}$ of weight-distorted rewards together with confidence width $\gamma_{m,l}$, we propose an UCB-type algorithm for minimizing the regret $R_n$ defined in \eqref{eq:regret-karmed}. 
The pseudocode of the Weighted-Upper Confidence Bound (W-UCB) algorithm is given in Algorithm~\ref{alg:w-ucb}. 
As in the case of regular UCB, the W-UCB algorithm pulls each arm once in the initialization phase, and 
in any round $m,$ after the initialization phase, chooses the arm with the highest UCB value.  
% A high level ideal of the W-UCB algorithm is as follows. The algorithm samples each arm exactly once in the initialization phase. 
Now, we present a result that gives an upper bound on the expected regret of the W-UCB algorithm. 
\begin{theorem}[\textit{Regret upper bound}]
\label{thm:BasicHolderRegretGap}
Under (A1)-(A2), the expected cumulative regret $R_n$ of W-UCB is bounded as follows:
$$ \E \left[ R_n \right] \le \sum\limits_{\{k:\Delta_k>0\}} \dfrac{3(2LM)^{2/\alpha}\log n}{2\Delta_k^{2/\alpha - 1}} + MK\left(1 + \dfrac{2\pi^2}{3} \right).
$$ 
\end{theorem} 
\begin{proof}
See Appendix~\ref{sec:appendix-gapdependentregret} in the supplementary material. 
\end{proof}
The theorem above involves the gaps $\Delta_k$. We next present a gap-independent regret bound in the following result:
\begin{corollary}[\textit{Gap-independent regret}]
\label{cor:BasicHolderRegretNoGap}
  Under (A1)-(A2), the expected cumulative
  regret of W-UCB satisfies the following gap-independent
  bound: there exists a universal constant $c>0$\footnote{The constant $c$ is universal as it does not depend on the problem-dependent quantities such as $K$, $n$, and the underlying distributions.} such that for all $n$,
  $ \E \left[ R_n \right] \le M K^{\alpha/2} \left(\frac{3}{2}(2L)^{2/\alpha} \log n + c \right)^{\frac{\alpha}{2}} \; n^{\frac{2-\alpha}{2}}.$ 
\end{corollary}
\begin{proof}
See Appendix~\ref{sec:appendix-gapindependentregret} in the supplementary material.
\end{proof}

{\color{blue} 
\begin{remark}
We can recover the $O(\sqrt{n})$ regret bound (or the same dependence on the gaps) as in regular UCB for the case when $\alpha=1$, i.e., Lipschitz weights. On the other hand, when $\alpha <1$, the regret bounds are weaker than $O(\sqrt{n})$. Note that the weight function recommended in~\cite{tversky1992advances} is H\"{o}lder-continuous with exponent $\alpha$ strictly less than $1$.
\end{remark}
}
\begin{remark}
	In \cite{cassel2018general}, the authors propose the `Strongly stable EDPM' condition for a risk measure, under which one can obtain regret bounds. In our setting, for the weight-distorted reward for non-negative r.v.s, this condition requires the existence of a norm $|| \cdot ||$, on the space of distribution functions, and $q \geq 1, b > 0$ such that, for all distributions $F,G$ of non-negative r.v.s,
	\begin{align}
	\left| \intinfinity w(1-F(z))dz - \intinfinity w(1-G(z)) dz  \right| \le  b \left( || F- G || + || F- G||^q \right).\label{eq:ss}
	\end{align}
	The above condition does not hold, as illustrated by the following example. Consider $F$ and $G$ to be the distribution functions of Bernoulli distributions with parameters $4\epsilon$ and $\epsilon$, respectively, for $\epsilon > 0$, and take the weight distortion function to be $w(x) = \sqrt{x}$. The left hand side of \eqref{eq:ss} is $\sqrt{4\epsilon} - \sqrt{\epsilon} = \sqrt{\epsilon}$. On the other hand,  $(F - G)(x)  = \epsilon \mathbf{1}_{[0,1)}(x)$. By homogeneity of norms, $||F - G||$ must scale linearly with $\epsilon$, and hence the inequality in \eqref{eq:ss} cannot hold for sufficiently small $\epsilon$. 
	Thus, the approach of \cite{cassel2018general} cannot be adopted for handling a cumulative prospect theory-based risk measure. Our approach is to work directly with a concentration result for estimating distorted value, and obtain regret bounds. 
\end{remark}
{\color{blue}
\begin{remark}
It is worth to note that, for ease of exposition, we have focused on the case of bounded support in the bandit algorithms. However, it is straightforward to extend these algorithms to cover the sub-Gaussian case, and the modification here would be to pull each arm a certain number of times in the initialization phase, so that the concentration result for sub-Gaussian case given in Proposition~\ref{prop:sub-gaussian-concentration} applies. 
\end{remark}
}
The following result shows that one can not hope to obtain better regret than that of W-UCB (Theorem \ref{thm:BasicHolderRegretGap}) over the class of \holderNS-continuous weight functions, i.e., weight functions satisfying (A1)-(A2), by exhibiting a matching lower bound for regret, in the style of \cite{lai1985lowerbd}.

%\begin{theorem}[\textbf{\textit{Regret lower bound}}]
%\label{thm:karmedlowerbdInformal}
%For any learning algorithm with sub-polynomial regret in the time horizon, there exists (1) a weight function which is monotone increasing and $\alpha$-\holder continuous with constant $L$, and (2) a set of cost distributions for the arms with support bounded by $M$, for which the algorithm's regret satisfies\\
%\centerline{${\expect{R_n}} = \Omega \left( \sum_{\{k: \Delta_k > 0\}} \dfrac{(LM)^{2/\alpha} \log n}{4\Delta_k^{2/\alpha - 1}} \right)$.}
%\end{theorem}

\begin{theorem}[\textit{Regret lower bound}]
\label{thm:karmedlowerbd}
Consider a learning algorithm for the $K$-armed weight-distorted bandit problem with the following property: For any weight distortion function $w:[0,1] \to [0,1]$ such that $w(0) = 0$ and $w(1) = 1,$ any set of reward distributions with rewards bounded by $M$, any $a > 0$ and any sub-optimal arm $k \in \{ 2, 3, \dots, K \},$ the expected number of plays of arm $k$ satisfies $\expect{T_k(n)} = o(n^a)$. 
Then, for any constant $\alpha \in (0,1]$ and $L \in (0, 2^{ \alpha - 1 } ],$ there exists a monotonically increasing weight function $w: [0, 1] \rightarrow [0, 1]$ with $w(0) = 0$ and $w(1) =1,$ which is $\alpha$-H\"{o}lder-continuous with constant $L$, and a set of reward distributions bounded by $M$, for which the algorithm's regret satisfies
\[  \liminf_{n \to \infty} \frac{\expect{R_n}}{\log n} \geq  \sum\limits_{k=2}^K \dfrac{(LM)^{2/\alpha}}{4\Delta_k^{2/\alpha - 1}}. \]
\end{theorem}

\begin{proof}[Proof sketch for Theorem~\ref{thm:karmedlowerbd}]
For convenience, a proof sketch is given for $M = 1$. The key ingredient in the proof is the seminal lower-bound on the number of suboptimal arm plays derived by Lai and Robbins \cite{lai1985lowerbd}. Their result shows that for any algorithm that plays suboptimal arms only a sub-polynomial number of times in the time horizon, 
\[ \liminf_{n \to \infty} \frac{\expect{T_k(n)}}{\log n} \geq \frac{1}{D(F_k || F_{1})} \]
for any suboptimal arm $k$ and we assumed that arm~1 is the optimal arm. It is worth noting that the argument at its core uses only a change-of-measure idea and the sub-polynomial regret hypothesis, and is thus unaffected by the fact that we measure regret by distorted, i.e., non-expected, rewards. Using this along with the definition of regret, we show that
\begin{align}
\liminf_{n \to \infty} \frac{\expect{R_n}}{\log n} &\geq \sum\limits_{k=2}^K \frac{\Delta_k}{D(F_k || F_{1})}. \label{eq:liminfbound_sketch}
\end{align}
We now construct a set of reward distributions and a weight function for which the limiting property claimed in the theorem holds. Consider Bernoulli distributions for the arms' rewards, i.e., arm $k$'s reward is Ber($p_k$). It gives a simple expression for the weight-distorted reward of any arm $k$ as $\mu_k = \int_0^1 w(p_k) dz = w(p_k)$, and, consequently, $\Delta_k = w(p_{1}) - w(p_k).$
Consider now a weight function $w:[0,1] \to [0,1]$ which is monotone increasing from $0$ to $1$, H\"{o}lder-continuous with any desired constant $L > 0$ and exponent $\alpha \in (0,1]$, and, moreover, satisfies the following ``\holder continuity property from below'': for some $\tilde{p} \in (0,1)$ (say, $\tilde{p} = 1/2$), $|w(\tilde{p}) - w(p)| \geq L |\tilde{p} - p|^\alpha$. Such a weight function can always be constructed, e.g., for $\alpha = 1/2, L = 1/\sqrt{2},$ take the function $w(x) = \frac{1}{2} - \frac{1}{\sqrt{2}}\sqrt{\frac{1}{2} - x}$ for $x \in [0, 1/2]$, and $w(x) = \frac{1}{2} + \frac{1}{\sqrt{2}}\sqrt{x - \frac{1}{2}}$ for $x \in (1/2, 1]$. (This is essentially formed by gluing together two inverted and scaled copies of the function $\sqrt{x}$, to make an S-shaped function infinitely steep at $x = 1/2$.)

For such a weight function, putting $p_{1} = \tilde{p} = 1/2$ and $p_k < p_{1}$, $k \neq 1$, we show that 
\begin{align*} 
D(F_k || F_{1}) 
&\leq 4 \left( \frac{\Delta_k}{L} \right)^{2/\alpha}. 
\end{align*}
The main claim can be easily established using the above inequality and~\eqref{eq:liminfbound_sketch}. 
For a detailed proof, the reader is referred to Appendix~\ref{sec:appendix-regretlowerbound} in the supplementary material.
\end{proof}
%Our proof of the above theorem relies on the classic cumulative regret lower bound given in~\cite{lai1985lowerbd}. However, the non-trivial part of the proof is to construct a weight function that satisfies the lower bound given in the theorem statement.

We now present a result which gives a problem-independent lower bound on the regret. 
\begin{corollary} 
\label{cor:prob-indepdent-regret-lowerbound}
Consider a H\"{o}lder-continuous weight function with parameters $\alpha$ and $L$. For any bandit algorithm $\mathcal{A}$, there exists a $K$-armed bandit problem instance $v$ such that 
\begin{align*}
R^{\mathcal{A}}_n(v) \geq c L n^{1-\frac{\alpha}{2}} K^{\frac{\alpha}{2}},
\end{align*}  
where $R^{\mathcal{A}}_n(v)$ is the regret incurred by the Algorithm $\mathcal{A}$ on the bandit problem instance $v$ until round $n$, and $c$ is a universal constant.
\end{corollary}
\begin{proof}
Follows directly from Theorem~1.12 in~\cite{prashanth6046}. 
\end{proof}
