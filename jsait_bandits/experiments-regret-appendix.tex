We describe two sets of experiments pertaining to the $K$-armed settings. For both problems, we take the $S$-shaped weight function $w(p) = \frac{p^\eta}{\left(p^\eta + (1-p)^\eta \right)^{1/\eta}}$, with $\eta = 0.61$ and H\"{o}lder parameters as $\alpha = \eta$, $L = 1/\eta$ to model perceived distortions in cost/reward. It has been observed in \cite{tversky1992advances} that this distortion weight function is a good
fit to explain distorted value preferences among human beings.

We first study three stylized $2$-armed bandit problems which, in part, draw upon experiments carried out by~\cite{tversky1992advances} on human subjects in their studies of cumulative prospect theory. Figures \ref{fig:karmed} (a) and (b) describe each problem setting in detail (following the convention of this paper of modeling costs, ``\$$x$ w.p. $p$" is taken to mean a loss of \$$x$ suffered with a probability of $p$.)

For the first problem, the weight function $w$ gives the distorted cost of arm 1 as \$$10.55$, much higher than its expected cost of \$$5$ and thus more expensive due to the deterministic Arm 2 with a (distorted and expected) cost of \$$7$. The distortion in costs thus shifts the optimal arm from Arm 1 to Arm 2, and an online learning algorithm must be aware of this effect in order to attain low regret with respect to choosing Arm 2. A similar pattern is true for the other problem involving truly stochastic arms -- weight distortion favors the arm with the higher cost in true expectation. 

We benchmark the cumulative regret of two algorithms -- (a) the well-known UCB algorithm \cite{auer2002finite}, and (b) W-UCB; the results are as in Figure \ref{fig:karmed}. In the experiments, UCB is not aware of the distorted weighting and hence suffers linear regret with respect to playing the optimal distorted arm, due to converging to essentially a `wrong' arm. On the other hand, the W-UCB algorithm, being designed to explicitly account for distorted cost perception, estimates the distortion using sample-based quantiles and exhibits significantly lower regret. 

\begin{figure}
\centering
  \begin{tabular}{cc}
  \hspace{-1em}
  \begin{subfigure}{0.5\textwidth}
  \label{fig:a}
 \tabl{c}{\scalebox{0.75}{\begin{tikzpicture}
      \begin{axis}[
	xlabel={Rounds},
	ylabel={Cumulative regret},
       clip mode=individual,grid,grid style={gray!30}]
      % UCB
      \addplot+[error bars/.cd,y dir=both,y explicit, every nth mark=10000] table [x=X,y=Y,y error=Y_error,col sep=comma] {karmed-UCB-Setting1.txt};
      % W-UCB
      \addplot+[error bars/.cd,y dir=both,y explicit, every nth mark=10000] table [x=X,y=Y,y error=Y_error,col sep=comma] {karmed-WUCB-Setting1.txt};
      \legend{UCB,W-UCB}
%       \errorband[darkgray!50!white, opacity=0.3]{karmed-WUCB-Setting1.txt}{0}{1}{2}
%       \addplot [thick, darkgray,mark=*,mark options={scale=0.75}] table [x index=0, y index=1,col sep=comma] {results/karmed-WUCB-Setting1.txt};
      \end{axis}
      \end{tikzpicture}}\\}
%  
%    \includegraphics[width=\textwidth]{Env2b-eps-converted-to.pdf}
  \end{subfigure}
  &
  \hspace{-1em}
  \begin{subfigure}{0.5\textwidth}
  \label{fig:b}
   \tabl{c}{\scalebox{0.75}{\begin{tikzpicture}
      \begin{axis}[
	xlabel={Rounds},
	ylabel={Cumulative regret},
       clip mode=individual,grid,grid style={gray!30}]
      % UCB
      \addplot+[error bars/.cd,y dir=both,y explicit, every nth mark=10000] table [x=X,y=Y,y error=Y_error,col sep=comma] {karmed-UCB-Setting2.txt};
      % W-UCB
      \addplot+[error bars/.cd,y dir=both,y explicit, every nth mark=10000] table [x=X,y=Y,y error=Y_error,col sep=comma] {karmed-WUCB-Setting2.txt};
      \legend{UCB,W-UCB}
%       \errorband[darkgray!50!white, opacity=0.3]{karmed-WUCB-Setting1.txt}{0}{1}{2}
%       \addplot [thick, darkgray,mark=*,mark options={scale=0.75}] table [x index=0, y index=1,col sep=comma] {results/karmed-WUCB-Setting1.txt};
      \end{axis}
      \end{tikzpicture}}\\}

%   \includegraphics[width=\textwidth]{Env3b-eps-converted-to.pdf}
  \end{subfigure}
  \end{tabular}
\caption{Cumulative regret along with standard error from $100$ replications for the UCB and W-UCB algorithms for 2 stochastic $K$-armed bandit environments: 
% The graphs show sample mean cumulative regret for the specified number of rounds of the game, along with standard error bars, across $100$ runs of each algorithm for each horizon value. 
(a) {\em Arm 1:} (\$50 w.p. 0.1, \$$0$ w.p. 0.9) {\bf vs.} {\em Arm 2:} \$7 w.p. 1. (b) {\em Arm 1:} (\$500 w.p. 0.01, \$$0$ w.p. 0.99) {\bf vs.} {\em Arm 2:} (\$250 w.p. 0.03, \$$0$ w.p. 0.97).%
%(c) {\em Arm 1:} (\$100 w.p. 0.01, \$$0$ w.p. 0.99) {\bf vs.} {\em Arm 2:} (\$10 w.p. 0.2, \$$0$ w.p. 0.8). 
}
\label{fig:karmed}
\end{figure}

In the following set up, we use a heuristic estimator for weight-distorted reward as $W$(empirical mean) for distributions with support $\subseteq$ $[0, 1].$  Note that our weight-distorted value estimate turns out to be $W(\textrm{empirical mean})$ for the Bernoulli distributions. We adapt the classic UCB~\cite{auer2002finite} policy to this set up by taking the above \textbf{W}(\textbf{E}mpirical mean) as a surrogate for the weight-distorted reward estimate and call the policy WE-UCB.   

\textbf{Setup(*).} We consider a simple two-armed stochastic bandit with arm distributions' as follows: 
\[
\text{Arm-1} =
\begin{cases}
  1  & \text{w.p.} \quad  0.1 \\
  0.2  & \text{w.p.} \quad 0.1 \\
  0  & \text{w.p.} \quad 0.8
\end{cases}
\quad \quad
\text{Arm-2} =
\begin{cases}
  1  & \text{w.p.} \quad  10^{-3} \\
  0.2  & \text{w.p.} \quad 0.8 \\
  0  & \text{w.p.} \quad 0.199
\end{cases}
\]
For convenience, we treat these arm distributions as corresponding to rewards and hence the optimal arm is defined as the arm with highest weight-distorted reward. In the following, we show the expected and weight-distorted values of both arms: 
\begin{table}[ht]
\centering % used for centering table
\begin{tabular}{c c c} % centered columns (4 columns)
\hline %inserts a horizontal line
Distribution & Expected value & Weight-distorted value \\ [0.5ex] % inserts table
\hline % inserts single horizontal line
Arm-1 & 0.12 & 0.2005 \\ % inserting body of the table
Arm-2 & 0.161 & 0.1327\\ [1ex] % [1ex] adds vertical space
\hline %inserts single line
\end{tabular}
\label{table:new_setup2} % is used to refer this table in the text
\end{table}

Thus, arm-2 is optimal with respect to expected value and arm-1 is optimal with respect to weight-distorted reward where the latter is calculated empirically using a very large number of i.i.d. samples. We now present the comparison of UCB, WE-UCB and W-UCB for a fixed time horizon in Figure~\ref{fig:setup_2_regret}. We fix the time horizon as $10^4$ and run the algorithms for 100 runs and averaged them to obtain the results. It is clear from Figure~\ref{fig:setup_2_regret} that our algorithm W-UCB (achieves logarithmic regret) outperforms both UCB and WE-UCB (both achieve linear regrets) in this simple setup. Hence, these results justify the need of our algorithms under the weight-distorted setup considered herein.   

\begin{figure}[htb]
\center{\includegraphics[scale=0.6]{setup_2_regret.eps}}
\caption{\label{fig:setup_2_regret} Comparison of UCB, WE-UCB and W-UCB for regret minimisation in the stochastic bandits.}
\end{figure}