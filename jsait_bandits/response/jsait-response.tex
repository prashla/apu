\documentclass{article} % For LaTeX2e
% \usepackage{nips13submit_e,times}
% \usepackage{nips12submit_e,times}
\usepackage{macros}
\oddsidemargin=0pt
\textwidth=455pt
\textheight=620pt
\voffset=-20pt
\marginparwidth=0pt
\marginparpush=0pt
\marginparsep=0pt
\evensidemargin=0pt


\title{Response to manuscript titled ``Bandits to emulate human decision making: A cumulative prospect theory approach''}
\date{}

\begin{document}

\maketitle

\hrule
\vspace*{3mm}
\noindent\emph{Note}:  We are grateful to the reviewers for the very thoughtful comments. We believe that they have led to a substantial improvement of this article. We hope that the revised manuscript satisfactorily addresses the concerns of the reviewers. 

\noindent In the following, we list the reviewers' concerns (in italics) and interleave our responses. We highlight the changes made to the revised manuscript using blue color, reproduced in the response for the reviewers' quick reference. 
\\[-1mm] \hrule
\vspace*{4mm}


\section*{Reviewer-1}
\begin{enumerate}
\item \textit{My major concern is that the proposed method in all four scenarios assumes the weight function $\omega$ is known in advance. This leads to two issues. (1) Given a known weight function $\omega$, Algorithm 1 estimates the weight-distorted reward. Afterwards, the proposed algorithms 2-5 are straightforward extensions of the corresponding traditional UCB or OFUL algorithms for K-armed/linear bandits by replacing the reward to the new weight-distorted reward. What would be new additional technical novelties? (2) What to do if the $\omega$ is unknown? Is it possible to estimate the weight function $\omega$? How sensitivity of the proposed algorithms with respect to the mis-specification of the weight function $\omega$?}

On (1): We highlight below the technical novelties in our algorithms/analyses.
\begin{enumerate}
	\item  While the regret upper bound for UCB algorithm is a straightforward variation of the proof for regular UCB, the same is not true for the lower bound. In particular, we construct a bandit problem instance, with a carefully chosen weight function, on which any algorithm will suffer a regret that matches the upper bound obtained for W-LUCB, up to constant factors. 
	\item In the regret analysis of W-OFUL algorithm, it is not straightforward to compute the weight-distorted reward after translation, and this poses a significant challenge for the arm-dependent noise model considered in the manuscript. Lemma~8 on p.54 of the revised manuscript provides the necessary result for the weight-distorted value under scaling, which aids in the derivation of W-OFUL algorithm's regret bound.
	\item We have provided a concentration result for the estimation of weight-distorted reward for the case when the underlying distribution is sub-Gaussian. We have moved the sub-Gaussian concentration result from Appendix to the main manuscript, which can be found as Proposition~1 on p.13, in the revised manuscript. As mentioned in Remark~2 on p.13 of the revised manuscript, we adopt a truncation-based approach to handle the sub-Gaussian case. This is because the proof of Theorem~1, which handled distributions with bounded support, does not extend to cover sub-Gaussian distributions. For ease of exposition, we have focused on the case of bounded support in the bandit algorithms. However, it is straightforward to extend these algorithms to cover the sub-Gaussian case, and the modification here would be to pull each arm a certain number of times in the initialization phase, so that the concentration result for sub-Gaussian case applies. We have added the same as a remark on p.15, Remark~6, in the revised manuscript. The proof of the concentration result for the truncated estimator  is available in Appendix A.   
\end{enumerate}

On (2): We assume that the algorithm *knows* these parameters in advance. Behavior scientists have proposed a specific choice
for the weight function and have shown via experiments involving human subjects that these choices work across multiple domains and realistically capture human preferences. For instance, there are several papers which conclude that the weight function is inverted­ S-shaped (cf. pp. 348­--349 and Section 5.1.1 of (Starmer, 2000) reference in our paper). We assume that the weight function belongs to a \holder class, which includes the candidate $w(p)=\frac{p^{0.61}}{(p^{0.61} + (1-p)^{0.61})^\frac{1}{0.61}}$ recommended in  Tversky and Kahnemann's  paper on CPT. 

We acknowledge that robustness to unknown parameters should indeed be studied in a
sensitivity study. We view this as a necessary next step, after covering the base (of what we
do here).

\item \textit{Algorithm 1 and Theorem 1 require sufficiently large i.i.d. samples of each arm in order to obtain a desirable estimation error bound. This can be treated as the initial pure exploration stage. In the regret bounds of Theorems 2-5, do you include the regret loss of the initial pure exploration stage?}

We would like to clarify that, as with the regular UCB algorithm, the W-UCB and W-LUCB algorithms do not involve an explicit exploration phase. The exploration is handled implicitly using the principle of optimism.  

The concentration bound in Theorem 1 establishes that to acheive an accuracy $\epsilon$ for the weight-distorted reward estimate, $O\left(\frac{1}{\epsilon^{2/\alpha}}\right)$ number of samples are necessary. If $\alpha=1$, then the sample complexity is comparable to the one obtained through Hoeffding's inequality, while the sample complexity is a little higher for $\alpha<1$.
The bound in Theorem 1 can be viewed as a generalization of Hoeffding's inequality, since the weight function $w(p)=p$ is Lipschitz, i.e., $\alpha=1$.

\item \textit{The key algorithms and regret bound analysis in this paper has been published in AAAI 2017. I did not see additional novelty beyond this AAAI conference version.}

This paper is an extended version of our AAAI 2017 conference paper, as highlighted in the cover letter.
In comparison to the conference version, this paper includes the best arm identification problem formulation as well as algorithms, in addition to the regret minimization framework considered there, includes formal proofs of convergence for both regret and best arm settings, includes additional experiments and a revised presentation. Note that the proofs of convergence in the regret minimization setting are not part of the AAAI proceedings.
\end{enumerate}

\section*{Reviewer-2}
\begin{enumerate}
\item \textit{The paper proposes multiple algorithms, while only W-OFUL is evaluated in the main paper. The extension to the best arm identification seems to be the main contribution of the paper, compared to [1]. It is unclear why the paper puts all experimental results about best arm identification in the appendix. If we keep it as it is now, one potential concern might be that the experimental results in the main paper seem very similar/incremental, compared to the results in [1]. Besides, LUCB [21], SR [22], G-allocation in [23] are early works of best arm identification. Can your developed algorithms outperform and/or improve the recent works of best arm identification? At least, some discussions are very helpful. For example,}

\emph{[1]. Russo, Daniel. ``Simple Bayesian algorithms for best arm identification." Conference on Learning Theory. 2016.}

\emph{[2]. Jedra, Yassir, and Alexandre Proutiere. ``Optimal Best-arm Identification in Linear Bandits." Advances in Neural Information Processing Systems 33 (2020).}

We thank the reviewer for pointing out these references. We have revised the manuscript to include these references in the Related work subsection in the Introduction section of the revised manuscript. In [1], the authors consider the best arm identification problem in stochastic multi-armed bandits from a Bayesian perspective as opposed to the frequentist approach and weight distortion setup used in our work. We believe that devising/analyzing weight-distorted variants of the algorithms proposed in [1] is challenging due to the Bayesian approach, and beyond the scope of this work.

In the recent work [2] on best arm identification in linear bandits, the authors propose and study the sample complexity of an algorithm that is inspired by the track-and-stop approach to best arm identification. The authors establish asymptotic optimality of their algorithm. In contrast, we consider the G-allocation algorithm from [3] and devise a weight-distorted variant. In terms of theoretical guarantees, we provide finite sample guarantees for the proposed algorithm. It would be an interesting future research direction to extend the algorithm in [2] to our weight-distorted setup and investigating the asymptotic/non-asymptotic performance of the same. 

Reference [3]. Soare, Marta, Alessandro Lazaric, and Rémi Munos. ``Best-arm identification in linear bandits." arXiv preprint arXiv:1409.6110 (2014). 

On the experiments: Due to space constraints, we have chosen to keep the simulation set up which is closer to the real world application i.e., vehicular traffic routing,  in the main manuscript. The supplementary material contains simulation experiments on synthetic setups in K-armed as well as linear bandits settings.

\item \textit{The current experimental results and descriptions in the main paper seem confusing. It is unclear what algorithm (OFUL, WOFUL or others) is used to get the results in Table I, based on the current descriptions. It is unclear how to derive the conclusion ‘The shorter route is preferred by OFUL algorithm, while the distortion-aware WOFUL algorithm finds the longer route’ from the results in Table I or other reported numbers.}

We would like to emphasize that Table II in the revised manuscript provides only the expected and distorted excess delays of the routing problem considered i.e., the expected and distorted values of paths in the problem set up. From Table II, we observe that Route 7 is the lowest expected delay path, and Route 8 is the lowest weight-distorted value path. Next, from our simulations in GLD, we observed that OFUL and WOFUL algorithms converged to Route 7 and 8, respectively. 

As mentioned on p.30 in the manuscript, we use a baseline to compute the delays which is common in the context of CPT. Details of the baseline computation are provided on  p.30 in the manuscript. Further, details about the edge delays that are parameterized by $\hat{\theta}_{\textrm{off}}$ is given in equation~(24) on p.31 in the manuscript.

\item \textit{The paper proposes multiple algorithms. The names of these algorithms usually do not reflect what the algorithms are used for (e.g., regret minimization or best arm identification, in k-armed bandit or linear bandit setting). To avoid confusion, it would be great if the user can use a table in the introduction, to summarize all the algorithms proposed in the paper. For example, the column can list the tasks (regret minimization or best arm identification, with Fixed Budget or not, etc), and the row can list the settings (k-armed bandit or linear bandit).}\

As per the reviewer's suggestion, we have added the following table, in the ``Introduction'' section in the revised manuscript:
%, which lists all algorithms under their respective settings for sake of better readability.
\begin{table}[h]
	\caption{All algorithms under various settings considered in the paper.}
	\label{tab:algorithms_list}
	\centering
	\begin{tabular}{|c|c|c|}
		\toprule 
		\textbf{Bandit type}& \textbf{Objective} & \textbf{Algorithm} \\\midrule
		K-armed & Regret minimization & W-UCB\\\midrule
		K-armed & Best arm identification under fixed confidence & W-LUCB\\\midrule
		K-armed & Best arm identification under fixed budget & W-SR\\\midrule
		Linear & Regret minimization & W-OFUL\\\midrule
		Linear & Best arm identification under fixed confidence & W-G\\\midrule
	\end{tabular}
	\end{table}

%\begin{table}[h]
%	\caption{Summary of algorithms' performance bounds and universal lower bounds in the corresponding settings under $K$-armed bandits.}
%	\label{tab:k_armed_bounds_summary_table}
%	\centering
%	\begin{tabular}{|c|c|c|c|}
%		\toprule 
%		\textbf{Objective} & \textbf{Algorithm} & \textbf{Algorithm's bound} & \textbf{Universal lower bound} \\\midrule
%		Regret minimization & W-UCB & $ \mathbb{E} \left[ R_n \right] \le \sum\limits_{\{k:\Delta_k>0\}} \dfrac{3(2LM)^{2/\alpha}\log n}{2\Delta_k^{2/\alpha - 1}} + MK\left(1 + \dfrac{2\pi^2}{3} \right)$ & $\liminf_{n \to \infty} \frac{\mathbb{E} \left[ R_n \right]}{\log n} \geq  \sum\limits_{k=2}^K \dfrac{(LM)^{2/\alpha}}{4\Delta_k^{2/\alpha - 1}}$ \\\midrule
%	    Best arm identification under fixed confidence & W-LUCB & $\mathbb{E} \left[ N^{CL} \right] = O \left( H_\alpha \log \frac{H_\alpha}{\delta} \right)$ & $\mathbb{E} \left[ N^A \right] \geq O \left( H_\alpha \log \left( \frac{1}{2.4 \delta} \right) \right)$ \\\midrule
%		Best arm identification under fixed budget & W-SR & $\mathbb{P} \left( J_n \neq 1 \right) \leq \frac{K(K-1)}{2} \exp \left( - \frac{n-K}{\overline{\log} K} \frac{1}{M^{2/\alpha}} \frac{1}{H_{2, \alpha}} \right)$ & $\max\limits_{1 \leq i \leq K} \mathbb{P}_i (J_n \neq i) \geq \frac{1}{6} \exp \left[ \frac{-60 n}{H_{1,\alpha}} - 2 \sqrt{n \log(6nK)} \right] $ \\\midrule
%	\end{tabular}
%	\end{table}


\item \textit{The paper mentions both $w(p) = \frac{p^\eta}{ (p^\eta +(1-p) ^\eta)^{1/\eta}}$ and $w(t) := t^\alpha.$ What is the relation between $\eta$ and $\alpha?$ Are they the same? Why there are two definitions? It is unclear why 0.1 is adopt in the weight function in some experiments, while 0.61 is adopt in the weight function in some other experiments. Might the comparison results be sensitive to this value? Please elaborate.}

Both weight functions are \holder-continuous, and there is no explicit relation between $\eta$ and $\alpha$.  For the K-armed bandit experiments, we adopted the first weight function with exponent value $0.61$, as it is the choice recommended in Tversky and Kahnemann's paper on CPT. A weight function similar to $w(t)=t^\alpha$ is employed in the proof of the lower bound (proof of Theorem~3 in the revised manuscript), to construct a problem instance involving Bernoulli arms. 

Next, in the traffic routing application, we set the exponent $\eta$ of the first weight function to $0.1$, as it leads to an emphatic difference in the best route w.r.t. expected and distorted delays. At higher values of the exponent, i.e.,  $\eta\ge 0.5$, the best routes under expectation/distortion turned out to be the same for the setup chosen in the manuscript. 

\item \textit{The title of the paper seems general and can be more specific. It does not reflect the main contributions of the paper: handling distorted probabilities on the reward distributions in the regret minimization and (especially) best arm identification task.}


We have modified the title to \\
``Bandit algorithms to emulate human decision making using probabilistic distortions''.

We have not added the phrases ``regret minimization'' and ``best-arm identification'' to avoid a long title.
\end{enumerate}

\section*{Reviewer-3}
\begin{enumerate}
\item \textit{No good regret bound is given for W-OFUL. The order is sublinear, but I fail to see good contribution in this theoretical results [equation 62, 63].}

We have provided a $\tilde O(\sqrt{n})$ regret bound for the W-OFUL algorithm in Theorem 10 of the manuscript.  This bound is comparable to regret bound of the regular OFUL algorithm, and our bound is interesting because the distortion of the reward distribution via a weight function does not impact the order of the bound on problem-independent regret. Further, as noted in Remarks 11 and 12, the regret bound for W-OFUL would match the lower bound up to constant factors.  

The regret analysis of W-OFUL algorithm is not a straightforward variation of the OFUL case, as we compute the weight-distorted reward after translation, and this poses a significant challenge for the arm-dependent noise model considered in the manuscript. Lemma~8 on p.54 of the revised manuscript provides the necessary result for the weight-distorted value under scaling, which aids in the derivation of W-OFUL algorithm's regret bound.

\item \textit{This paper focuses on the regret analysis. The distortion seems a difficult to get optimal values. Is there any technical challenges on computing the optimal, and what is the solution to tackle them? This paper may use an existing way to compute, and this further undermines the novelty.}   

\textit{Multiple distortion algorithms are proposed, W-SR, W-OFUL, W-UCB, and W-LCB. They are all baed on classical algorithms. It is good to see their distortion-aware versions, and the paper covers many important problems in bandit. My concern is that the paper looks like simply combining the distortion with existing bandit algorithms. After briefly reading OFUL and CPL papers, the proof techniques almost borrowed from the classical sketch and some proof does not provide detailed/better bound compared with previous papers.
For example, in the CPL sample complexity proof, the two assumptions are nearly identical to the original CPL paper.}

We highlight below the technical novelties in our algorithms/analyses.
\begin{enumerate}
	\item  While the regret upper bound for UCB algorithm is a straightforward variation of the proof for regular UCB, the same is not true for the lower bound. In particular, we construct a bandit problem instance, with a carefully chosen weight function, on which any algorithm will suffer a regret that matches the upper bound obtained for W-LUCB, up to constant factors. 
	\item In the regret analysis of W-OFUL algorithm, it is not straightforward to compute the weight-distorted reward after translation, and this poses a significant challenge for the arm-dependent noise model considered in the manuscript. Lemma~8 on p.54 of the revised manuscript provides the necessary result for the weight-distorted value under scaling, which aids in the derivation of W-OFUL algorithm's regret bound.

	\item We have provided a concentration result for the estimation of weight-distorted reward for the case when the underlying distribution is sub-Gaussian. We have moved the sub-Gaussian concentration result from Appendix to the main manuscript, which can be found as Proposition~1 on p.13, in the revised manuscript. As mentioned in Remark~2 on p.13 of the revised manuscript, we adopt a truncation-based approach to handle the sub-Gaussian case. This is because the proof of Theorem~1, which handled distributions with bounded support, does not extend to cover sub-Gaussian distributions. For ease of exposition, we have focused on the case of bounded support in the bandit algorithms. However, it is straightforward to extend these algorithms to cover the sub-Gaussian case, and the modification here would be to pull each arm a certain number of times in the initialization phase, so that the concentration result for sub-Gaussian case applies. We have added the same as a remark on p.15, Remark~6, in the revised manuscript. The proof of the concentration result for the truncated estimator  is available in Appendix A.   
\end{enumerate}

We would like to point out the following in terms of comparison with the below prior work. 

\textit{1. Weighted bandits or: How bandits learn distorted values that are not expected} 

\textit{2. Cumulative prospect theory meets reinforcement learning: prediction and control}

\textit{3. Stochastic optimization in a cumulative prospect theory framework}

This paper is an extended version of the first reference above, as highlighted in the cover letter. In comparison to the conference version, this paper includes the best arm identification problem formulation as well as algorithms, in addition to the regret minimization framework considered there, includes formal proofs of convergence for both regret and best arm settings, includes additional experiments and a revised presentation. Note that the proofs of convergence in the regret minimization setting are not part of the AAAI proceedings.

As far as the second and third references above are concerned, we would like to point out that a bandit formulation was not considered under distorted rewards there. In the K-armed bandits case with bounded support, we use the concentration bound for weight-distorted reward estimation from the second reference above, while we derive an improved bound for the case of sub-Gaussian distributions. 

In Proposition 3 of the third reference above, the authors provide a tail bound of the order \\ $\left(2n e^{-n^\frac{\alpha}{2+\alpha}} 
+ 2 e^{-n^{\frac{\alpha}{2+\alpha}}\left(\frac{\epsilon}{2L}\right)^{\frac{2}{\alpha}}}\right)$ for weight-distorted value estimation in the sub-Gaussian case, for all $n \geq \left(\frac{\ln2-\ln\epsilon}{2\alpha}\right)^{\alpha+2}$. In contrast, the tail bound provided in Proposition 1  on p.13 of this manuscript is of the order $\left(2 e^{-c n \left(\epsilon - \frac{8L}{\alpha n^{\alpha/2}}\right)^{\frac{2}{\alpha}}}\right)$ holds for all $n\ge 1$. Our bound exhibits improved dependence on the number of samples $n$, while having a similar dependence on the accuracy $\epsilon$. 
\end{enumerate} 

\section*{Reviewer-4}
\begin{enumerate}
\item \textit{There are some restrictions on the weight function, $w$, which I understand are needed for analysis. But can they be relaxed to just continuous functions? And what will be the practical scenarios where we will \holder-continuous weight function?}

We have relaxed from Lipschitz continuous to \holder continuous, to accommodate the weight function $w(p)=\frac{p^{0.61}}{(p^{0.61} + (1-p)^{0.61})^\frac{1}{0.61}}$ recommended in Tversky Kahnemann's paper on CPT.  
%See (Starmer, 2000) for another weight candidate.  
Recall the definition of weight-distorted value here: 
\begin{align}
\hspace{-0.4em}\mu_x = \intinfinity w(\prob{X > z}) dz - \intinfinity w(\prob{-X > z}) dz.\label{eq:cpt-intro}
\end{align}
Note that, for a general continuous weight function, weight-distorted value defined in~\eqref{eq:cpt-intro} may be infinite. In particular, observe that the first integral in (1), i.e., $\intinfinity w(\prob{X > z}) dz$ may diverge even if the first moment of random variable $X$ is finite. For example, suppose $X$ has the tail distribution function $\mathbb{P} (X > z) = \frac{1}{z^2}, z \in [1, +\infty)$
and weight function takes the form $w(z) = z^{\frac{1}{3}}$. Then, the first integral in (1), i.e., $\int\limits_{1}^{+\infty} z^{\frac{-2}{3}} dz$ does not even exist. A similar argument applies to the second integral in (1). 

Further, it was argued in Proposition~1 in~[1] that weight-distorted value is finite under the following assumption: 
(i). The weight function is \holder continuous with parameters $\alpha$ and $L$. 
(ii). There exists $\gamma \leq \alpha$ such that $\int\limits_{0}^{+\infty} \mathbb{P}^\gamma (X > z) dz < +\infty$ and 
$\int\limits_{0}^{+\infty} \mathbb{P}^\gamma (-X > z) dz < +\infty$ where $\mathbb{P}^\gamma(\cdot) = \left( \mathbb{P}(\cdot) \right)^\gamma.$ 

Therefore, it is an interesting future research direction to analyze weight-distorted value for continuous but not \holder continuous functions. 

Reference [1]. Jie, Cheng, et al. ``Stochastic optimization in a cumulative prospect theory framework." IEEE Transactions on Automatic Control 63.9 (2018): 2867-2882.

\item \textit{The authors present theoretical analysis for the proposed algorithms but it would be helpful to add a remark after each theorem to state what does it mean intuitively. Moreover, how the results for conventional bandits algorithm will compare. I understand a direct comparison might not be plausible due to different underlying problems but a comment on what has changed by introducing weight distortion will be more clear.}

We would like to mention that discussions regarding comparison of proposed algorithms with conventional bandits algorithms present in the original manuscript. However, as per the reviewer's suggestion, we have transformed the same into remarks. In particular, please refer to Remarks 4, 7, 8, 10, 11, 12 and 13 in the revised manuscript. 


\item \textit{If the \holder continuity is not satisfied, which means $\alpha>1$, would it then mean that W-UCB will have linear regret and UCB might be better?}

The condition ``$\alpha>1$'' would imply the weight function is Lipschitz, and in this case, the regret bound for W-UCB is comparable to that of UCB i.e., both algorithms have order $\tilde O(\sqrt{n})$ regret bound. 

\item \textit{The corollary for Theorem 3 says that there exists a weight function with suitable properties. I just wanted to understand how will an algorithm be designed for this.}

We would like to clarify that Theorem 3 and its corollary provide algorithm-independent lower bounds. The W-UCB algorithm handles \holder weights, and its regret upper bound matches the lower bound in Theorem 3, up to constant factors.

\item \textit{This paper presents bandits algorithms to tackle the situation with distortions in distributions. A vast majority of work in bandits is for the case where the expected value is considered a good measure. But it seems very similar to previous work by same authors:}

\textit{1. Weighted bandits or: How bandits learn distorted values that are not expected} 

\textit{2. Cumulative prospect theory meets reinforcement learning: prediction and control}

\textit{It will be great if the authors can discuss in detail if this work is continuation of previous work with the same key-idea.}

This paper is an extended version of the first reference above, as highlighted in the cover letter.
In comparison to the conference version, this paper includes the best arm identification problem formulation as well as algorithms, in addition to the regret minimization framework considered there, includes formal proofs of convergence for both regret and best arm settings, includes additional experiments and a revised presentation. Note that the proofs of convergence in the regret minimization setting are not part of the AAAI proceedings.

As far as the second reference above is concerned, we would like to point out that a bandit formulation was not considered under distorted rewards there. In the K-armed bandits case with bounded support, we use the concentration bound for weight-distorted reward estimation from this paper, while we derive an improved bound for the case of sub-Gaussian distributions. 

%In Proposition 3 of \cite{prashanth2018cpt}, the authors provide a tail bound of the order $\left(2n e^{-n^\frac{\alpha}{2+\alpha}} 
%+ 2 e^{-n^{\frac{\alpha}{2+\alpha}}\left(\frac{\epsilon}{2L}\right)^{\frac{2}{\alpha}}}\right)$ for weight-distorted value estimation in the sub-Gaussian case, for all $n \geq \left(\frac{\ln2-\ln\epsilon}{2\alpha}\right)^{\alpha+2}$. In contrast, the tail bound provided in Proposition  on p. x of the manuscript is of the order $\left(2 e^{-c n \left(\epsilon - \frac{8L}{\alpha n^{\alpha/2}}\right)^{\frac{2}{\alpha}}}\right)$ holds for all $n\ge 1$. Our bound exhibits improved dependence on the number of samples $n$, while having a similar dependence on the accuracy $\epsilon$. 
\end{enumerate}



\bibliographystyle{plainnat}
%\bibliography{cpt_refs}
\end{document} 
