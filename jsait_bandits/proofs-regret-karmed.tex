\subsubsection{Proof of Theorem \ref{thm:BasicHolderRegretGap}}
\label{sec:appendix-gapdependentregret}
%\begin{proof}
As in the case of regular UCB, we bound the number of times a sub-optimal arm is pulled using the technique from~\cite{munos2014survey}. 
Recall that, wlog we assumed that arm 1 is the optimal in Section~\ref{sec:model-karmed}. Suppose that a sub-optimal arm $k$ is pulled at time $m$, which implies that 
$$\widehat \mu_{k,T_k(m-1)} + \gamma_{m,T_k(m-1)} \ge  \widehat \mu_{1,T_{1}(m-1)} + \gamma_{m,T_{1}(m-1)}.$$
The UCB-value of arm $k$ can be larger than that of arm $1$ {\em only if} one of the following three conditions holds:
\begin{align}
\widehat \mu_{1,T_{1}(m-1)}  & \le \mu_*- \gamma_{m,T_{1}(m-1)}, \label{eq:optarmmeanoutside}\\
&\text{ or } \nonumber\\
\widehat \mu_{k,T_k(m-1)} & \ge \mu_k + \gamma_{m,T_k(m-1)}, \label{eq:karmmeanoutside}\\
&\text{ or } \nonumber\\
\quad \mu_* &< \mu_k \,+\, 2 \,\gamma_{m,T_{k}(m-1)}. \label{eq:cond3}  
\end{align}

Note that condition (\ref{eq:cond3}) above is equivalent to requiring 
\[T_{k}(m-1) \le  \dfrac{2^{2/\alpha -1} 3 (LM)^{2/\alpha}\log m}{\Delta_k^{2/\alpha}}.\]

Let $\kappa = \dfrac{2^{2/\alpha -1} 3 (LM)^{2/\alpha}\log n}{\Delta_k^{2/\alpha}} + 1$. When $T_{k}(m-1) \ge  \kappa$, i.e., the condition in \eqref{eq:cond3} does not hold, then either (i) arm $k$ is not pulled at time $m$, or 
(ii) \eqref{eq:optarmmeanoutside} or \eqref{eq:karmmeanoutside} occurs.
Thus, we have  
\begin{equation}
T_k(n) \le \kappa + \sum_{m=\kappa+1}^n I(I_m=k; T_k(m) > \kappa) \le \kappa + \sum_{m=\kappa+1}^n I(\eqref{eq:optarmmeanoutside} \text{ or }\eqref{eq:karmmeanoutside} \text{ occurs}). \label{eq:tkn}
\end{equation}
From Theorem \ref{thm:cpt-est}, we can upper bound the probability of occurence of \eqref{eq:optarmmeanoutside} as follows:
$$ \mathbb{P} \left( \exists l \in \lbrace 2, 3, \dots, m-1 \rbrace \text{ such that } \widehat \mu_{1,T_{1}(l-1)}  \le \mu_*- \gamma_{l,T_{1}(l-1)} \right) \le \sum_{l=1}^m \frac{2}{m^{3}} \le \frac{2}{m^{2}}.$$
A similar argument works for \eqref{eq:karmmeanoutside}.  
Plugging the bounds on the events governed by \eqref{eq:optarmmeanoutside} and \eqref{eq:karmmeanoutside} into \eqref{eq:tkn} and taking expectations, we obtain
  \begin{equation*}
	\E[T_k(n)] \leq \kappa + 2 \sum_{m=\kappa+1}^n \frac{2}{m^{2}} \le \dfrac{2^{2/\alpha -1} 3 (LM)^{2/\alpha}\log n}{\Delta_k^{2/\alpha}} + \left(1 + \dfrac{2\pi^2}{3} \right).
	\end{equation*}
  The claim follows by plugging the inequality above into the definition of expected regret, and using the fact that $\Delta_k \leq M$ for all arms $k$, as it follows that the weight-distorted reward of any distribution bounded by $M$ again admits an upper bound of $M$. \hfill$\blacksquare$
%\end{proof}

\begin{remark}
In the above proof, we have used simple union bounds for handling the random stopping times in the interest of readability. Note that improved techniques in the literature such as peeling arguments~\cite{bubeck2010jeux}, self-normalized concentration inequalities\cite{de2004self, bercu2008exponential} can also be used to obtain potentially better bounds. %However, we chose to not to apply these improved techniques to our proofs, since it makes the paper less readable.   
\end{remark}

\subsubsection{Proof of Corollary \ref{cor:BasicHolderRegretNoGap}}
\label{sec:appendix-gapindependentregret}
%\begin{proof}
  We have, by the analysis in the proof of Theorem
  \ref{thm:BasicHolderRegretGap}, that for all $k$ with
  $\Delta_k > 0$,
  $$ \E[T_k(n)] \leq \dfrac{3(2LM)^{2/\alpha}\log n}{2\Delta_k^{2/\alpha}} +\left(1 + \dfrac{2\pi^2}{3} \right).
$$
  Thus, we can write
  \begin{align*}
    \E R_n &= \sum_{k} \Delta_k \; \E [T_k(n)] = \sum_{k} \left( \Delta_k \; \E [T_k(n)]^{\frac{\alpha}{2}}  \right) \left( \E [T_k(n)]^{1-\frac{\alpha}{2}} \right) \\
    &\leq \left( \sum_k \Delta_k^{2/\alpha} \; \E [T_k(n)] \right)^{\frac{\alpha}{2}} \left( \sum_k \E [T_k(n)] \right)^{1 - \frac{\alpha}{2}} \\
    &\text{(H\"{o}lder's inequality with exponents $2/\alpha$ and $2/(2-\alpha)$)} \\
    & \leq \left( \dfrac{3K(2LM)^{2/\alpha}\log n}{2} + K M^{2/\alpha}\left(1 + \dfrac{2\pi^2}{3} \right)  \right)^{\frac{\alpha}{2}} n^{\frac{2-\alpha}{2}}, \quad \quad \text{(using $\Delta_k^{2/\alpha} \leq M^{2/\alpha}$)}. 
  \end{align*}
  This proves the result.\hfill$\blacksquare$
%\end{proof}

\subsubsection{Proof of Theorem~\ref{thm:karmedlowerbd}} 
\label{sec:appendix-regretlowerbound}
We will need the following definition to prove the theorem. For two probability distributions $p$ and $q$ on $\mathbb{R}$, define the {\em relative entropy} or {\em Kullback-Leibler (KL) divergence} $D(p || q)$ between $p$ and $q$ as 
\[ D(p || q) = \int \log \left(\frac{dp}{dq}(x)\right) dp(x) \]
if $p$ is absolutely continuous w.r.t. $q$ (i.e., $q(A) = 0 \Rightarrow p(A) = 0$ for all Borel sets $A$), and $D(p || q) = \infty$ otherwise. For instance, if $p$ and $q$ are Bernoulli distributions with parameters $a \in (0,1)$ and $b \in (0,1)$, respectively, then a simple calculation gives that $D(p || q) = a \log \frac{a}{b} + (1-a) \log \frac{1-a}{1-b}$. 
We will often use $D(F || G)$ to mean the KL divergence between distributions represented by their cumulative distribution functions $F$ and $G$, respectively. 
\begin{proof}
We first show the result for rewards bounded by $M = 1$, the extension to general $M$ follows. 

The key ingredient in the proof is the seminal lower-bound on the number of suboptimal arm plays derived by Lai and Robbins \cite{lai1985lowerbd}. Their result shows that for any algorithm that plays suboptimal arms only a sub-polynomial number of times in the time horizon, 
\[ \liminf_{n \to \infty} \frac{\expect{T_k(n)}}{\log n} \geq \frac{1}{D(F_k || F_{1})} \]
for any suboptimal arm $k$ and we assumed that arm~1 is the optimal arm. (Note that the argument at its core uses only a change-of-measure idea and the sub-polynomial regret hypothesis, and is thus unaffected by the fact that we measure regret by distorted, i.e., non-expected, rewards.)

Using this along with the definition of regret, we get
\begin{align}
	\liminf_{n \to \infty} \frac{\expect{R_n}}{\log n} &= \liminf_{n \to \infty} \frac{\sum\limits_{k=2}^K \E[T_k(n)] \Delta_k}{\log n} \nonumber \\
	&\geq \sum\limits_{k=2}^K \Delta_k \cdot \liminf_{n \to \infty} \frac{\E[T_k(n)] }{\log n} \nonumber \\
	&\geq \sum\limits_{k=2}^K \frac{\Delta_k}{D(F_k || F_{1})} \nonumber \\
	&= \sum\limits_{k=2}^K \dfrac{(2LM)^{2/\alpha}}{\Delta_k^{2/\alpha - 1}} \cdot \frac{(\Delta_k/2LM)^{2/\alpha}}{D(F_k || F_{1})}. \label{eq:liminfbound}
\end{align}

(Recall that $\Delta_k = \mu_{1} - \mu_k$ represents the difference between the weight-distorted values of arms $k$ and $1$.) \\

We now design a set of reward distributions for the arms, for which the limiting property claimed in the theorem holds. In fact, we will show that it is enough to design Bernoulli distributions for the arms' rewards, i.e., arm $k$'s reward is Ber($p_k$), or equivalently, $F_k(x) = (1-p_k)\mathbf{1}_{[0,1)}(x) + \mathbf{1}_{[1,\infty)}(x)$. This gives a simple expression for the weight-distorted reward of any arm $k$ as $\mu_k = \int_0^1 w(p_k) dz = w(p_k)$, and, consequently, $\Delta_k = w(p_{1}) - w(p_k)$, for any monotonic increasing weight function $w: [0,1] \to [0,1]$ with $w(0) = 1 $ and $w(1) = 1.$

Consider now a weight function $w:[0,1] \to [0,1]$ which is monotone increasing from $0$ to $1$, H\"{o}lder-continuous with any desired constant $L > 0$ and exponent $\alpha \in (0,1]$, and, moreover, satisfies the following ``\holder continuity property from below'': for some $\tilde{p} \in (0,1)$ (say, $\tilde{p} = 1/2$), $|w(\tilde{p}) - w(p)| \geq L |\tilde{p} - p|^\alpha$. Such a weight function can always be constructed, e.g., for $\alpha = 1/2, L = 1/\sqrt(2),$ take the function $w(x) = \frac{1}{2} - \frac{1}{\sqrt{2}}\sqrt{\frac{1}{2} - x}$ for $x \in [0, 1/2]$, and $w(x) = \frac{1}{2} + \frac{1}{\sqrt{2}}\sqrt{x - \frac{1}{2}}$ for $x \in (1/2, 1]$. (This is essentially formed by gluing together two inverted and scaled copies of the function $\sqrt{x}$, to make an S-shaped function infinitely steep at $x = 1/2$.)

For such a weight function, putting $p_{1} = \tilde{p} = 1/2$ and $p_k < p_{1}$, $k \neq 1$, we can use the standard bound\footnote{This results from using $\log x \leq x - 1$.} 
\begin{align*} 
D(F_k || F_{1}) &= p_k \log \frac{p_k}{p_{1}} + (1-p_k) \log \frac{1-p_k}{1-p_{1}}  \\
&\leq \frac{(p_{1} - p_k)^2}{p_{1}(1 - p_{1})} = 4{( p_{1} - p_k)^2} \\
&\leq 4 \left( \frac{ w(p_{1}) - w(p_k) }{L} \right)^{2/\alpha} \quad \quad \mbox{(by the ``lower-\holder'' property of $w$)} \\
&= 4 \left( \frac{\Delta_k}{L} \right)^{2/\alpha}. 
\end{align*}
Since $M = 1$ for the Bernoulli family of reward distributions, this implies, together with \eqref{eq:liminfbound}, that 
\[ \liminf_{n \to \infty} \frac{\expect{R_n}}{\log n} \geq  \sum_{\{k: \Delta_k > 0\}} \dfrac{L^{2/\alpha}}{4\Delta_k^{2/\alpha - 1}}. \]
For general $M$, one can modify this construction and consider arms' reward distributions to be Bernoulli scaled by the constant $M$ (i.e., equal to $M$ w.p. $p$ and $0$ otherwise). This completes the proof. 
\end{proof}