%\subsection{Experiments for Linear Bandit}
%\label{sec:exptsLinBandit}

We study the problem of optimizing the route choice of a human traveller using the Green light district (GLD) traffic simulation software \cite{GLDSim}. In this setup, a source-destination pair is fixed in a given road network. Learning proceeds in an online fashion, where the algorithm chooses a route in each round and the system provides the (stochastic) delay for the chosen route. The objective is to find the ``best'' path that optimizes some function of delay, while not exploring too much. While traditional algorithms minimize the expected delay, in this work, we consider the distorted value (as defined in \eqref{eq:cpt-lb}) as the performance metric. 

The gains/losses in CPT are usually w.r.t. a baseline \cite{tversky1992advances}.  Motivated by this observation, the distorted delay calculation in the experiments includes a baseline. In this setting, the goal is to minimize the excess delay, either in expectation or under the distortion measure. To elaborate, the r.v. considered is  $x\tr(\theta_{*} + N) -B$, where $B$ is the baseline delay, while the rest of the symbols are as defined in Section \ref{sec:model-linear}. 
For our experiments, we pick the average delay of a random routing algorithm as the baseline delay: a choice used previously in \cite{avineri12002sensitivity}. 
%Notice that Unlike the experimental setting in the previous revision, the distorted delays could be negative, since we incorporate the baseline delay.

We implement both OFUL and W-OFUL algorithms for this problem. 
OFUL algorithm finds a route that has the lowest excess delay, while WOFUL finds a route with the lowest distorted excess delay. The latter quantity is calculated using \eqref{eq:cpt-lb} for the r.v. mentioned above. 
The distorted excess delay is calculated using the following weight function: $w(p) = \frac{p^{0.1}}{{(p^{0.1}+ (1-p)^{0.1})}^{10}}.$
Since the weight function $w$ is non-linear, a closed-form expression for $\mu_x(\hat\theta_m)$ is not available, and we employ the empirical distribution scheme, described for the $K$-armed bandit setting (see Algorithm \ref{alg:cpt-estimator}), for estimating the weight-distorted value. For this purpose, we simulate $25000$ samples of the Gaussian distribution, as defined in \eqref{eq:linban-depnoise}. 


\begin{figure}
	%%%%%%% Lin-bandit figure goes here
	\centering
	\scalebox{0.8}{
		\tikzset{roads/.style={line width=0.1cm}}
		
		\tabl{c}{\begin{tikzpicture}
				\filldraw (0,0) node[color=white,font=\bfseries]{1} circle (0.4cm);
				\filldraw (1.5,0) node[color=white,font=\bfseries]{2} circle (0.4cm);
				\filldraw (3,0) node[color=white,font=\bfseries]{3} circle (0.4cm);
				
				\filldraw (0,-6.0) node[color=white,font=\bfseries]{7} circle (0.4cm);
				\filldraw (1.5,-6.0) node[color=white,font=\bfseries]{8} circle (0.4cm);
				\filldraw (3,-6.0) node[color=white,font=\bfseries]{9} circle (0.4cm);
				
				\filldraw (-1.5,-0-1.5) node[color=white,font=\bfseries]{10} circle (0.4cm);
				\filldraw (-1.5,-1.5-1.5) node[color=white,font=\bfseries]{11} circle (0.4cm);
				\filldraw (-1.5,-3-1.5) node[color=white,font=\bfseries]{12} circle (0.4cm);
				
				\filldraw (4.5,-0-1.5) node[color=white,font=\bfseries]{4} circle (0.4cm);
				\filldraw (4.5,-1.5-1.5) node[color=white,font=\bfseries]{5} circle (0.4cm);
				\filldraw (4.5,-3-1.5) node[color=white,font=\bfseries]{6} circle (0.4cm);
				
				\foreach \x in {0,1.5,3}
				{
					\draw[roads,] (\x,-0.25) -- (\x,-5.75);
					\draw[roads] (-1.25,-\x-1.5) -- (4.25,-\x-1.5);
				}
				
				\draw[dotted,blue,line width=0.1cm] (-1.25,-3-1.35)  -- (1.4,-3-1.35) -- (1.4, -1.6)-- (4.5, -1.6);% -- (3.15,-1.6) -- (4.25,-1.6);
				\draw[dotted,green,line width=0.1cm] (-1.15,-3-1.65)  -- (2.9,-3-1.65) -- (2.9,-1.5-1.62) -- (-0.1,-1.5-1.62) -- (-0.1,-1.4)--(4.12, -1.4);
				
				\node[draw=none] at  (-1.5,-3-1.5-1.5) (a1) {\textbf{\large\color{darkgreen}src}};
				\draw[thick,->] (a1) -- (-1.5,-3-2);
				\node[draw=none] at  (4.5,-1.5+1.5) (a2) {\textbf{\large\color{darkgreen}dst}};
				\draw[thick,->] (a2) -- (4.5,-1);
			\end{tikzpicture}\\[1ex]}}
	%\hspace{0.1em}
	\caption{Road network used for our experiments. The blue-dotted route, corresponding to Route {\color{blue} $7$} in Table \ref{tab:results}, has the lowest expected excess delay, while the green-dotted ``detour'' route, corresponding to Route {\color{blue} $8$} in Table \ref{tab:results}, has the lowest distorted excess delay.}
	\label{fig:road-net}
\end{figure}
\begin{table}[h]
	\caption{Expected and distorted excess delays of the routes from node $12$ to node $4$ in in Figure \ref{fig:road-net}. Route $7$ corresponds to the blue-dotted path in Figure \ref{fig:road-net}, while Route $8$ corresponds to the green-dotted detour in the same figure.}
	\label{tab:results}
	\centering
	\begin{tabular}{|c|c|c|}
		\toprule 
		& \textbf{Expected excess delay }& \textbf{Distorted excess delay}\\
		& \textbf{$x_i\tr\hat\theta_{\text{off}} - B$} &  \textbf{$\mu_{x_i}(\hat\theta_{\text{off}})$}\\\midrule
		Route $1$ & $-11.03 $ & $-0.20$\\\midrule
		Route $2$ & $5.76 $ & $-0.13$\\\midrule
		Route $3$ & $27.24 $ & $ 0.07$\\\midrule
		Route $4$ & $-15.61 $ & $-0.25$\\\midrule
		Route $5$ & $21.62 $ & $-0.01$\\\midrule
		Route $6$ & $-12.94$ & $-0.27$\\\midrule
		Route $7$ & $\bm{-21.67}$  & $-0.29$\\\midrule
		Route $8$ & $58.88 $ & $\bm{-0.45}$\\\midrule
		Route $9$ & $15.59$ & $-0.04$\\\midrule
		Route $10$ & $5.89$ & $-0.09$\\\bottomrule
	\end{tabular}
	\end{table}


For computing a performance benchmark to evaluate OFUL/W-OFUL, we perform an experiment to learn a linear (additive) relationship between the delay of a route and the delays along each of its component lanes. This is reasonable, considering the fact the individual delays along each lane stabilize when the traffic network reaches steady state, and the delay incurred along any route $x$ would be centered around $x\tr\theta$.
For learning the linear relationship, we simulate $100000$ steps of the traffic simulator to collect the delays along any route of the source-destination pair. Using these samples, we perform ridge regression to obtain $\hat\theta_{\text{off}}$, i.e., we solve
\begin{align}
	%\label{eq:regression}
	\hat\theta_{\text{off}} = \argmin_{\theta}  \dfrac{1}{2} \sum\limits_{i=1}^{\tau} (c_i - \theta\tr x_i)^2 + \lambda \left\| \theta \right\|^2,
\end{align}
where $c_i$ is the $i$th delay sample corresponding to a route choice $x_i,$ $\tau$ is the total number of delay samples for the source-destination pair considered, and $\lambda$ is a regularization parameter. It is well known that $\hat \theta_{\text{off}} =A^{-1} b$, where 
$A =  \sum\limits_{i=1}^{\tau} x_i x_i\tr + \lambda I_d$
and $b =  \sum\limits_{i=1}^{\tau} c_{i} x_i,$ where $I_d$ denotes the $d$-dimensional identity matrix, with $d$ set to the number of lanes in the road network considered. 

We set the various parameters of the problem as well as bandit routing algorithms - OFUL and W-OFUL, as follows: Regularization parameter $\lambda = 1$, confidence $\delta=0.99$, and norm bound $\beta = 400$.
We observed that both OFUL and W-OFUL algorithms learn the parameter underlying the linear relationship (Eq. \eqref{eq:linban-depnoise}) to a good accuracy. 
%For instance, the normalized difference $\left\| \theta_{\text{alg}} - \hat \theta_{\text{off}} \right\|^2/\left\|  \hat \theta_{\text{off}} \right\|^2 \approx 0.08$ for OFUL (resp. $0.07$ for W-OFUL) on the 3x3-grid network.  

%Figure~\ref{fig:linban-perf} contains the 3x3-grid network considered in our simulations, and the expected and weight-distorted values for OFUL and W-OFUL for the same network.
%Figure~\ref{fig:road-nets} shows a $11$-junction network used for our experiments. 
%Table \ref{tab:mixedup-results} presents similar results for the $11$-junction network. These values are calculated using a ridge regression-based estimate $\hat\theta_{\text{off}}$ of the true parameter $\theta$.

%The latter are obtained using a model similar to Eq. (8) in the manuscript, except that noise distribution is chosen to be uniform in $d$ dimensions, where $d$ is the number of lanes in the road network. The  
%The distorted excess delay is calculated using the following weight function: $w(p) = \frac{p^{0.1}}{{(p^{0.1}+ (1-p)^{0.1})}^{10}}.$ The rest of the parameters are as specified in Section 6.2 of the manuscript. 
%As specified in that section, we learn a linear relationship by running $500000$ steps in the GLD simulator, and collect sample delays under a random routing policy. The ridge regression solution, denoted by $\hat\theta_{\text{off}}$, is obtained using Eq. (20) in the manuscript. This ridge estimate is used as a benchmark for computing the expected and distorted delays.

Figure \ref{fig:road-net} shows the grid network used for our experiments, while Table \ref{tab:results} tabulates the expected and distorted excess delays for ten routes that connect node $12$ to node $4$. As illustrated in Figure \ref{fig:road-net}, the route with the lowest expected excess delay, i.e., the blue-dotted route, is one of the shortest paths connecting node $12$ to $4$. On the other hand, the route with lowest distorted excess delay, i.e., the green-dotted route, is a detour. 
The lower value of distorted excess delay for the longer green-dotted route
over the shorter blue-dotted route is
presumably due to the fact that the two routes differ in the variance
of the end-to-end delay. This leads to rare events being overweighted
by the weight function, ultimately making the former path more
appealing from the distorted excess delay viewpoint. The shorter route is preferred by OFUL algorithm, while the distortion-aware WOFUL algorithm finds the longer route.

% As expected, OFUL (resp. W-OFUL) algorithm recommends a route $x_{\text{\tiny OFUL}}$ (resp. $x_{\text{\tiny W-OFUL}}$) with minimum mean delay (resp. weight-distorted value). As shown in Figure \ref{fig:linban-perf}, $x_{\text{\tiny OFUL}}$ is the shortest path, while $x_{\text{\tiny W-OFUL}}$ involves a detour. The lower value of distorted value (delay) for the longer path
%preferred by W-OFUL, over the shorter path preferred by OFUL, is
%presumably due to the fact that the two routes differ in the variance
%of the end-to-end delay. This leads to rare events being overweighted
%by the weight function, ultimately making the former path more
%appealing to the distortion-conscious W-OFUL strategy.
%



%\begin{figure}
%%%%%%%% Lin-bandit figure goes here
%  \begin{tabular}{cc}
%\scalebox{0.65}{
%\tikzset{roads/.style={line width=0.1cm}}
%
%  \tabl{c}{\begin{tikzpicture}
%  \filldraw (0,0) node[color=white,font=\bfseries]{1} circle (0.4cm);
%  \filldraw (1.5,0) node[color=white,font=\bfseries]{2} circle (0.4cm);
%  \filldraw (3,0) node[color=white,font=\bfseries]{3} circle (0.4cm);
%
%  \filldraw (0,-6.0) node[color=white,font=\bfseries]{7} circle (0.4cm);
%  \filldraw (1.5,-6.0) node[color=white,font=\bfseries]{8} circle (0.4cm);
%  \filldraw (3,-6.0) node[color=white,font=\bfseries]{9} circle (0.4cm);
%  
%  \filldraw (-1.5,-0-1.5) node[color=white,font=\bfseries]{10} circle (0.4cm);
%  \filldraw (-1.5,-1.5-1.5) node[color=white,font=\bfseries]{11} circle (0.4cm);
%  \filldraw (-1.5,-3-1.5) node[color=white,font=\bfseries]{12} circle (0.4cm);
%  
%  \filldraw (4.5,-0-1.5) node[color=white,font=\bfseries]{4} circle (0.4cm);
%  \filldraw (4.5,-1.5-1.5) node[color=white,font=\bfseries]{5} circle (0.4cm);
%  \filldraw (4.5,-3-1.5) node[color=white,font=\bfseries]{6} circle (0.4cm);
%    
%  \foreach \x in {0,1.5,3}
%  {
%  \draw[roads,] (\x,-0.25) -- (\x,-5.75);
%  \draw[roads] (-1.25,-\x-1.5) -- (4.25,-\x-1.5);
%  }
%  
%  \draw[dotted,blue,line width=0.1cm] (4.25,-4.35) -- (3.15,-4.35) -- (3.15,-1.6) -- (4.25,-1.6);
%  \draw[dotted,green,line width=0.1cm] (3,-2.85) -- (1.65,-2.85) -- (1.65, -1.6) -- (3, -1.6);
%  
%  \node[draw=none] at  (4.5,-3-1.5-1.5) (a1) {\textbf{\large\color{darkgreen}src}};
%  \draw[thick,->] (a1) -- (4.5,-3-2);
%  \node[draw=none] at  (4.5,-1.5+1.5) (a2) {\textbf{\large\color{darkgreen}dst}};
%  \draw[thick,->] (a2) -- (4.5,-1);
%  \end{tikzpicture}\\[1ex]}}
%  &
%  %\hspace{0.1em}
%  \begin{tabular}{c|c|c}
%  \toprule 
%   & \textbf{Expected delay }& \textbf{Distorted delay }\\
%   & \textbf{$x_{\text{alg}}\tr\hat\theta_{\text{off}}$} &  \textbf{$\mu_{x_{\text{alg}}}(\hat\theta_{\text{off}})$}\\\midrule
%   OFUL & $51.71$ & $2.9$ \\\midrule
%   W-OFUL & $57.86$ & $6.75$\\\bottomrule
%  \end{tabular}
%  \end{tabular}
%  \caption{Expected delay $x_{\text{alg}}\tr\hat\theta_{\text{off}}$ and weight-distorted delay $\mu_{x_{\text{alg}}}(\hat\theta_{\text{off}})$ for OFUL and WOFUL algorithms on a 3x3-grid network. Here $\hat\theta_{\text{off}}$ is a ridge regression-based estimate of the true parameter $\theta$ (see \eqref{eq:linban-depnoise}) that is obtained by running an independent simulation, and $x_{\text{alg}}$ is the route to which the respective algorithm converges. $x_{\text{\tiny OFUL}}$ is the blue-dotted route in the figure, while $x_{\text{\tiny W-OFUL}}$ includes the green-dotted detour.}
%  \label{fig:linban-perf}
%\end{figure}
