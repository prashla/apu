\subsection{Proofs for K-armed bandits}
\subsubsection{Proof of Theorem \ref{thm:cpt-est}}
\label{sec:appendix-cpt-est}
The proof is very similar to that of \citep[Proposition 2]{prashanth2015cumulative}. The proof relies on the Dvoretzky-Kiefer-Wolfowitz (DKW) inequality, which gives finite-sample exponential concentration of the empirical distribution $\hat{F}_{k,m}$ around the true distribution $F_k$, as measured by the $||\cdot||_\infty$ function norm \citep[Chapter 2]{wasserman2006}.

\subsubsection{Proof of Theorem \ref{thm:BasicHolderRegretGap}}
\label{sec:appendix-gapdependentregret}
\begin{proof}
As in the case of regular UCB, we bound the number of times a sub-optimal arm is pulled using the technique from \citep{munos2014survey}. 

Let $k_*$ denote the optimal arm. Suppose that a sub-optimal arm $k$ is pulled at time $m$, which implies that 
$$\widehat \mu_{k,T_k(m-1)} - \gamma_{m,T_k(m-1)} \le  \widehat \mu_{k_*,T_{k_*}(m-1)} - \gamma_{m,T_{k_*}(m-1)}.$$
The B-value of arm $k$ can be smaller than that of $k_*$ {\em only if} one of the following three conditions holds:
\begin{align}
 \mu_* < \widehat \mu_{k_*,T_{k_*}(m-1)} &- \gamma_{m,T_{k_*}(m-1)}, \label{eq:optarmmeanoutside}\\
&\text{ or } \nonumber\\
\widehat \mu_{k,T_k(m-1)} + \gamma_{m,T_k(m-1)}& < \mu_k, \label{eq:karmmeanoutside}\\
&\text{ or } \nonumber\\
\quad \mu_k \,-\, 2 \,\gamma_{m,T_{k}(m-1)}& \le \mu_*. \label{eq:cond3}  
\end{align}
Note that condition (\ref{eq:cond3}) above is equivalent to requiring $T_{k}(m-1) \le  \dfrac{2^{2/\alpha -1} 3 (LM)^{2/\alpha}\log m}{\Delta_k^{2/\alpha}}$. 

Let $\kappa = \dfrac{2^{2/\alpha -1} 3 (LM)^{2/\alpha}\log n}{\Delta_k^{2/\alpha}} + 1$. When $T_{k}(m-1) \ge  \kappa$, i.e., the condition in \eqref{eq:cond3} does not hold, then either (i) arm $k$ is not pulled at time $m$, or 
(ii) \eqref{eq:optarmmeanoutside} or \eqref{eq:karmmeanoutside} occurs.
Thus, we have  
\begin{align}
T_k(n) \le &\kappa + \sum_{m=\kappa+1}^n I(I_m=k; T_k(m) > \kappa) \\
\le & \kappa + \sum_{m=\kappa+1}^n I(\eqref{eq:optarmmeanoutside} \text{ or }\eqref{eq:karmmeanoutside} \text{ occur}). \label{eq:tkn}
\end{align}
From Theorem \ref{thm:cpt-est}, we can upper bound the probability of occurence of \eqref{eq:optarmmeanoutside} as follows:
$$ P(\exists 1 < l < m \text{ such that } B_{l,T_{k_*}(l-1)}(k_*) < \mu_* ) \le \sum_{l=1}^m \frac{2}{m^{3}} \le \frac{2}{m^{2}}.$$
A similar argument works for \eqref{eq:karmmeanoutside}.  
Plugging the bounds on the events governed by \eqref{eq:optarmmeanoutside} and \eqref{eq:karmmeanoutside} into \eqref{eq:tkn} and taking expectations, we obtain
  \begin{align}
	\E[T_k(n)] \leq & \kappa + 2 \sum_{m=\kappa+1}^n \frac{2}{m^{2}}\\
	 \le & \dfrac{2^{2/\alpha -1} 3 (LM)^{2/\alpha}\log n}{\Delta_k^{2/\alpha}} + \left(1 + \dfrac{2\pi^2}{3} \right).
	\end{align}
  The claim follows by plugging the inequality above into the definition of expected regret, and using the fact that $\Delta_k \leq M$ for all arms $k$, as it follows that the weight-distorted cost of any distribution bounded by $M$ again admits an upper bound of $M$. 
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Proof of Corollary \ref{cor:BasicHolderRegretNoGap}}
\label{sec:appendix-gapindependentregret}
\begin{proof}
  We have, by the analysis in the proof of Theorem
  \ref{thm:BasicHolderRegretGap}, that for all $k$ with
  $\Delta_k > 0$,
  $$ \E[T_k(n)] \leq \dfrac{3(2LM)^{2/\alpha}\log n}{2\Delta_k^{2/\alpha}} +\left(1 + \dfrac{2\pi^2}{3} \right).
$$
  Thus, we can write
  \begin{align*}
    \E R_n &= \sum_{k} \Delta_k \; \E [T_k(n)] = \sum_{k} \left( \Delta_k \; \E [T_k(n)]^{\frac{\alpha}{2}}  \right) \left( \E [T_k(n)]^{1-\frac{\alpha}{2}} \right) \\
    &\leq \left( \sum_k \Delta_k^{2/\alpha} \; \E [T_k(n)] \right)^{\frac{\alpha}{2}} \left( \sum_k \E [T_k(n)] \right)^{1 - \frac{\alpha}{2}} \\
    &\text{(H\"{o}lder's inequality with exponents $2/\alpha$ and $2/(2-\alpha)$)} \\
    & \leq \left( \dfrac{3K(2LM)^{2/\alpha}\log n}{2} + K M^{2/\alpha}\left(1 + \dfrac{2\pi^2}{3} \right)  \right)^{\frac{\alpha}{2}} n^{\frac{2-\alpha}{2}}, \quad \quad \text{(using $\Delta_k^{2/\alpha} \leq M^{2/\alpha}$)}. 
  \end{align*}
  This proves the result.
\end{proof}

\subsubsection{Formal Regret Lower Bound} 
\label{sec:appendix-regretlowerbound}
%We define the {\em relative entropy} or {Kullback-Leibler divergence} $D(p || q)$ between a Bernoulli($p$) and a Bernoulli($q$) probability distribution as 
%\[ D(p || q) = p \log \frac{p}{q} + (1-p) \log \frac{1-p}{1-q}. \]
We will need the following definition. For two probability distributions $p$ and $q$ on $\mathbb{R}$, define the {\em relative entropy} or {\em Kullback-Leibler (KL) divergence} $D(p || q)$ between $p$ and $q$ as 
\[ D(p || q) = \int \log \left(\frac{dp}{dq}(x)\right) dp(x) \]
if $p$ is absolutely continuous w.r.t. $q$ (i.e., $q(A) = 0 \Rightarrow p(A) = 0$ for all Borel sets $A$), and $D(p || q) = \infty$ otherwise. For instance, if $p$ and $q$ are Bernoulli distributions with parameters $a \in (0,1)$ and $b \in (0,1)$, respectively, then a simple calculation gives that $D(p || q) = a \log \frac{a}{b} + (1-a) \log \frac{1-a}{1-b}$. 
We will often use $D(F || G)$ to mean the KL divergence between distributions represented by their cumulative distribution functions $F$ and $G$, respectively. 

\begin{theorem}
\label{thm:karmedlowerbd}[Regret lower bound]
Consider a learning algorithm for the $K$-armed weight-distorted bandit problem with the following property. For any weight distortion function $w:[0,1] \to [0,1]$, any set of cost distributions with costs bounded by $M$, any $a > 0$ and any arm $k$ with $\Delta_k > 0$, the expected number of plays of arm $k$ satisfies $\expect{T_k(n)} = o(n^a)$. \\
Then, for any constants $L > 0$ and $\alpha \in (0,1]$, there exists a weight function $w$ which is monotone increasing and $\alpha$-\holder continuous with constant $L$, and a set of cost distributions bounded by $M$, for which the algorithm's regret satisfies
\[  \liminf_{n \to \infty} \frac{\expect{R_n}}{\log n} \geq  \sum_{\{k: \Delta_k > 0\}} \dfrac{(LM)^{2/\alpha}}{4\Delta_k^{2/\alpha - 1}}. \]
\end{theorem}

\begin{proof}
We first show the result for costs bounded by $M = 1$, the extension to general $M$ follows. 

The key ingredient in the proof is the seminal lower-bound on the number of suboptimal arm plays derived by Lai and Robbins \citep{Lai+Robbins:1985}. Their result shows that for any algorithm that plays suboptimal arms only a sub-polynomial number of times in the time horizon, 
\[ \liminf_{n \to \infty} \frac{\expect{T_k(n)}}{\log n} \geq \frac{1}{D(F_k || F_{k^*})} \]
for any suboptimal arm $k$, where $k^*$ denotes the optimal arm. (Note that the argument at its core uses only a change-of-measure idea and the sub-polynomial regret hypothesis, and is thus unaffected by the fact that we measure regret by distorted, i.e., non-expected, costs.)

Using this along with the definition of regret, we get
\begin{align}
	\liminf_{n \to \infty} \frac{\expect{R_n}}{\log n} &= \liminf_{n \to \infty} \frac{\sum_{\{k: \Delta_k > 0\}} \E[T_k(n)] \Delta_k}{\log n} \nonumber \\
	&\geq \sum_{\{k: \Delta_k > 0\}} \Delta_k \cdot \liminf_{n \to \infty} \frac{\E[T_k(n)] }{\log n} \nonumber \\
	&\geq \sum_{\{k: \Delta_k > 0\}} \frac{\Delta_k}{D(F_k || F_{k^*})} \nonumber \\
	&= \sum_{\{k: \Delta_k > 0\}} \dfrac{(2LM)^{2/\alpha}}{\Delta_k^{2/\alpha - 1}} \cdot \frac{(\Delta_k/2LM)^{2/\alpha}}{D(F_k || F_{k^*})}. \label{eq:liminfbound}
\end{align}

(Recall that $\Delta_k = \mu_k - \mu_{k^*}$ represents the difference between the weight-distorted values of arms $k$ and $k^*$.) \\

We now design a set of cost distributions for the arms, for which the limiting property claimed in the theorem holds. In fact, we will show that it enough to design Bernoulli distributions for the arms' costs, i.e., arm $k$'s cost is Ber($p_k$), or equivalently, $F_k(x) = (1-p_k)\mathbf{1}_{[0,1)}(x) + \mathbf{1}_{[1,\infty)}(x)$. This gives a simple expression for the weight-distorted cost of any arm $k$ as $\mu_k = \int_0^1 w(p_k) dz = w(p_k)$, and, consequently, $\Delta_k = w(p_k) - w(p_{k^*})$, for any weight function $w: [0,1] \to [0,1]$ with $w(0) = w(1) - 1 = 0$.

Consider now a weight function $w:[0,1] \to [0,1]$ which is monotone increasing from $0$ to $1$, \holder continuous with any desired constant $L > 0$ and exponent $\alpha \in (0,1]$, and, moreover, satisfies the following ``\holder continuity property from below'': for some $\tilde{p} \in (0,1)$ (say, $\tilde{p} = 1/2$), $|w(p) - w(\tilde{p})| \geq L |p - \tilde{p}|^\alpha$. Such a weight function can always be constructed, e.g., for $\alpha = 1/2, L = 1$, take the function $w(x) = \frac{1}{2} - \frac{1}{\sqrt{2}}\sqrt{\frac{1}{2} - x}$ for $x \in [0, 1/2]$, and $w(x) = \frac{1}{2} + \frac{1}{\sqrt{2}}\sqrt{x - \frac{1}{2}}$ for $x \in (1/2, 1]$. (This is essentially formed by gluing together two inverted and scaled copies of the function $\sqrt{x}$, to make an S-shaped function infinitely steep at $x = 1/2$.)

For such a weight function, putting $p_{k^*} = \tilde{p} = 1/2$ and $p_k > p_{k^*}$, $k \neq k^*$, we can use the standard bound\footnote{This results from using $\log x \leq x - 1$.} 
\begin{align*} D(F_k || F_{k^*}) &= p_k \log \frac{p_k}{p_{k^*}} + (1-p_k) \log \frac{1-p_k}{1-p_{k^*}}  \\
&\leq \frac{(p_k - p_{k^*})^2}{p_{k^*}(1 - p_{k^*})} = 4{(p_k - p_{k^*})^2} \\
&\leq 4 \left( \frac{w(p_k) - w(p_{k^*})}{L} \right)^{2/\alpha} \quad \quad \mbox{(by the ``lower-\holder'' property of $w$)} \\
&= 4 \left( \frac{\Delta_k}{L} \right)^{2/\alpha}. 
\end{align*}
Since $M = 1$ for the Bernoulli family of cost distributions, this implies, together with \eqref{eq:liminfbound}, that 
\[ \liminf_{n \to \infty} \frac{\expect{R_n}}{\log n} \geq  \sum_{\{k: \Delta_k > 0\}} \dfrac{L^{2/\alpha}}{4\Delta_k^{2/\alpha - 1}}. \]
For general $M$, one can modify this construction and consider arms' cost distributions to be Bernoulli scaled by the constant $M$ (i.e., equal to $M$ with probability $p$ and $0$ otherwise). This completes the proof. 

\end{proof}

\subsubsection{Cumulative prospect theory (CPT) for K-armed bandit}
As remarked in the introduction, a popular approach using a weight function to distort probabilities is the so-called \textit{cumulative prospect theory} (CPT) of \citep{tversky1992advances}. %In addition to the weight function, a salient feature of CPT is to employ different utility functions, say $u^+$ and $u-$, to handle gains and losses, respectively. 
In the following, we extend the W-UCB algorithm to incorporate CPT-style utility functions. 

Define the CPT-value $\mu_k$ for any arm $k$ as follows: 
\begin{align}
\mu_k  := &\intinfinity w^+(P_k(u^+(X)>z)) dz - \intinfinity w^-(P_k(u^-(X)>z)) dz, \label{eq:cpt-general-appendix}
\end{align}
where $P_k(\cdot)$  is the probability of event $A$ for arm $k$,  $u^+,u^-:\R\rightarrow \R_+$ are the utility functions and  $w^+,w^-:[0,1] \rightarrow [0,1]$ are weight functions.

The optimal arm is one that maximizes the CPT-value, i.e., 
$\mu_* = \max_k \mu_k.$

At time instant $n$, let $X_{k,1},\ldots, X_{k,m}$ be $m$ samples from the cost distribution $F_k$ for arm $k$.
Order the samples in an ascending fashion using $u^+$ as \\$(u^+(X_{[1,n]}),\ldots ,u^+(X_{[m,n]})$. The first integral in \eqref{eq:cpt-general-appendix} is estimated by $\widehat \mu_{m,n}^+$, defined by
$$\widehat \mu_{k,m}^+:=\sum_{i=1}^{m-1} u^+(X_{[k,i]}) \left(w^+(\frac{m-i}{m})- w^+(\frac{m-i-1}{m}) \right).$$
Along similar lines, using $u^{-}$ obtain the following decreasing sample set: \\$(u^-(X_{[k,1]}),\ldots ,u^-(X_{[k,m]})$. The quantity $\widehat \mu_{k,m}^-$, defined below, serves as a proxy for the second integral in \eqref{eq:cpt-general-appendix}. 
$$\widehat \mu_{k,m}^-:=\sum_{i=1}^{m-1} u^-(X_{[k,i]}) \left(w^-(\frac{i}{m})- w^-(\frac{i-1}{m}) \right). $$
Now, we define an $m$-sample approximation to $\mu_k$ as follows:
\begin{align}
\widehat \mu_{k,m} =\widehat \mu_{k,m}^+ - \widehat \mu_{k,m}^-.
\label{eq:cpt-est-appendix}
\end{align}

We next provide a sample complexity result for the estimator $\widehat \mu_{k,m}$:
\begin{theorem}[\textbf{\textit{Sample complexity}}]
\label{thm:cpt-est-appendix}
Assume that the weights $w^+,w^-$ are \holder continuous with constant $H$ and order $\alpha \in (0,1]$, the utility functions $u^+, u^-$ are both continuous, bounded above by $M<\infty$, with $u^+$ increasing and $u^-$ decreasing. 
Then, for any $\epsilon >0$ and any $k\in \{1,\ldots,K\}$, we have 
$$
P(\left |\widehat \mu_{k,m} - \mu_k \right| > \epsilon ) \le \exp(-m\epsilon^{2/\alpha}/4H^2M^2).
$$
\end{theorem}
\begin{proof}
Follows from Proposition 2 in \citep{prashanth2015cumulative}.
\end{proof}


\subsection{Proofs for linear bandits}
We establish a few technical results in the following lemmas that are necessary for the proof of Theorem \ref{thm:linear-bandit-regret}. 
The first step in the proof of Theorem \ref{thm:linear-bandit-regret} is to bound the instantaneous regret $r_m$ at instant $m$, which is the difference in weight-distorted value of arm $x_*$ and the arm $x_{m}$ chosen by the algorithm. For bounding the instantaneous regret, it is necessary to relate the difference in weight-distorted values of $x_*$ and $x_{m}$ to the difference in their means, i.e., $(x_*\tr\theta - x_{m}\tr\theta)$, and Lemma \ref{lemma:cptdiff} provides this connection.

%In the following sections, we use the notation $F_X$, for a random variable $X$, to denote the cumulative distribution function of $X$.

%%%%%%%%%%%%%%%% From notes.tex
\subsubsection{Proof of Lemma \ref{lemma:cptdiff}}
% 
% Recall that, for a random variable $X$, the weight-distorted value with respect to a weight function $w: [0,1] \mapsto [0,1]$ is defined to be 
% \[ \mu_{w,X} \equiv \mu_X := \intinfinity w(\prob{X > z}) dz - \intinfinity w(\prob{-X > z}) dz.   \]
% 
% \begin{lemma}
% For any $a \in \R$, 
% \[ \mu_{X+a} = \mu_X + \int_{-a}^0 \left[ w(\prob{X > u}) + w(\prob{X < u})  \right] du. \]
% \end{lemma}

\begin{proof}
	\begin{align*}
		\mu_{X+a} &= \intinfinity w(\prob{X+a > z}) dz - \intinfinity w(\prob{-X-a > z}) dz \\
		&= \intinfinity w(\prob{X > z-a}) dz - \intinfinity w(\prob{-X > z+a}) dz \\
		&= \int_{-a}^\infty w(\prob{X > u}) du - \int_{a}^\infty w(\prob{-X > u}) du \\
		&= \int_{-a}^0 w(\prob{X > u}) du - \int_{a}^0 w(\prob{-X > u}) du + \mu_X \\
		&= \int_{-a}^0 w(\prob{X > u}) du + \int_{-a}^0 w(\prob{X < v}) dv + \mu_X.
	\end{align*}
	This proves the main claim. Since $w$ is bounded by 1, it is easy to infer that $|\mu_{X+a} - \mu_X| \leq 2|a|$.
\end{proof}

%%%%%%%%%%%%%% End: notes.tex

\begin{lemma}
\label{lemma:confidenceellipsoid}
Under the hypotheses of Theorem \ref{thm:linear-bandit-regret}, for any $\delta>0$, the following event occurs with probability at least $1-\delta$: $\forall m \ge 1$, $\theta$ lies in the set $C_m$ defined in Algorithm \ref{alg:WOFUL}, i.e., 
\begin{align*}
	C_{m}\! &= \!\left\{\theta \in \mathbb{R}^d: \norm{\theta - \hat{\theta}_m}_{A_m} \leq D_m \right\}, \quad \mbox{where} \\
	D_m \!&=\! \sqrt{2 \log \left(\frac{\det(A_m)^{1/2} \det(\lambda I_{d \times d})^{1/2}}{\delta} \right)} + \beta\sqrt{\lambda}, \\
	A_m &= \lambda I_{d \times d} + \sum_{l=1}^{m-1} \frac{x_l x_l\tr}{\norm{x_l}^2}, \\
	\hat{\theta}_m &= A_m^{-1} b_m, \quad \mbox{and} \quad b_m = \sum_{l=1}^{m-1} \frac{c_l x_k}{\norm{x_l}}.
\end{align*}
\end{lemma}
\begin{proof}
The proof is a consequence of \citep[Theorem 2]{abbasi2011improved}. Indeed, the hypotheses of the stated theorem are satisfied with the sub-Gaussianity parameter $R = 1$ since by (\ref{eq:linban-depnoise}), the stochastic cost $c_m$ normalised by $\norm{x_m}$ at each instant satisfies
\[ \frac{c_m}{\norm{x_m}} = \frac{x_m}{\norm{x_m}} \tr \theta + \frac{x_m}{\norm{x_m}} \tr N_m, \]
whose distribution is Gaussian with mean $\frac{x_m}{\norm{x_m}} \tr \theta$ and variance $1$, and which is thus sub-Gaussian with parameter $R = 1$. 
\end{proof}

\subsubsection*{Proof of Theorem \ref{thm:linear-bandit-regret}}

We first prove the following lemma which we will need in the proof of Theorem \ref{thm:linear-bandit-regret}. It relates the weight-distorted value $\mu_x(\theta)$ of the stochastic cost from an arm $x \in \X$ to the weight-distorted value of a translated standard normal distribution. 

\begin{lemma}[Weight-distorted value under scaling]
\label{lem:CPTscaling}
If $Z$ denotes a standard normal random variable, then for any arm $x$ and $\theta$, \[ \mu_{x}(\theta) = \norm{x} \cdot \mu_{Z + \frac{x\tr\theta}{\norm{x}}}, \]
where $\mu_{Y}$ denotes the weight-distorted value of a random variable $Y$.
\end{lemma}

\begin{proof}
Let $X$ denote the stochastic cost from arm $x$ with $\theta$ as the parameter; we have that $X$ is normal with mean $x\tr\theta$ and variance $\norm{x}^2$. With $\stdnormal$ being a standard normal $d$-dimensional vector and $\hat x = \frac{x}{\norm{x}}$, we can write
	\begin{align*}  
	 \mu_X &:= \intinfinity w(\prob{X > z}) dz - \intinfinity w(\prob{-X > z}) dz\\
	 & =  \intinfinity w(\prob{x_{m}\tr\theta + x_m\tr\stdnormal > z}) dz - \intinfinity w(\prob{-x_{m}\tr\theta - x_m\tr\stdnormal > z})dz\\
	 & = \intinfinity w(\prob{\hat x_{m}\tr\theta + \hat x_m\tr\stdnormal > \frac{z}{\norm{x_m}}}) dz - \intinfinity w(\prob{-\hat x_{m}\tr\theta - \hat x_m\tr\stdnormal > \frac{z}{\norm{x_m}}})dz\\
	 &= \norm{x_m}\left(\intinfinity w(\prob{\hat x_{m}\tr\theta + \hat x_m\tr\stdnormal > y}) dy - \intinfinity w(\prob{-\hat x_{m}\tr\theta - \hat x_m\tr\stdnormal > y})dy\right)\\
	 &= \norm{x_m} \mu_{Z + \hat x_{m}\tr\theta};
	\end{align*}
\end{proof}

\begin{proof}[Proof of Theorem \ref{thm:linear-bandit-regret}]
Let $r_m =  \mu_{x_{m}}(\theta) -\mu_{x_*}(\theta)$ denote the instantaneous regret incurred by the algorithm that chooses arm $x_{m}\in \X$ in round $m$.
Let $\arm_x$ (resp. $P_x$) denote the r.v. (resp. probability function) governing the rewards of the arm $x \in \X$.

Letting $\hat x_m = \frac{x_m}{\norm{x_m}}$ and $W$ to be a standard Gaussian r.v.,  we upper-bound the instantaneous regret $r_m$ at round $m$ as follows: 
\begin{align}
r_m &=  \mu_{x_{m}}(\theta) - \mu_{x_*}(\theta)  \nonumber\\
& \le \mu_{x_{m}}(\theta) - \mu_{x_m}(\tilde\theta_m) \label{eq:rm1}\\
& = \norm{x_m} \left( \mu_{W+ \hat x_{m}\tr\theta} - \mu_{W+ \hat x_{m}\tr\tilde\theta_m} \right) \label{eq:rm2}\\
& \le 2 \norm{x_m} \left| \hat x_{m}\tr(\theta-\tilde\theta_m) \right| \label{eq:rm3}\\
& = 2 \left| x_{m}\tr(\theta-\tilde\theta_m) \right| \label{eq:rm4}\\
& \le 2 \left(\left| x_{m}\tr(\hat\theta_{m} - \theta)\right|  + \left| x_{m}\tr(\tilde\theta_m - \hat\theta_{m})\right|\right)  \label{eq:rm5} \\
& = 2 \left(\norm{\hat\theta_{m} - \theta}_{A_m}  \norm{x_{m}}_{A_m^{-1}}  + \norm{\tilde\theta_m - \hat\theta_{m}}_{A_m}   \norm{x_{m}}_{A_m^{-1}} \right)  \label{eq:rm6} \\
& \le 4 w_m \sqrt{D_m}  \quad \text{whenever }\theta \in C_m, \label{eq:rm7}
\end{align} 
where $w_m = \sqrt{x_m\tr A_m^{-1}x_m}$ and $D_m$ is as defined in Algorithm \ref{alg:WOFUL}.
The inequalities above are derived as follows:
\begin{itemize}
	\item \eqref{eq:rm1} follows from the choice of $x_{m}$ and $\tilde \theta_m$ in Algorithm \ref{alg:WOFUL};
	\item \eqref{eq:rm2} follows from the scaling lemma (Lemma \ref{lem:CPTscaling});
	\item \eqref{eq:rm3} follows by applying the translation lemma (Lemma \ref{lemma:cptdiff}); 
	%with the r.v. $X = W + x_m\tr \theta$ 
	%(note that $x \mapsto \prob{X > x}$ is integrable over $[0, \infty)$ since $\eta_1$ is sub-Gaussian) 
	%and $a$ = 2, an upper bound on the absolute differences between the expected rewards of arms;
	\item \eqref{eq:rm4} follows from the definition of $\hat x_{m}$;
	\item \eqref{eq:rm6} is by the Cauchy-Schwarz inequality applied to the pair of dual norms $\norm{\cdot}_{A_m}$ and $\norm{\cdot}_{A_m^{-1}}$ on $\R^d$;
%	
%	follows by first noticing that 
%	\begin{align*}
%	\left| x_{m}\tr(\hat\theta_{m} - \theta)\right| &= \left| (\hat\theta_{m} - \theta)\tr A_m^{1/2} A_m^{-1/2} x_{m}\right|
%	 = \left| \left(A_m^{1/2}(\hat\theta_{m} - \theta)\right)\tr  A_m^{-1/2} x_{m}\right| \le \norm{A_m^{1/2}(\hat\theta_{m} - \theta)}\norm{A_m^{-1/2} x_{m}},
%	\end{align*}
%where the last inequality uses Cauchy-Schwarz; 
	\item \eqref{eq:rm7} follows from the definition of the confidence ellipsoid $C_m$ in Algorithm \ref{alg:WOFUL}, as $\hat\theta_{m}$, $\tilde \theta_m$ and $\theta$ belong to $C_m$. Note that Lemma \ref{lemma:confidenceellipsoid} guarantees that this is true with probability at least $1-\delta$.
\end{itemize}

The cumulative regret $R_n$ of WOFUL can now be bounded as follows. With probability at least $1-\delta$, 
\begin{align}
R_n &= \sum_{m=1}^n r_m \nonumber\\
& \le  \sum_{m=1}^n 4 \sqrt{D_m} w_m \nonumber \\
& \le 4 \sqrt{D_n}  \sum_{m=1}^n w_m \nonumber \\
& \le 4 \sqrt{D_n}  \left(n \sum_{m=1}^n w_m^2\right)^{1/2} \label{eq:Rn3}\\
& \le \sqrt{32 d n D_n \log n}. \label{eq:Rn4}
%& \le C (d \log (\lambda + nL/d))^{\alpha/2} n^{\frac{2-\alpha}{2}}.  \text{ for some } C<\infty. \label{eq:Rn4}
\end{align} 
Inequality \eqref{eq:Rn3} follows by an application of Cauchy-Schwarz inequality, while the inequality in \eqref{eq:Rn4} follows from (the standard by now) Lemma 9 in \citep{dani2008stochastic}, which shows that $\sum_{m=1}^n w_m^2 \le  2d \log n$. 
The claim follows.
\end{proof}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:




























