We consider the stochastic multi-armed bandit setting with a finite set of arms $\A = \lbrace 1, 2, \cdots, K \rbrace$.  Each arm $k$ is associated with a distribution $F_k$, whose parameters are unknown a priori to the agent. Let $\mu_k$ denote the weight-distorted reward of arm $k$ and $a^* = \argmax_k~\mu_k$ denote the optimal arm. Note that the quantity $\mu_k$, defined in \eqref{eq:cpt-intro}, can be seen to be equivalent to the following:
\begin{equation}
\mu_k  := \intinfinity w(1-F_k(z)) dz - \intinfinity w(F_k(-z)) dz, \label{eq:cpt-general}
\end{equation}
where $w:[0,1] \rightarrow [0,1]$ satisfies $w(0) =0$ and $w(1) = 1,$ as described earlier, is a weight function that distorts probabilities. The optimal arm is the one that maximizes the weight-distorted reward, i.e., 
$\mu_* = \max_k \mu_k.$ The optimal arm is not necessarily unique, i.e., there may exist multiple arms with the optimal weight-distorted reward $\mu_*$. For sake of simplicity, we assume that $\mu_1 > \mu_2 \geq \cdots \geq \mu_K$, i.e., arm 1 is the arm with the highest weight-distorted reward, which we call the unique optimal arm~\footnote{The ordering assumption is without loss of generality, and without uniqueness the problem can only get easier.}. For $k \geq 2,$ let $\Delta_k = \mu_1 - \mu_k$ denote the gap between the weight-distorted rewards of the optimal arm and an arm $k,$ and $\Delta_1 = \mu_1 - \mu_2\footnote{Generally, it is considered that $\Delta_1 = 0$ in the regret minimization literature. However, we require this notation, $\Delta_1 = \mu_1 - \mu_2,$  for the setting of best arm identification.}.$ 

In the following sections, we present the learning model and objective for $K$-armed bandits under the regret minimization and the best arm identification frameworks. 

\subsection{Regret minimization with weight-based distortions}
\label{sec:model-karmed-regret}
In each round $m=1,\ldots,n$, the algorithm pulls an arm $I_m \in \A = \{1,\ldots,K\}$ and obtains a sample reward from the distribution $F_{I_m}$ of arm $I_m$. The classic objective is to play (or pull) the arm whose expected reward is the highest. In this paper, we take a different approach inspired by non-expected utility  models and use the weight distorted-reward $\mu_k$ as the performance criterion for any arm $k$. 

We now define the cumulative regret $R_n$ as follows:
\begin{align}
R_n = n \mu_1 - \sum_{k=1}^K  T_k(n) \mu_k\, ,\label{eq:regret-karmed} 
\end{align}
where $T_k(n)= \sum_{m=1}^n I(I_m=k)$ is the number of times arm $k$ is pulled up to time $n$. The expected regret can be written as 
\[\E R_n = \sum_{k=2}^K \E[T_k(n)] \Delta_k.\] Note that this definition of regret arises from the interpretation that each arm $k$ is associated with a deterministic value $\mu_k$. The highest possible cumulative reward that can be accumulated in $n$ rounds is thus $n \mu_1$, while that accumulated by a given strategy is $\sum_{k=1}^K T_k(n) \mu_k$. Thus, the regret as defined above is a measure of  the rate at which a strategy converges to playing the optimal arm in the sense of weighted or distorted reward. Our goal is to design algorithms that have the lowest expected regret.

We remark that the regret performance measure, as defined above, is explicitly defined within a stochastic model for rewards. Thus, low-regret algorithms designed for the non-stochastic setting, e.g., EXP3 \citep{Auer02NonSto}, are not inherently suitable for this problem, as they do not factor in the distortion caused in (expected) reward. A similar observation holds for conventional stochastic bandit algorithms such as UCB, and algorithms sensitive to arm reward variances such as UCB-V \citep{AudMunSze09:UCBV} --  once weight distortion is incorporated, the algorithm can converge to an arm that is not weight-distorted value optimal. Thus, applying a variance-sensitive algorithm (like UCB-V) may still yield linear regret in the distorted setting.
\subsection{Best arm identification with weight-based distortions}
\label{sec:model-karmed-bestarm}
The bandit algorithm interacts with the environment as follows: 
In each round $m=1,2,\ldots$, the algorithm chooses an arm $I_m \in \{1,\ldots,K\}$ and receives an independent sample from the distribution corresponding to arm $I_m$.
The objective is to find the arm with the highest weight-distorted value by sequentially sampling the arms; we study this problem under the following popular variants:
\begin{description}
\item[Fixed Confidence :] Here, a confidence parameter $\delta \in (0,1)$ is given. At each round, an algorithm can either (a) stop and declare an arm as optimal, or (b) continue and choose an arm to sample. The algorithm must ensure that the arm declared upon stopping is indeed optimal with a probability of at least $(1 - \delta).$ An algorithm's performance is quantified by the (expected) number of samples it uses before it stops which we call the sample complexity of the algorithm.% and its performance is quantified by the number of rounds before it stops. 
\item[Fixed Budget :] Here, a budget of $n$ rounds is given, with the stipulation that the algorithm must declare an arm as optimal at the end of $n$ rounds. The algorithm's goal is to minimize the probability of declaring a sub-optimal arm as optimal.
\end{description}
Our goal is to design algorithms that take the lowest number of rounds and have the smallest probability of error under fixed confidence and fixed budget settings, respectively.