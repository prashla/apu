We now propose and study algorithms for the best arm identification problem for $K$-armed bandits under the fixed confidence and fixed budget settings. %We assume that the observations are bounded by $M > 0$ almost surely\footnote{This models a variety of settings where each arms' distributions are supported on a bounded set, e.g., the well-studied Bernoulli-armed bandit where $M = 1$.}.   
\subsubsection{Best arm identification with Fixed Confidence}  
We introduce the Weighted-Lower Upper Confidence Bound (W-LUCB) algorithm, which is a non-trivial adaptation of the well-known LUCB algorithm~\citep{kalyanakrishnan2012pac}, for best expected-value arm identification. Unlike the latter algorithm, W-LUCB incorporates weight-distorted reward estimates and confidence widths for arms to find the arm with the highest weight-distorted reward. 

Algorithm~\ref{alg:cpt-lucb} presents the pseudocode of W-LUCB.  A high-level description of the W-LUCB algorithm is as follows: In any round $m$, we calculate a Lower Confidence Bound (LCB) and Upper Confidence Bound (UCB) on the weight-distorted rewards of each arm, obtained by adding and subtracting a confidence term $\left( \gamma'_{(\cdot), (\cdot)} \right)$ to the weight-distorted reward estimate $\left( \hat{\mu}_{(\cdot), (\cdot)} \right)$ of the arm. Then, we find the arm, denoted $h_m$ in Algorithm \ref{alg:cpt-lucb}, with the highest weight-distorted reward estimate and its LCB.  Next, we find the arm, say $l_m$, which has the highest UCB among all arms excluding $h_m$. We sample from the distributions of $h_m$ and $l_m$, unless $LCB(h_m) > UCB(l_m)$, when the algorithm terminates to return $h_m$. This is justified because the weight-distorted reward of $h_m$ lies within its confidence interval with high probability and none of the other arms can possibly have a higher weight-distorted reward, since their confidence intervals do not intersect with that of $h_m.$ 
\begin{algorithm}[!h]  
\caption{W-LUCB}
\label{alg:cpt-lucb}
\begin{algorithmic}
\State {\bfseries Input:} $\delta \in (0, 1)$,  weight function $w$
\For{$m=1, 2, \dots K$}
\State Sample each arm once.
\EndFor
\State Let $T_k(m-1)$ denote the number of times the algorithm has chosen arm $k$ up to round~$m,$ $\widehat\mu_{k, T_k(m-1)}$ is the weight-distorted reward estimate given by Algorithm~\ref{alg:cpt-estimator}.
\State Calculate the following: 
\State ETop$_m = h_m = \arg \max\limits_{k \in \mathcal{A}} \widehat \mu_{k, T_k(m-1)}$
\State EBot$_m = \mathcal{A} \setminus$ ETop$_m$ 
\State $\gamma'_{m,T_k(m-1)} = LM \left[ \frac{1}{2T_k(m-1)} \log \left( \frac{m^4 K \left(\pi^2/3 + 1\right)}{\delta} \right) \right]^{\alpha/2}$
\State  $l_m = \arg \max\limits_{k \in EBot_m} \widehat \mu_{k, T_k(m-1)} + \gamma'_{m, T_k(m-1)}$,   
\While{$\left( \widehat \mu _{l_m, T_{l_m}(m-1)} + \gamma'_{m, T_{l_m}(m-1)} > \widehat \mu_{h_m, T_{h_m}(m-1)} - \gamma'_{m, T_{h_m}(m-1)} \right)$} 
\State Sample arms $h_m$ and $l_m$
\EndWhile 
%\For{$m=K+1, K+2, \dots$}
%\State  ETop$_m = h_m = \arg \max\limits_{k \in \mathcal{A}} \widehat \mu_{k, T_k(m-1)}$,
%where $T_k(m-1)$ denotes the number of times the algorithm has chosen arm $k$ up to round $m,$ and $\widehat\mu_{k, T_k(m-1)}$ is the weight-distorted reward estimate given by Algorithm \ref{alg:cpt-estimator} 
%\State  EBot$_m = \mathcal{A} \setminus$ ETop$_m$ 
%\State  $l_m = \arg \max\limits_{k \in EBot_m} \widehat \mu_{k, T_k(m-1)} + \gamma'_{m, T_k(m-1)}$, \\
%where  
%$\gamma'_{m,T_k(m-1)} = LM \left[ \frac{1}{2T_k(m-1)} \log \left( \frac{m^4 K \left(\pi^2/3 + 1\right)}{\delta} \right) \right]^{\alpha/2}$ is the confidence width
%\If{$\left( \widehat \mu _{l_m, T_{l_m}(m-1)} + \gamma'_{m, T_{l_m}(m-1)} < \widehat \mu_{h_m, T_{h_m}(m-1)} - \gamma'_{m, T_{h_m}(m-1)} \right)$}  
%\State  Terminate
%\Else
%\State  Sample $h_m$ and $l_m$
%\EndIf
%\EndFor
\State {\bfseries Output:} Return $h_m$.
\end{algorithmic}
\end{algorithm}
The main results concerning the W-LUCB algorithm are given below.
\begin{theorem}[\textbf{\textit{Correctness of W-LUCB}}]
\label{Thm:correctness}
Assume (A1)-(A2) and let $\delta \in (0, 1)$. The following holds for the W-LUCB algorithm: %$\mathbb{P} \left( h_m \ne 1 \right) < \delta.$
\begin{align*}
\mathbb{P} \left( \text{W-LUCB does not return optimal arm} \right) < \delta.
\end{align*}
\end{theorem}
\begin{proof}
Refer to Section~\ref{sec:proof-of-W-LUCB-correctness}.
\end{proof}
\begin{theorem}[\textbf{\textit{Sample complexity bound for W-LUCB}}]
\label{Thm:LUCB6}
Assume $(A1)-(A2),$ for a given $\delta \in (0,1)$, let $N^{CL}$ denote the number of samples used by W-LUCB before termination. Then, we have
\begin{equation*}
\mathbb{E} \left[ N^{CL} \right] = O \left( H_\alpha \log \frac{H_\alpha}{\delta} \right),\text{where} \,\,\, H_\alpha~\footnote{For the sake of notational convenience, we have suppressed the dependence of $H_\alpha$ on $L$ and $M$.} = \sum\limits_{i \in \mathcal{A}} \left[ \frac{2LM}{\Delta_i} \right]^\frac{2}{\alpha}.
\end{equation*}  
\end{theorem} 
\begin{proof}
We have followed the technique from~\citep{kalyanakrishnan2012pac} for proving this result. For a detailed proof, refer to Section~\ref{sec:proof-W-LUCB-complexity-bound}.
\end{proof}
Theorems \ref{Thm:correctness}--\ref{Thm:LUCB6} together imply that, for any given $\delta \in (0, 1),$ W-LUCB terminates after using $N^{CL}$ samples (i.e., after $N^{CL}/2$ rounds) and returns the optimal arm w.p. at least $(1-\delta).$ In the next result, we show that any algorithm requires $\Omega \left( H_\alpha \right)$ samples to find the best arm under the fixed confidence setting. Hence, we call $H_\alpha$ as the \emph{hardness measure} of the problem of finding the best arm with the fixed confidence.

The key principle behind the algorithm and its performance guarantees (Theorems \ref{Thm:correctness} and \ref{Thm:LUCB6}) is as follows: In any round, we design the LCB and UCB so that at any time, the probability that the weight-distorted reward of an arm lies below (resp. above) its UCB (resp. LCB) is overwhelming high. Note that, standard concentration of measure bounds for the sample mean as an estimator of the expected value (such as Hoeffding's inequality) do not help to control estimates of the more complex weight-distorted reward functional. Hence, we leverage a concentration inequality for the weight-distorted reward estimate of Algorithm \ref{alg:cpt-estimator} \citep{aditya2016weighted}, relying on the Dvoretzky-Kiefer-Wolfowitz (DKW) inequality for concentration of the empirical CDF around the true CDF \citep{massart1990tight, wasserman2006}.

The classic expected-value optimizing algorithm LUCB requires $\tilde{O} \left( \sum\limits_{i \in \mathcal{A}} 1/ \Delta_i^2 \right)$\footnote{$\tilde O(\cdot)$ is a variant of $O(\cdot)$ that ignores log-factors.} number of samples. In comparison, W-LUCB requires $\tilde{O} \left( \sum\limits_{i \in \mathcal{A}} \left( L/ \Delta_i\right)^{2/\alpha} \right)$ number of samples to find the arm with the highest weight-distorted reward. If $\alpha=1$, then the complexities match. However, a typical weight function used in the weight-distorted reward (see Figure \ref{fig:weight}) has $\alpha < 1$, leading to increased expected sample complexity for W-LUCB. 

A natural question to ask is whether the upper bound on the expected sample complexity of W-LUCB algorithm can be improved. We now present an algorithm-independent lower bound on the expected sample complexity that answers this question in the negative. 
\begin{theorem}[\textbf{\textit{Lower bound on sample complexity }}]
\label{Thm:FCSBLB}
Let $\delta \in (0, 1), \alpha \in (0,1)$ and $L \in (0, 2^{\alpha - 1} ].$ Let $Alg(\delta, K)$ denote the class of algorithms which output the optimal arm w.p. at least $(1-\delta)$ on any $K$-armed bandit problem. Then, there exists a monotonically increasing weight function that satisfies $(A1)$ with parameters $\alpha, L,$ and a set of arm distributions bounded by $M$, such that any algorithm $A \in Alg(\delta, K)$ satisfies 

%there exists weight function$w : [0, 1] \rightarrow [0, 1]$ with $w(0) = 0$ and $w(1) = 1$, which is monotonic increasing, H\"older continuous with parameters $L, \alpha,$ and a set of arm distributions bounded by $M$, such that for any algorithm $A$ with success probability at least $1-\delta$,
\begin{equation*}
\mathbb{E} \left[ N^A \right] \geq O \left( H_\alpha \log \left( \frac{1}{2.4 \delta} \right) \right),
\end{equation*} 
where $N^A$ is the round number at which algorithm $A$ terminates and $H_\alpha$ is as defined in Theorem \ref{Thm:LUCB6}. 
\end{theorem}
\begin{proof}
Note that, our proof uses an information theoretic lower bound given in~\citep{kaufmann2015complexity}. For a detailed proof, refer to Section~\ref{sec:proof-complexity-lower-bound}.
\end{proof}
Theorems~\ref{Thm:LUCB6} and~\ref{Thm:FCSBLB} imply that W-LUCB is optimal in expected sample complexity up to a factor of $\log \left( H_\alpha \right)$.
\subsubsection{Best arm identification with Fixed Budget}
\label{Sec:SBFB}
\begin{algorithm}[t]  
\caption{W-SR}
\label{alg:cpt-sr}
\begin{algorithmic}
\State {\bfseries Input:}  Budget $n$ of plays,  weight function $w$ 
\State {\bfseries Initialization:} Let $\mathcal{A}_1 = \mathcal{A},$ $\overline{\log} K = \frac{1}{2} + \sum\limits_{i=2}^K \frac{1}{i}$ and $n_0 = 0.$ 
\For{each phase $k = 1, 2, \cdots K-1 $}
\State Define $n_k$ as follows: $n_k = \Big\lceil \frac{1}{\overline{\log} K} \frac{n-K}{K+1-k} \Big\rceil$
\State Play each arm in $\mathcal{A}_k$ for $(n_k - n_{k-1})$ times 
\State Estimate weight-distorted rewards $\left( \widehat \mu_{i, (\cdot)} : i \in \mathcal{A}_k \right)$ for all arms in $\mathcal{A}_k$ using Algorithm~\ref{alg:cpt-estimator}
\State $I_k = \arg\min\limits_{i \in \mathcal{A}_k} \hat{\mu}_{i, (\cdot)}$
\State Set $\mathcal{A}_{k+1} = \mathcal{A}_k \setminus I_k$
\EndFor
\State {\bfseries Output:} Return $J_n = \mathcal{A}_K.$
\end{algorithmic}
\end{algorithm}
We begin by introducing the Weighted-Successive Rejects (W-SR) algorithm for the best arm identification problem with a fixed budget (Algorithm~\ref{alg:cpt-sr}), inspired by the Successive Rejects (SR) algorithm~\citep{audibert2010best}.  A high-level idea of the W-SR algorithm is as follows: We divide the given fixed budget into $K-1$ phases. In each phase, we play all available arms an equal number of times and eliminate the arm with the lowest weight-distorted reward estimate. Note that, the phase lengths are designed such that the optimal arm survives across all $(K-1)$ phases with overwhelming probability. Despite the phase lengths in both algorithms being the same, an arm that will get eliminated at the end of a phase in the W-SR algorithm depends on weight-distorted reward estimates of arms rather than empirical means of arms.

The following result provides an  upper bound on the probability of incorrect identification by W-SR. 
\begin{theorem}[\textbf{\textit{Probability of incorrect identification by W-SR}}]
\label{Thm:SR1}
Let $(A1)-(A2)$ hold. For a given budget $n$, the arm $J_n$ returned by the W-SR algorithm satisfies:
\begin{equation*}
\mathbb{P} \left( J_n \neq 1 \right) \leq \frac{K(K-1)}{2} \exp \left( - \frac{n-K}{\overline{\log} K} \frac{1}{M^{2/\alpha}} \frac{1}{H_{2, \alpha}} \right).
\end{equation*}
In the above, $\overline{\log} K = \frac{1}{2} + \sum\limits_{i=2}^K \frac{1}{i}$, $H_{2, \alpha} = \max\limits_{i\neq 1} i\left( \frac{L}{\Delta_i} \right)^{2/\alpha}$, where $\alpha$ is the \holder exponent, $L$ is the \holder constant and $M$ is a bound on the stochastic rewards from any arm's distribution (see (A1) and (A2)).
\end{theorem}
\begin{proof}
We have followed the proof technique from~\citep{audibert2010best} for proving this result. For a detailed proof, refer to Section~\ref{sec:proof-W-SR-error-upper-bound}.
\end{proof}
From the result in the theorem above, it is apparent that $O(H_{2, \alpha})$ number of samples would be necessary for the SR algorithm to correctly identify the best arm, with high probability. 
Further, the dependence on the underlying gaps in $H_{2, \alpha}$ is $\tilde{O}\left(1/\Delta_2^{2/\alpha}\right)$, while the corresponding complexity term in classic SR is $\tilde{O}\left(1/\Delta_2^2\right)$.  
In order to show that the dependence of W-SR on the underlying gaps is optimal, we proceed to present two lower bounds on the probability of incorrect identification. To that end, we construct $K$ transformations of a $K$-armed bandit, and show that any algorithm must return a sub-optimal arm with a sufficiently high probability on at least one of these transformations.  

We consider a $K$-armed Bernoulli bandit problem with $p_1 = 1/2, p_j \in [1/4, 1/2)$ for $2 \leq j \leq K$. Following the measure change technique from~\cite{carpentier2016tight}, we consider $K$ transformations of a $K$-armed bandit problem. 
%In the $i^{th}$ transformation, arm $i$'s distribution is modified such that it has the highest CPT-value. 
For $1 \leq i \leq K,$ the transformation-$i$ corresponds to a $K$-armed bandit with Bernoulli arm distributions with parameters $p_1, p_2, \cdots, p_{i-1}, 1 - p_i, p_{i+1}, \cdots p_K$. We denote transformation-$i$ as $MAB_i.$ It is easy to see that for a H\"{o}lder-continuous and monotonically increasing weight function $w(\cdot)$, the weight-distorted reward of a Bernoulli random variable with parameter $p$ is $w(p)$. Hence, arm $i$ is the optimal arm under $MAB_i.$ Let $\mathbb{P}_i$ and $\mathbb{E}_i$ denote the joint probability distribution and expectation under $MAB_{i},$ respectively. 

Let $\lbrace \Delta^i_k \rbrace_k$ denote the gaps between the optimal arm and arm $k$ under $MAB_i,$ which are defined as follows: 
\begin{align*}
&\Delta^i_k := 
\begin{cases}
1-p_i - p_k, \hspace{0.85cm} \text{for $i \neq k,$}\\
1/2 - p_2,  \hspace{1.25cm} \text{for $i=k=1,$} \\
1/2 - p_i, \hspace{1.25cm} \text{for $2 \leq i=k \leq K.$}
\end{cases}
\end{align*}
Let $H(i,\alpha) = \sum\limits_{1 \leq k \leq K, k\neq i} \left( \frac{L}{\Delta^i_k} \right)^{2/\alpha}$ and $H_{1,\alpha} = \max\limits_i H(i, \alpha).$ We call $H_{1, \alpha}$ the \emph{hardness measure} of the problem of finding the best arm under the fixed budget setting. 
Using arguments similar to that in Section 6.1 of \cite{audibert2010best}, it can be  verified that 
\begin{align}
H_{2, \alpha} \le H_{1, \alpha} \le 2 \log(K) H_{2, \alpha}.\label{eq:hrels}
\end{align}
In the following, we provide two lower bounds on the probability of returning a sub-optimal arm by any algorithm on one of the $K$ transformations described above.
\begin{theorem}[\textbf{\textit{Lower bound1 on probability of incorrect identification}}]
\label{Thm:FBSBLB1}
Let $n$ be the given budget, $\alpha~\in~(0,~1)$ and $L \in (0, 2^{\alpha - 1} ].$ Then, there exists a monotonically increasing weight function that satisfies $(A1)$ with parameters $\alpha, L,$ and arm distributions bounded by 1 such that any algorithm satisfies the following:
%Then, there exists a weight function $w : [0, 1] \rightarrow [0, 1]$ with $w(0) = 0,$ and $w(1) = 1,$ which is monotonic increasing, H\"older continuous with parameters $\alpha \in (0, 1)$ and $L \in (0, 2^{\alpha - 1} ]$ and arm distributions bounded by $M,$ such that any algorithm for the best arm identification problem under the fixed budget satisfies the following:
\begin{eqnarray*}
\max\limits_{1 \leq i \leq K} \mathbb{P}_i (J_n \neq i) \geq \frac{1}{6} \exp \left[ \frac{-60 n}{H_{1,\alpha}} - 2 \sqrt{n \log(6nK)} \right]. %\\
%\max\limits_{1 \leq i \leq K} \left( \mathbb{P}_i (J_n \neq i) \times \exp\left[ \frac{-60 n}{h^* H(i,\alpha)} - 2 \sqrt{n \log(6nK)} \right] \right) \geq \frac{1}{6}.
\end{eqnarray*}
\end{theorem}
\begin{proof}
Note that, our proof follows the technique from~\cite{carpentier2016tight}. Refer to Section~\ref{sec:proof-k-armed-bai-fb-lowerbound} for a detailed proof.
\end{proof}

{\color{blue} In the next result, we present an improved lower bound on the probability of incorrect identification, in the case of knowledge of the problem complexity is unknown an algorithm,  that matches with the upper bound of W-SR algorithm.

\begin{theorem}[\textbf{\textit{Lower bound2 on probability of incorrect identification}}]
\label{thm:bai_karmed_improved_lb}
Let $K > 1$, $a > 0 $, $\alpha \in (0, 1]$ and $L \in (0, 2^{\alpha - 1} ].$ Let $\mathbb{B}_a$ be the set of MAB problems with support of arm distributions lying in $[0, 1]$ and the problem complexity $H_\alpha$ bounded by $a$. For any MAB problem $G \in \mathbb{B}_a$, let $a^*(G)$ be the set of optimal arms and $H_\alpha(G)$ be the problem complexity of the corresponding problem $G.$ Let $\mathbb{P}_G(\cdot)$ denote the probability measure under the problem $G.$
\begin{itemize}
\item[(a)] If $n \ge a^2 (4 \log(6nK))/3600$, for any MAB algorithm that outputs an arm $J_n$ at time $n$, the following holds:
\begin{align}
\label{eq:bai_new_thm1}
\sup_{G \in \mathbb{B}_a} \mathbb{P}_G \left( J_n \notin a^*(G) \right) \geq \frac{1}{6} \exp\left( -120 \frac{n}{a} \right). 
\end{align}
\item[(b)] Furthermore, if $a \geq 2 (4K)^{2/\alpha}$ and $K \geq 2$, then for any MAB algorithm that outputs an arm $J_n$ at time $n$, the following holds:
\begin{align}
\label{eq:bai_new_thm2}
\sup_{G \in \mathbb{B}_a} \left[ \mathbb{P}_G \left( J_n \notin a^*(G) \right) \times \exp \left(  \frac{480 (1-2/\alpha)n}{\left[ (\log K)^{1-2/\alpha} - 2^{1-2/\alpha}  \right] H_\alpha(G)} \right) \right] \geq \frac{1}{6}. 
\end{align}
\end{itemize}
\end{theorem}
\begin{proof}
We prove this result with the help of Theorem~\ref{Thm:FBSBLB1}. See section~\ref{proof:bai_karmed_improved_lb} for a detailed proof.
\end{proof}

} 

%The lower bound in the theorem above implies that at least $O(H_{1,\alpha})$ samples are necessary to identify the best arm with high probability. On the other hand, the upper bound for W-SR algorithm in  Theorems~\ref{Thm:SR1} implies that SR finds the best arm in $O(H_{2,\alpha})$ samples. The lower and upper bounds together with \eqref{eq:hrels} suggest that W-SR is an order-optimal algorithm, in terms of the dependence on the underlying gaps of the problem.
The lower and upper bounds together with \eqref{eq:hrels} suggest that W-SR is an order-optimal algorithm.

%Please refer Section~\ref{sec:proofs-karmed} for proofs of Theorems~\ref{Thm:correctness}-\ref{Thm:FBSBLB1}.

