\begin{figure}[h]
\centering
% \includegraphics[scale=0.42]{a-typical-weight-function.eps}
\scalebox{0.75}{\begin{tikzpicture}
  \begin{axis}[width=10cm,height=6cm,legend pos=south east,
           grid = major,
           grid style={dashed, gray!30},
           xmin=0,     % start the diagram at this x-coordinate
           xmax=1,    % end   the diagram at this x-coordinate
           ymin=0,     % start the diagram at this y-coordinate
           ymax=1,   % end   the diagram at this y-coordinate
           axis background/.style={fill=white},
           ylabel={\large Weight $\bm{w(p)}$},
           xlabel={\large Probability $\bm{p}$},
%            legend pos=outer north east,
           ]
          \addplot[domain=0:1, red, thick,smooth,samples=1500] 
             {pow(x,0.61)/pow((pow(x,0.61) + pow(1-x,0.61)),1.63)};
             \addlegendentry{CPT weight};
                 \addplot[domain=0:1, black, thick, dashed]           {x};
                 \addlegendentry{Identity};
  \end{axis}
  \end{tikzpicture}}
\caption{A typical weight function that overweights low probabilities and underweights high probabilities. We have used  $w(p)= \frac{p^{0.61}}{(p^{0.61} + (1-p)^{0.61})^{1/0.61}}$, recommended by Tversky and Kahneman \citep{tversky1992advances}.}
\label{Fig:a-typical-weight-function}
\end{figure}

We begin by defining the CPT-value of a random variable (r.v.) $X$, a functional of its cumulative distribution function (CDF) $F_X(\cdot).$ Let $w:~[0,1]~\rightarrow~[0,1]$ be a (given) weight function such that $w(0) = 0$ and $w(1) = 1.$ The CPT-value $\mu_X$ for a r.v. $X$ is defined as
\begin{equation}
\label{eq:CPT1}
\mu_X = \int\limits_{0}^\infty w \left( \mathbb{P} \left[ X > z \right] \right) dz -\! \int\limits_{0}^\infty w \left( \mathbb{P} \left[ -X > z \right] \right) dz. 
\end{equation}
It is easy to see that, if we take $w(x) = x, \,\, \forall x \in [0,1],$ i.e., the identity function, then we get $\mu_X = \mathbb{E}(X).$ Hence, the definition~\eqref{eq:CPT1} generalizes standard expected value.
On the other hand, a non-trivial choice for the weight function can transform the probabilities in a non-linear manner leading to a significant difference between CPT-value and the classic expected value. Based on experiments involving humans, CPT suggests using a weight function which inflates low probabilities and deflates high probabilities, as illustrated in Figure \ref{Fig:a-typical-weight-function}. 

% \begin{algorithm}  
% \caption{CPT-value estimator}
% \label{alg:cpt-estimator}
% \begin{algorithmic}
% \State {\bfseries Input:} $l$ i.i.d samples, $\left( X_t \right)_{t=1}^l,$ drawn from $F_X(\cdot),$ weight function $w$
% \State Sort the samples in ascending order as follows: $X_{[1]} \leq X_{[2]} \leq \dots X_{[l_b]} \leq 0 \leq X_{[l_b+1]} \leq \dots \leq X_{[l]},$ where $l_b \in \lbrace 1, 2, \dots, l \rbrace.$   
% \State Let $ \hat{\mu}_{X, l}^+  := \sum\limits_{k=l_b+1}^l X_{[k]}\left[ w \left( \frac{l+1-k}{l} \right) - w \left( \frac{l-k}{l} \right) \right]$ and  
% \State $\hat{\mu}_{X, l}^-  := \sum\limits_{k=1}^{l_b} X_{[k]}\left[ w \left( \frac{k - 1}{l} \right) - w \left( \frac{k}{l} \right) \right].$ 
% \State $\text{Estimate of CPT-value of $X$}: \hat{\mu}_{X, l} = \hat{\mu}_{X, l}^+ - \hat{\mu}_{X, l}^-.$
% \State {\bfseries Output:} Return $\hat{\mu}_{X, l}$.
% \end{algorithmic}
% \end{algorithm}
% 
% For convenience, we briefly recall a CPT-value estimation scheme, originally proposed by Prashanth et al~\citep{prashanth2015cumulative}, as Algorithm~\ref{alg:cpt-estimator}. Note that, $\hat{\mu}_{X, l}^+$ and $\hat{\mu}_{X, l}^-$ are simply the empirical estimates of the first and second integrals in the definition of CPT-value \eqref{eq:CPT1}. Under a H\"older-continuity assumption on the weight function (see [A1] in Section~\ref{sec-k-armed-bandits}), the authors in \citep{prashanth2015cumulative} established that this estimator requires $O \left( 1/ \epsilon^{(2/\alpha)} \right)$ samples to estimate the CPT-value up to accuracy $\epsilon > 0,$ where $\alpha$ is the H\"older exponent of the weight function. 

% \todop{Handle this later}
% \subsubsection{Cumulative prospect theory (CPT) for K-armed bandit}
% As remarked in the introduction, a popular approach using a weight function to distort probabilities is the so-called \textit{cumulative prospect theory} (CPT) of \citep{tversky1992advances}. %In addition to the weight function, a salient feature of CPT is to employ different utility functions, say $u^+$ and $u-$, to handle gains and losses, respectively. 
% In the following, we extend the W-UCB algorithm to incorporate CPT-style utility functions. 
% 
% Define the CPT-value $\mu_k$ for any arm $k$ as follows: 
% \begin{align}
% \mu_k  := &\intinfinity w^+(P_k(u^+(X)>z)) dz - \intinfinity w^-(P_k(u^-(X)>z)) dz, \label{eq:cpt-general-appendix}
% \end{align}
% where $P_k(\cdot)$  is the probability of event $A$ for arm $k$,  $u^+,u^-:\R\rightarrow \R_+$ are the utility functions and  $w^+,w^-:[0,1] \rightarrow [0,1]$ are weight functions.
% 
% The optimal arm is one that maximizes the CPT-value, i.e., 
% $\mu_* = \max_k \mu_k.$
% 
% At time instant $n$, let $X_{k,1},\ldots, X_{k,m}$ be $m$ samples from the cost distribution $F_k$ for arm $k$.
% Order the samples in an ascending fashion using $u^+$ as \\$(u^+(X_{[1,n]}),\ldots ,u^+(X_{[m,n]})$. The first integral in \eqref{eq:cpt-general-appendix} is estimated by $\widehat \mu_{m,n}^+$, defined by
% $$\widehat \mu_{k,m}^+:=\sum_{i=1}^{m-1} u^+(X_{[k,i]}) \left(w^+(\frac{m-i}{m})- w^+(\frac{m-i-1}{m}) \right).$$
% Along similar lines, using $u^{-}$ obtain the following decreasing sample set: \\$(u^-(X_{[k,1]}),\ldots ,u^-(X_{[k,m]})$. The quantity $\widehat \mu_{k,m}^-$, defined below, serves as a proxy for the second integral in \eqref{eq:cpt-general-appendix}. 
% $$\widehat \mu_{k,m}^-:=\sum_{i=1}^{m-1} u^-(X_{[k,i]}) \left(w^-(\frac{i}{m})- w^-(\frac{i-1}{m}) \right). $$
% Now, we define an $m$-sample approximation to $\mu_k$ as follows:
% \begin{align}
% \widehat \mu_{k,m} =\widehat \mu_{k,m}^+ - \widehat \mu_{k,m}^-.
% \label{eq:cpt-est-appendix}
% \end{align}
% 
% We next provide a sample complexity result for the estimator $\widehat \mu_{k,m}$:
% \begin{theorem}[\textbf{\textit{Sample complexity}}]
% \label{thm:cpt-est-appendix}
% Assume that the weights $w^+,w^-$ are \holder continuous with constant $H$ and order $\alpha \in (0,1]$, the utility functions $u^+, u^-$ are both continuous, bounded above by $M<\infty$, with $u^+$ increasing and $u^-$ decreasing. 
% Then, for any $\epsilon >0$ and any $k\in \{1,\ldots,K\}$, we have 
% $$
% P(\left |\widehat \mu_{k,m} - \mu_k \right| > \epsilon ) \le \exp(-m\epsilon^{2/\alpha}/4H^2M^2).
% $$
% \end{theorem}
% \begin{proof}
% Follows from Proposition 2 in \citep{prashanth2015cumulative}.
% \end{proof}
