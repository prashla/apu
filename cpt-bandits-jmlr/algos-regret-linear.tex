In Algorithm \ref{alg:WOFUL}, we present a pseudocode of the W-OFUL algorithm for solving the problem described in Section \ref{sec:model-linear-regret}. Algorithm \ref{alg:WOFUL} follows the general template for linear bandit algorithms (cf. ConfidenceBall in \cite{dani2008stochastic} or OFUL in \cite{abbasi2011improved}), but deviates in the step in which an arm is chosen. Let $C_m$ be the confidence ellipsoid that is specified in Algorithm \ref{alg:WOFUL}, and $\mu_x(\theta)$ be the weight-distorted value defined in \eqref{eq:cpt-lb} for any arm $x \in \X$ and $\theta \in C_m$. It is worth noting that, in any round $m$ of the algorithm, the W-OFUL uses $\mu_x(\theta)$ as the decision criterion, whereas regular linear bandit algorithms use $x\tr\theta$.  Note that the ``in-parameter'' or arm-dependent noise model \eqref{eq:linban-depnoise} also necessitates modifying the standard confidence ellipsoid construction of \cite{abbasi2011improved} by rescaling with the arm size (the $A_m$ and $b_m$ variables in Algorithm \ref{alg:WOFUL}). For a positive semidefinite matrix $P$ and a vector $x$, we use the notation $\norm{x}_P = \sqrt{x\tr P x}$ to denote the Euclidean norm of $x$ weighted by $P$.  

%In particular,  in any round $m$ of the algorithm, W-OFUL uses $\mu_x(\theta)$ as the decision criterion for any arm $x \in \X$ and $\theta \in C_m$, where $\mu_x(\theta)$ is the weight-distorted value that is defined in \eqref{eq:cpt-lb} and $C_m$ is the confidence ellipsoid that is specified in Algorithm \ref{alg:WOFUL}. This is unlike regular linear bandit algorithms, which use  $x\tr\theta$ as the reward for any arm $x \in \X$ and $\theta \in C_m$.  Note that the ``in-parameter'' or arm-dependent noise model \eqref{eq:linban-depnoise} also necessitates modifying the standard confidence ellipsoid construction of \cite{abbasi2011improved} by rescaling with the arm size (the $A_m$ and $b_m$ variables in Algorithm \ref{alg:WOFUL}). For a positive semidefinite matrix $P$ and a vector $x$, we use the notation $\norm{x}_P = \sqrt{x\tr P x}$ to denote the Euclidean norm of $x$ weighted by $P$.  
\subsubsection*{Computational complexity of W-OFUL:}
W-OFUL can be implemented with the same computational complexity as OFUL when the number of arms is finite. In particular, the computationally intensive step in W-OFUL is the optimization of the weight-distorted value over an ellipsoid in the parameter space (the third line in the {\bf for} loop). This can be explicitly solved as follows. For a fixed $x \in \X$, we can let 
\[\bar \theta_{m,x} := \argmax\limits_{\theta' \in C_{m}} \mu_x(\theta') = \argmax\limits_{\theta' \in C_{m}} x\tr\theta' =  D_m A_m^{-1}x/\norm{x}_{A^{-1}} - \hat{\theta}_m.\] This is because the weight-distorted value is monotone under translation (see Lemma \ref{lemma:cptdiff} below). The reward-maximizing arm is thus computed as \[x_m = \argmax \{ \mu_{x_1}(\bar \theta_{m,1}), \ldots, \mu_{x_{|\X|}}(\bar \theta_{m, |\X|})\}.\]

%%%%%%%%%%%%%%% alg-custom-block %%%%%%%%%%%%
\algblock{UCBcompute}{EndUCBcompute}
\algnewcommand\algorithmicUCBcompute{\textbf{\em Confidence set computation}}
 \algnewcommand\algorithmicendUCBcompute{}
\algrenewtext{UCBcompute}[1]{\algorithmicUCBcompute\ #1}
\algrenewtext{EndUCBcompute}{\algorithmicendUCBcompute}

\algblock{RecoAndFeedback}{EndRecoAndFeedback}
\algnewcommand\algorithmicRecoAndFeedback{\textbf{\em Arm selection + feedback}}
 \algnewcommand\algorithmicendRecoAndFeedback{}
\algrenewtext{RecoAndFeedback}[1]{\algorithmicRecoAndFeedback\ #1}
\algrenewtext{EndRecoAndFeedback}{\algorithmicendRecoAndFeedback}

\algblock{StatsUpdate}{EndStatsUpdate}
\algnewcommand\algorithmicStatsUpdate{\textbf{\em Update statistics}}
 \algnewcommand\algorithmicendStatsUpdate{}
\algrenewtext{StatsUpdate}[1]{\algorithmicStatsUpdate\ #1}
\algrenewtext{EndStatsUpdate}{\algorithmicendStatsUpdate}

\algtext*{EndUCBcompute}
\algtext*{EndRecoAndFeedback}
\algtext*{EndStatsUpdate}

\begin{algorithm} 
\caption{W-OFUL}
\label{alg:WOFUL}
\begin{algorithmic}
\State {\bfseries Input:} regularization constant $\lambda \geq 0$, confidence $\delta \in (0,1)$, bound $\beta$\footnotemark, weight function~$w$.
\State {\bfseries Initialization:} $A_1=\lambda I_{d \times d}$ ($d \times d$ identity matrix), $b_1=0$, $\hat\theta_1 =0$.
  \For{$m = 1,2,\ldots n$}
%   \UCBcompute
  \State Set $C_{m} := \left\{\theta \in \mathbb{R}^d: \norm{\theta - \hat{\theta}_m}_{A_m} \leq D_m \right\}$ and \State $D_m := \sqrt{2 \log \left(\frac{\det(A_m)^{1/2} \; \lambda^{d/2} }{\delta} \right)} + \beta\sqrt{\lambda}$.
	%\State $\hat{\theta}_t := A^{-1} b$
%   \EndUCBcompute
	
%   \RecoAndFeedback       
  \State Let $ (x_{m}, \tilde \theta_{m}) := \argmax\limits_{(x,\theta') \in \X\times  C_{m}} \mu_x(\theta')$.
  
	%, where $\mu(x,\theta)$ is given by \eqref{eq:cpt-lb}
  \State Choose arm $ x_{m}$ and observe reward $r_m$.
%   \EndRecoAndFeedback     
  
% 	\StatsUpdate
			\State Update $A_{m+1} = A_m + \frac{x_{m}x_{m}\tr}{\norm{x_m}^2}$,
			\State $b_{m+1} = b_m+ \frac{r_m x_{m}}{\norm{x_m}}$, and
		  \State $\hat\theta_{m+1} = A_{m+1}^{-1} b_{m+1}$
% 	\EndStatsUpdate
  \EndFor
\end{algorithmic}
\end{algorithm}

The following results bound the regret of the W-OFUL algorithm under mild conditions on the arms' rewards distributions and for any non-linear weight function $w$ that is bounded in $[0,1]$ (unlike the K-armed setting, we do not impose a \holder continuity assumption on $w$). 
\begin{theorem}[\textbf{\textit{Regret bound for W-OFUL}}]
\label{thm:linear-bandit-regret} 
Consider a weight function $w : [0, 1] \rightarrow [0, 1]$ with $w(0) = 0,$ $w(1) = 1$ and 
$\forall x \in \X: x\tr \theta \in [-1,1]$, and $\norm{\theta}_2 \leq \beta$. Then, for any $\delta > 0$, the regret $R_n$ of W-OFUL, run with parameters $\lambda > 0$, $B$, $\delta$ and $w$,
  satisfies
$\mathbb{P}\left(R_n \le \sqrt{32 d n D_n \log n} \; \; \forall n \geq 1 \right) \ge 1-\delta.$
%Suppose that the weight function $w$ satisfies $0 \le w(p) \le 1$, $\forall p \in (0,1)$, and 
%$\forall x \in \X: x\tr \theta \in [-1,1]$, and $\norm{\theta}_2 \leq \beta$. Then, for any $\delta > 0$, the regret $R_n$ of W-OFUL, run with parameters $\lambda > 0$, $B$, $\delta$ and $w$,
  %satisfies
%$\mathbb{P}\left(R_n \le \sqrt{32 d n D_n \log n} \; \; \forall n \geq 1 \right) \ge 1-\delta.$
\end{theorem}
If, for all $x \in X$, $\norm{x}_2 \leq \ell$, then the quantity $D_n$ appearing in the regret bound above is $O\left( \sqrt{d \log \left( \frac{n\ell^2}{\lambda \delta}\right)}\right)$ \citep[Lemma 10]{abbasi2011improved}; thus, the overall regret is $\tilde{O}\left( d \sqrt{n} \right)$.

\footnotetext{The parameter $\beta$ in Algorithm \ref{alg:WOFUL} represents a known upper bound on the size of $\theta$, \emph{i.e.,} $\norm{\theta}_2 \leq \beta.$}

For the identity weight function $w(p) = p$, $0 \leq p \leq 1$ with $L = \alpha = 1$, we recover the stochastic linear bandit setting, and the associated $\tilde{O}\left( d \sqrt{n}\right)$ regret bound for linear bandit algorithms such as $\text{ConfidenceBall}_1$ and $\text{ConfidenceBall}_2$ \citep{dani2008stochastic}, OFUL \citep{abbasi2011improved}. Hence, the result above is a generalization of regret bounds for standard linear bandit optimization to the case where a non-linear
    weight function of the reward distribution is to be optimized from linearly parameterized observations. The distortion of the reward distribution via a weight function, rather interestingly, does \emph{not} impact the order of the bound on problem-independent regret, and we obtain $\tilde{O}\left( d \sqrt{n}\right)$ here, as well. 

\begin{remark}
A lower bound of essentially the same order as Theorem \ref{thm:linear-bandit-regret} (O$\left(d \sqrt{n}\right)$) holds for regret in (undistorted) linear bandits \citep{DanKakHay07:LinBandit}. One can show a similar lower bound argument with distortions, implying that the result of the theorem is not improvable (order-wise) across weight functions.
\end{remark}


\noindent\textbf{\textit{Linear bandits with arm-independent additive noise}}: An alternative to modeling ``in-parameter'' or arm-dependent noise \eqref{eq:linban-depnoise} is to have independent additive noise, i.e., $r_m := x_m\tr \theta + \eta_m$. This is a standard model of stochastic observations adopted in the linear bandit literature \citep{abbasi2011improved,dani2008stochastic}. The key difference here is that, unlike the setting in \eqref{eq:linban-depnoise}, the noise component $\eta_m$ does \emph{not} depend on the arm played $x_m$. In this case, Lemma \ref{lemma:cptdiff} below shows that $\mu_{X+a} \geq \mu_X$, i.e., the weight-distorted reward $\mu$ preserves order under translations of random variables. As a consequence of this fact, the W-OFUL algorithm reduces to the OFUL algorithm in the standard linear bandit setting with arm-independent noise.  The reduction is in the sense that both WOFUL and regular OFUL algorithms choose the same sequence of arms at time instants $1,\ldots,n$, assuming the same noise values $\eta_1,\ldots,\eta_n$. This is because the arm $x_m$ chosen at instant $m$ is impacted by the ordering of the arms and both weight-distorted value $\mu_x(\theta')$ and $x\tr\theta'$ result in the same order for the arms due to the aforementioned fact regarding translation for $\mu$.


% If $N$ is assumed to be iid Gaussian with unit variance per-component, this is equivalent to the conditional distribution of reward/cost being Gaussian with mean $x\tr\theta$ and variance $\norm{x}_2^2$. But now, it is unclear how to evaluate/compute CPTs of arbitrary Gaussians (translations do not work cleanly). 


% 

\begin{proof}[Proof sketch for Theorem \ref{thm:linear-bandit-regret}]
We upper-bound the instantaneous regret $ir_m$ as follows:
Letting $\hat x_m = \frac{x_m}{\norm{x_m}}$ and $\stdnormal$ to be a standard Gaussian r.v. in $d$ dimensions,  we have
\begin{align}
ir_m =  \mu_{x_*}(\theta_*) - \mu_{x_{m}}(\theta_*) \le \mu_{x_m}(\tilde\theta_m) - \mu_{x_{m}}(\theta_*) &= \norm{x_m} \left( \mu_{W + \hat x_{m}\tr\tilde\theta_m} - \mu_{W+ \hat x_{m}\tr\theta_*} \right) \label{eq:trm2}\\
& \le 2 \norm{x_m} \left| \hat x_{m}\tr(\tilde\theta_m - \theta_*) \right|, \label{eq:trm3}
\end{align} 
and the rest of the proof uses the standard confidence ellipsoid result that ensures $\theta_*$ resides in $C_m$ with high probability. 
 A crucial observation necessary to ensure \eqref{eq:trm2} is that, for any r.v. $X$ and any $a\in\R$, the difference in weight-distorted reward $\mu_{X+a} - \mu_X$ is a non-linear function of $a$ (see Lemma \ref{lemma:cptdiff} below). Thus, it is not straightforward to compute the weight-distorted reward after translation, and this poses a significant challenge in the analysis of W-OFUL for the arm-dependent noise model that we consider here. 

% The proof relies on the following lemma that characterizes the change in weight-distorted value of a r.v. $X$ when shifted by $a \in \R$.  

\begin{lemma}[weight-distorted reward under translation] 
\label{lemma:cptdiff}
Let\\ 
$\mu_X := \intinfinity w(\prob{X > z}) dz - \intinfinity w(\prob{-X > z}) dz.$ Then, for any $a \in \R$, we have 
\[\mu_{X+a} = \mu_X + \int_{-a}^0 \left[ w(\prob{X > u}) + w(\prob{X < u})  \right] du.\]
In addition, if the weight function $w$ is bounded above by $1$, then 
\begin{align*}
|\mu_{X+a} - \mu_X| \leq 2|a|, \text{ for any }a\in \R.
\end{align*}
\end{lemma}

\begin{proof}
Refer to Section~\ref{sec:weight-distortion-undert-translation}.
\end{proof}
The reader is referred to Section~\ref{sec:proofs-regert-linear} for a detailed proof of Theorem~\ref{thm:linear-bandit-regret}. 
\end{proof}

%%% arm-independent noise
%\subsection{Arm-independent noise setting}
%As before, the arms are given as the compact set $\X\subset\R^d$ and the learning game proceeds as in the arm-dependent noise setting. The difference here is that playing an arm $x_m \in \X$ in round $m$ results in a stochastic cost given by $r_m := x_m\tr \theta + \eta_m$. Note that, unlike the previous setting, the noise component $\eta_m$ does not depend on the chosen arm $x_m$. This is a standard model of stochastic observations adopted in the linear bandit literature \citep{abbasi2011improved,dani2008stochastic}. For the sake of simplicity, assume that $\eta_1, \eta_2, \ldots$ is an i.i.d. 
%% \footnote{Our analysis can be generalized to the case when the $\{\eta_m\}_{m \geq 1}$ forms a martingale difference sequence instead of being iid. However, this introduces additional notational complications -- in particular, the weight-distorted cost of an arm can change depending on the noise distribution in a given round, and we avoid this for the sake of simplicity.} 
% sequence of $1$-sub-Gaussian (and hence zero-mean) random variables.
%% \footnote{A random variable $X$ is $b$-sub-Gaussian if for all $t \in \mathbb{R}$, $\expect{e^{tX}} \leq e^{b^2 t^2 / 2}$ \citep{rivasplata2012subgaussian}. Equivalently, there exists $c \equiv c(b)$ such that for all $\lambda > 0$, $\prob{|X| > \lambda} \leq 2 e^{-c \lambda^2}$.} 
%% \footnote{We focus on
%%   $1$-sub-Gaussian noise for simplicity of exposition. All our results can be adapted (scaled) to the case when $\eta$ is
%%   $b$-sub-Gaussian for $b > 0$.}. 
%
%As a corollary to Lemma \ref{lemma:cptdiff}, it is easy to infer that for nonnegative $a$, we have that $\mu_{X+a} \geq \mu_X$, i.e., $\mu$ preserves order under translations of random variables. As a consequence of this fact, the WOFUL algorithm reduces to the LinUCB algorithm in the standard linear bandit setting where the noise in the stochastic costs do not depend on the played arm. The reduction is in the sense that both WOFUL and regular LinUCB algorithms choose the same sequence of arms at time instants $1,\ldots,n$, assuming the same noise values $\eta_1,\ldots,\eta_n$. This is because the arm $x_m$ chosen at instant $m$ is impacted by the ordering of the arms and both weight-distorted value $\mu_x(\theta')$ and $x\tr\theta'$ result in the same order for the arms due to the aforementioned fact regarding translation for $\mu$.
