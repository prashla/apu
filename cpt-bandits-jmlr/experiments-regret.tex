We describe two sets of experiments pertaining to the $K$-armed and linear bandit settings. 
For both problems, we take the $S$-shaped weight function $w(p) = \frac{p^\eta}{\left(p^\eta + (1-p)^\eta \right)^{1/\eta}}$, with $\eta = 0.61$, to model perceived distortions in cost/reward. 
\cite{tversky1992advances} observe that this distortion weight function is a good
fit to explain distorted value preferences among human beings.
\subsection{Experiments for $K$-armed Bandit}
\label{sec:exptsKarmed}
We study three stylized $2$-armed bandit problems which, in part, draw upon experiments carried out by~\cite{tversky1992advances} on human subjects in their studies of cumulative prospect theory. Figures \ref{fig:karmed} (a) and (b) describe each problem setting in detail (following the convention of this paper of modeling costs, ``\$$x$ w.p. $p$" is taken to mean a loss of \$$x$ suffered with a probability of $p$.)
%\begin{enumerate}[label={\bf Experiment \arabic*.}, leftmargin=2.5cm]
%	\item {\em Arm 1:} (\$50 w.p. 0.1, \$$0$ w.p. 0.9) {\bf vs.} {\em Arm 2:} \$7 w.p. 1. 
%	\item {\em Arm 1:} (\$500 w.p. 0.01, \$$0$ w.p. 0.99) {\bf vs.} {\em Arm 2:} (\$250 w.p. 0.02, \$$0$ w.p. 0.98).
%	\item {\em Arm 1:} (\$100 w.p. 0.01, \$$0$ w.p. 0.99) {\bf vs.} {\em Arm 2:} (\$10 w.p. 0.2, \$$0$ w.p. 0.8).
%\end{enumerate}

For the first problem, the weight function $w$ gives the distorted cost of arm 1 as \$$10.55$, much higher than its expected cost of \$$5$ and thus more expensive due to the deterministic Arm 2 with a (distorted and expected) cost of \$$7$. The distortion in costs thus shifts the optimal arm from Arm 1 to Arm 2, and an online learning algorithm must be aware of this effect in order to attain low regret with respect to choosing Arm 2. A similar pattern is true for the other problem involving truly stochastic arms -- weight distortion favors the arm with the higher cost in true expectation. 

We benchmark the cumulative regret of two algorithms -- (a) the well-known UCB algorithm \citep{auer2002finite}, and (b) W-UCB; the results are as in Figure \ref{fig:karmed}. In the experiments, UCB is not aware of the distorted weighting and hence suffers linear regret with respect to playing the optimal distorted arm, due to converging to essentially a `wrong' arm. On the other hand, the W-UCB algorithm, being designed to explicitly account for distorted cost perception, estimates the distortion using sample-based quantiles and exhibits significantly lower regret. 

\begin{figure}
\centering
  \begin{tabular}{cc}
  \hspace{-1em}
  \begin{subfigure}{0.5\textwidth}
  \label{fig:a}
 \tabl{c}{\scalebox{0.75}{\begin{tikzpicture}
      \begin{axis}[
	xlabel={Rounds},
	ylabel={Cumulative regret},
       clip mode=individual,grid,grid style={gray!30}]
      % UCB
      \addplot+[error bars/.cd,y dir=both,y explicit, every nth mark=10000] table [x=X,y=Y,y error=Y_error,col sep=comma] {karmed-UCB-Setting1.txt};
      % W-UCB
      \addplot+[error bars/.cd,y dir=both,y explicit, every nth mark=10000] table [x=X,y=Y,y error=Y_error,col sep=comma] {karmed-WUCB-Setting1.txt};
      \legend{UCB,W-UCB}
%       \errorband[darkgray!50!white, opacity=0.3]{karmed-WUCB-Setting1.txt}{0}{1}{2}
%       \addplot [thick, darkgray,mark=*,mark options={scale=0.75}] table [x index=0, y index=1,col sep=comma] {results/karmed-WUCB-Setting1.txt};
      \end{axis}
      \end{tikzpicture}}\\}
%  
%    \includegraphics[width=\textwidth]{Env2b-eps-converted-to.pdf}
  \end{subfigure}
  &
  \hspace{-1em}
  \begin{subfigure}{0.5\textwidth}
  \label{fig:b}
   \tabl{c}{\scalebox{0.75}{\begin{tikzpicture}
      \begin{axis}[
	xlabel={Rounds},
	ylabel={Cumulative regret},
       clip mode=individual,grid,grid style={gray!30}]
      % UCB
      \addplot+[error bars/.cd,y dir=both,y explicit, every nth mark=10000] table [x=X,y=Y,y error=Y_error,col sep=comma] {karmed-UCB-Setting2.txt};
      % W-UCB
      \addplot+[error bars/.cd,y dir=both,y explicit, every nth mark=10000] table [x=X,y=Y,y error=Y_error,col sep=comma] {karmed-WUCB-Setting2.txt};
      \legend{UCB,W-UCB}
%       \errorband[darkgray!50!white, opacity=0.3]{karmed-WUCB-Setting1.txt}{0}{1}{2}
%       \addplot [thick, darkgray,mark=*,mark options={scale=0.75}] table [x index=0, y index=1,col sep=comma] {results/karmed-WUCB-Setting1.txt};
      \end{axis}
      \end{tikzpicture}}\\}

%   \includegraphics[width=\textwidth]{Env3b-eps-converted-to.pdf}
  \end{subfigure}
  %&
  %\begin{subfigure}{0.33\textwidth}
  %\label{fig:c}
  %\includegraphics[width=0.9\textwidth]{Env4b-eps-converted-to.pdf}
  %\end{subfigure}
  \end{tabular}
\caption{Cumulative regret along with standard error from $100$ replications for the UCB and W-UCB algorithms for 2 stochastic $K$-armed bandit environments: 
% The graphs show sample mean cumulative regret for the specified number of rounds of the game, along with standard error bars, across $100$ runs of each algorithm for each horizon value. 
(a) {\em Arm 1:} (\$50 w.p. 0.1, \$$0$ w.p. 0.9) {\bf vs.} {\em Arm 2:} \$7 w.p. 1. (b) {\em Arm 1:} (\$500 w.p. 0.01, \$$0$ w.p. 0.99) {\bf vs.} {\em Arm 2:} (\$250 w.p. 0.03, \$$0$ w.p. 0.97).%
%(c) {\em Arm 1:} (\$100 w.p. 0.01, \$$0$ w.p. 0.99) {\bf vs.} {\em Arm 2:} (\$10 w.p. 0.2, \$$0$ w.p. 0.8). 
}
\label{fig:karmed}
\end{figure}

{\color{blue} In the following set up, we use a heuristic estimator for weight-distorted reward as $W(\textrm{empirical mean})$ for distributions with support $\subseteq$ $[0, 1].$  Note that our weight-distorted value estimate turns out to be $W(\textrm{empirical mean})$ for the Bernoulli distributions. We adapt the classic UCB~\citep{auer2002finite} policy to this set up by taking the above \textbf{W}(\textbf{E}mpirical mean) as a surrogate for the weight-distorted reward estimate and call the policy WE-UCB.   

\textbf{Setup(*).} We consider a simple two-armed stochastic bandit with arm distributions' as follows: 
\[
\text{Arm-1} =
\begin{cases}
  1  & \text{w.p.} \quad  0.1 \\
  0.2  & \text{w.p.} \quad 0.1 \\
  0  & \text{w.p.} \quad 0.8
\end{cases}
\quad \quad
\text{Arm-2} =
\begin{cases}
  1  & \text{w.p.} \quad  10^{-3} \\
  0.2  & \text{w.p.} \quad 0.8 \\
  0  & \text{w.p.} \quad 0.199
\end{cases}
\]
For convenience, we treat these arm distributions as corresponding to rewards and hence the optimal arm is defined as the arm with highest weight-distorted reward. In the following, we show the expected and weight-distorted values of both arms: 
\begin{table}[ht]
\centering % used for centering table
\begin{tabular}{c c c} % centered columns (4 columns)
\hline %inserts a horizontal line
Distribution & Expected value & Weight-distorted value \\ [0.5ex] % inserts table
\hline % inserts single horizontal line
Arm-1 & 0.12 & 0.2005 \\ % inserting body of the table
Arm-2 & 0.161 & 0.1327\\ [1ex] % [1ex] adds vertical space
\hline %inserts single line
\end{tabular}
\label{table:new_setup2} % is used to refer this table in the text
\end{table}

Thus, arm-2 is optimal with respect to expected value and arm-1 is optimal with respect to weight-distorted reward where the latter is calculated empirically using a very large number of i.i.d. samples. We now present the comparison of UCB, WE-UCB and W-UCB for a fixed time horizon in Figure~\ref{fig:setup_2_regret}. We fix the time horizon as $10^4$ and run the algorithms for 100 runs and averaged them to obtain the results. It is clear from Figure~\ref{fig:setup_2_regret} that our algorithm W-UCB (achieves logarithmic regret) outperforms both UCB and WE-UCB (both achieve linear regrets) in this simple setup. Hence, these results justify the need of our algorithms under the weight-distorted setup considered herein.   

\begin{figure}[htb]
\center{\includegraphics[scale=0.5]{setup_2_regret.eps}}
\caption{\label{fig:setup_2_regret} Comparison of UCB, WE-UCB and W-UCB for regret minimisation in the stochastic bandits.}
\end{figure}
}
 
\subsection{Experiments for Linear Bandit}
\label{sec:exptsLinBandit}

We study the problem of optimizing the route choice of a human traveller using the Green light district (GLD) traffic simulation software \citep{GLDSim}. In this setup, a source-destination pair is fixed in a given road network. Learning proceeds in an online fashion, where the algorithm chooses a route in each round and the system provides the (stochastic) delay for the chosen route. The objective is to find the ``best'' path that optimizes some function of delay, while not exploring too much. While traditional algorithms minimized the expected delay, in this work, we consider the distorted value (as defined in \eqref{eq:cpt-lb}) as the performance metric. 

\begin{figure}
%%%%%%% Lin-bandit figure goes here
  \begin{tabular}{cc}
\scalebox{0.65}{
\tikzset{roads/.style={line width=0.1cm}}

  \tabl{c}{\begin{tikzpicture}
  \filldraw (0,0) node[color=white,font=\bfseries]{1} circle (0.4cm);
  \filldraw (1.5,0) node[color=white,font=\bfseries]{2} circle (0.4cm);
  \filldraw (3,0) node[color=white,font=\bfseries]{3} circle (0.4cm);

  \filldraw (0,-6.0) node[color=white,font=\bfseries]{7} circle (0.4cm);
  \filldraw (1.5,-6.0) node[color=white,font=\bfseries]{8} circle (0.4cm);
  \filldraw (3,-6.0) node[color=white,font=\bfseries]{9} circle (0.4cm);
  
  \filldraw (-1.5,-0-1.5) node[color=white,font=\bfseries]{10} circle (0.4cm);
  \filldraw (-1.5,-1.5-1.5) node[color=white,font=\bfseries]{11} circle (0.4cm);
  \filldraw (-1.5,-3-1.5) node[color=white,font=\bfseries]{12} circle (0.4cm);
  
  \filldraw (4.5,-0-1.5) node[color=white,font=\bfseries]{4} circle (0.4cm);
  \filldraw (4.5,-1.5-1.5) node[color=white,font=\bfseries]{5} circle (0.4cm);
  \filldraw (4.5,-3-1.5) node[color=white,font=\bfseries]{6} circle (0.4cm);
    
  \foreach \x in {0,1.5,3}
  {
  \draw[roads,] (\x,-0.25) -- (\x,-5.75);
  \draw[roads] (-1.25,-\x-1.5) -- (4.25,-\x-1.5);
  }
  
  \draw[dotted,blue,line width=0.1cm] (4.25,-4.35) -- (3.15,-4.35) -- (3.15,-1.6) -- (4.25,-1.6);
  \draw[dotted,green,line width=0.1cm] (3,-2.85) -- (1.65,-2.85) -- (1.65, -1.6) -- (3, -1.6);
  
  \node[draw=none] at  (4.5,-3-1.5-1.5) (a1) {\textbf{\large\color{darkgreen}src}};
  \draw[thick,->] (a1) -- (4.5,-3-2);
  \node[draw=none] at  (4.5,-1.5+1.5) (a2) {\textbf{\large\color{darkgreen}dst}};
  \draw[thick,->] (a2) -- (4.5,-1);
  \end{tikzpicture}\\[1ex]}}
  &
  %\hspace{0.1em}
  \begin{tabular}{c|c|c}
  \toprule 
   & \textbf{Expected delay }& \textbf{Distorted delay }\\
   & \textbf{$x_{\text{alg}}\tr\hat\theta_{\text{off}}$} &  \textbf{$\mu_{x_{\text{alg}}}(\hat\theta_{\text{off}})$}\\\midrule
   OFUL & $51.71$ & $2.9$ \\\midrule
   W-OFUL & $57.86$ & $6.75$\\\bottomrule
  \end{tabular}
  \end{tabular}
  \caption{Expected delay $x_{\text{alg}}\tr\hat\theta_{\text{off}}$ and weight-distorted delay $\mu_{x_{\text{alg}}}(\hat\theta_{\text{off}})$ for OFUL and WOFUL algorithms on a 3x3-grid network. Here $\hat\theta_{\text{off}}$ is a ridge regression-based estimate of the true parameter $\theta$ (see \eqref{eq:linban-depnoise}) that is obtained by running an independent simulation, and $x_{\text{alg}}$ is the route to which the respective algorithm converges. $x_{\text{\tiny OFUL}}$ is the blue-dotted route in the figure, while $x_{\text{\tiny W-OFUL}}$ includes the green-dotted detour.}
  \label{fig:linban-perf}
\end{figure}

We implement both OFUL and W-OFUL algorithms for this problem. Since the weight function $w$ is non-linear, a closed-form expression for $\mu_x(\hat\theta_m)$ is not available, and we employ the empirical distribution scheme, described for the $K$-armed bandit setting (see Algorithm \ref{alg:cpt-estimator}), for estimating the weight-distorted value. For this purpose, we simulate $25000$ samples of the Gaussian distribution, as defined in \eqref{eq:linban-depnoise}. 

% \subsection{Setting the benchmark $\hat\theta_{\text{off}}$}
For computing a performance benchmark to evaluate OFUL/W-OFUL, we perform an experiment to learn a linear (additive) relationship between the delay of a route and the delays along each of its component lanes. This is reasonable, considering the fact the individual delays along each lane stabilize when the traffic network reaches steady state, and the delay incurred along any route $x$ would be centered around $x\tr\theta$.
% This is based on the assumption of a linear (additive) relationship between the delay of a route and the delays along each of its component lanes, in steady state. 
For learning the linear relationship, we simulate $100000$ steps of the traffic simulator to collect the delays along any route of the source-destination pair. Using these samples, we perform ridge regression to obtain $\hat\theta_{\text{off}}$, i.e., we solve
\begin{align}
%\label{eq:regression}
 \hat\theta_{\text{off}} = \argmin_{\theta}  \dfrac{1}{2} \sum\limits_{i=1}^{\tau} (c_i - \theta\tr x_i)^2 + \lambda \left\| \theta \right\|^2,
\end{align}
where $c_i$ is the $i$th delay sample corresponding to a route choice $x_i,$ $\tau$ is the total number of delay samples for the source-destination pair considered, and $\lambda$ is a regularization parameter. It is well known that $\hat \theta_{\text{off}} =A^{-1} b$, where 
$A =  \sum\limits_{i=1}^{\tau} x_i x_i\tr + \lambda I_d$
and $b =  \sum\limits_{i=1}^{\tau} c_{i} x_i,$ where $I_d$ denotes the $d$-dimensional identity matrix, with $d$ set to the number of lanes in the road network considered. 

% \subsection{Bandit routing algorithms}
We set the various parameters of the problem as well as bandit routing algorithms - OFUL and W-OFUL, as follows: Regularization parameter $\lambda = 1$, confidence $\delta=0.99$, norm bound $\beta = 400$ and the weight function $w(p) = \frac{p^{0.61}}{{(p^{0.61}+ (1-p)^{0.61})}^{\frac{1}{0.61}}}.$ %\\
%\begin{table}[h]
%\centering
%\begin{tabular}{|c|c|}
%\toprule
%\multirow{2}{*}{\textbf{Algorithm}} & \multirow{2}{*}{\textbf{Parameters}} \\ 
 %&  \\ \hline
%\textbf{OFUL} & Regularization parameter $\lambda = 1$, confidence $\delta=0.99$, norm bound $\beta = 400$ \\ \midrule
%\textbf{W-OFUL} & Regularization parameter $\lambda = 1$, confidence $\delta=0.99$, norm bound $\beta = 400$,\\
%& weight function $w(p) = \frac{p^{0.61}}{{(p^{0.61}+ (1-p)^{0.61})}^{\frac{1}{0.61}}}$  \\ \bottomrule
%\end{tabular}
%\end{table}
We observed that both OFUL and W-OFUL algorithms learn the parameter underlying the linear relationship (Eq. \eqref{eq:linban-depnoise}) to a good accuracy. For instance, the normalized difference $\left\| \theta_{\text{alg}} - \hat \theta_{\text{off}} \right\|^2/\left\|  \hat \theta_{\text{off}} \right\|^2 \approx 0.08$ for OFUL (resp. $0.07$ for W-OFUL) on the 3x3-grid network.  
\begin{table}
  \caption{Performance comparison of OFUL and W-OFUL algorithms on the $11$-junction network shown in Figure \ref{fig:road-nets}. The trend observed in Figure \ref{fig:linban-perf} continues on this network.}
  \label{tab:mixedup-results}
\centering
  \begin{tabular}{|c|c|c|}
  \toprule 
   & \textbf{Expected delay }& \textbf{Weight-distorted delay}\\
   & \textbf{$x_{\text{alg}}\tr\hat\theta_{\text{off}}$} &  \textbf{$\mu_{x_{\text{alg}}}(\hat\theta_{\text{off}})$}\\\midrule
   OFUL & $56.09$ & $9.74$ \\\midrule
   W-OFUL & $58.54$ & $22.21$\\\bottomrule
  \end{tabular}

\end{table}

 \begin{figure}
    \centering
    % \begin{tabular}{cc}
     %\begin{subfigure}{0.5\textwidth}
       % \includegraphics[width=2.8in,height=2in]{gld_3x3grid.png}
     %\caption{A 3x3-grid network} 
     %\label{fig:3x3gridgld}
      %\end{subfigure}
%&		
  %   \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=2.8in,height=2in]{mixedup.png}
    % \caption{A $11$-junction road network.} 
     %\label{fig:mixedupgld}
%\end{subfigure}
% \\
%      \begin{subfigure}{0.5\textwidth}
%         \includegraphics[width=3in,height=2in]{results/gld_3x3grid_closer.png}
%      \caption{Zoomed in view of the network in Figure \ref{fig:3x3gridgld}. Note: the source node is labeled $8$ and destination is labeled $6$.} 
%      \label{fig:3x3gridzoom}
% \end{subfigure}
% &		
%      \begin{subfigure}{0.5\textwidth}
%         \includegraphics[width=3in,height=2in]{results/mixedup_closer.png}
%      \caption{Zoomed in view of the network in Figure \ref{fig:mixedupgld}. Note: the source node is labeled $1$ and destination is labeled $4$.} 
%      \label{fig:mixedupzoom}
% \end{subfigure}
%\end{tabular}

\caption{Snapshot from GLD simulator of the 11-junction road network used for our experiments. The figure shows edge nodes that generate traffic, traffic lights with red-dots and  two-lane roads carrying cars.}
\label{fig:road-nets}
\end{figure}
Figure~\ref{fig:linban-perf} contains the 3x3-grid network considered in our simulations, and the expected and weight-distorted values for OFUL and W-OFUL for the same network.
Figure~\ref{fig:road-nets} shows a $11$-junction network used for our experiments. 
Table \ref{tab:mixedup-results} presents similar results for the $11$-junction network. These values are calculated using a ridge regression-based estimate $\hat\theta_{\text{off}}$ of the true parameter $\theta$. As expected, OFUL (resp. W-OFUL) algorithm recommends a route $x_{\text{\tiny OFUL}}$ (resp. $x_{\text{\tiny W-OFUL}}$) with minimum mean delay (resp. weight-distorted value). As shown in Figure \ref{fig:linban-perf}, $x_{\text{\tiny OFUL}}$ is the shortest path, while $x_{\text{\tiny W-OFUL}}$ involves a detour. The lower value of distorted value (delay) for the longer path
preferred by W-OFUL, over the shorter path preferred by OFUL, is
presumably due to the fact that the two routes differ in the variance
of the end-to-end delay. This leads to rare events being overweighted
by the weight function, ultimately making the former path more
appealing to the distortion-conscious W-OFUL strategy.


























