 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% JMLR 2014                               %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[twoside, 11pt]{article}
\usepackage{jmlr2e}
\usepackage{macros}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\ShortHeadings{Bandits with probabilistic distortions}{}

\title{Bandits to emulate human decision making: \\A cumulative prospect theory approach
\thanks{A preliminary version of this paper was published in \textit{AAAI 2016} - see \citep{aditya2016weighted}. In comparison to the conference version, this paper includes the best arm identification problem formulation as well as algorithms in addition to the regret minimization framework considered there, includes formal proofs of convergence for both regret and best arm settings, includes additional experiments and a revised presentation.}}
\author{\name Ravi Kumar Kolla \email ee12d024@ee.iitm.ac.in \\
       \addr Department of Electrical Engineering \\
       Indian Institute of Technology Madras, Chennai 600036, India
       \AND
 \name Prashanth L. A. \email prashla@cse.iitm.ac.in \\
       \addr Department of Computer Science and Engineering \\
       Indian Institute of Technology Madras, Chennai 600036, India
       \AND
       \name Aditya Gopalan \email aditya@iisc.ac.in \\
       \addr Department of Electrical Communication Engineering \\
       Indian Institute of Science, Bengaluru 560012, India
       \AND
       \name Krishna Jagannathan \email krishnaj@ee.iitm.ac.in \\
       \addr Department of Electrical Engineering \\
       Indian Institute of Technology Madras, Chennai 600036, India
       \AND
       \name Michael C. Fu \email mfu@umd.edu \\
       \addr Robert H. Smith School of Business \& Institute for Systems Research \\
       University of Maryland, College Park, MD 20742, USA
       \AND
       \name Steven I. Marcus \email marcus@umd.edu \\
       \addr Department of Electrical and Computer Engineering \& Institute for Systems Research \\ University of Maryland, College Park, MD 20742, USA
}

 \editor{}
\synctex=1
\begin{document}

\maketitle

\begin{abstract}
Motivated by models of human decision making proposed to explain commonly observed deviations from conventional expected value preferences, we formulate two stochastic multi-armed bandit problems with distorted probabilities on the reward distributions: the classic $K$-armed bandit and the linearly parameterized bandit settings. We consider the aforementioned problems in the regret minimization as well as best arm identification framework for multi-armed bandits. For the regret minimization setting in $K$-armed as well as linear bandit problems, we propose algorithms that are inspired by Upper Confidence Bound (UCB) algorithms, incorporate reward distortions, and exhibit sublinear regret. 
For the $K$-armed bandit setting, we derive an upper bound on the expected regret for our proposed algorithm, and then we prove a matching lower bound to establish the order-optimality of our algorithm. For the linearly parameterized setting, our algorithm achieves a regret upper bound that is of the same order as that of regular linear bandit algorithm called Optimism in the Face of Uncertainty Linear (OFUL) bandit algorithm, and unlike OFUL, our algorithm handles distortions and an arm-dependent noise model. For the best arm identification problem in the $K$-armed bandit setting, we propose algorithms, derive guarantees on their performance, and also show that these algorithms are order optimal by proving matching fundamental limits on performance. For best arm identification in linear bandits, we propose an algorithm and establish sample complexity guarantees. Finally, we present simulation experiments which demonstrate the advantages resulting from using distortion-aware learning algorithms in a vehicular traffic routing application. 
\end{abstract}

\begin{keywords}
Multi-armed bandits, weight-distorted reward, confidence bounds, best arm identification, regret minimization.
\end{keywords}

\section{Introduction}
\input{intro}

%\section{Preliminaries}
%\label{sec:prelims}
%\input{prelims}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{K-armed bandit models}
\label{sec:model-karmed}
\input{model-karmed}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{K-armed bandit algorithms}
\label{sec:algos-karmed}
\input{algos-regret-karmed}

\subsection{Best arm identification with weight-based distortions}
\label{sec:algos-karmed-bestarm}
\input{algos-bestarm-karmed}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Linear bandit models}
\label{sec:model-linear}
\input{model-linear}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Linear bandit algorithms}
\label{sec:algos-linear}
%In this section, we first present algorithms for linear bandits under the regret minimization setting. Then, we present algorithms for linear bandits under the best arm identification setting. 
In this section, we present algorithms and their performance guarantees for linear bandits under the regret minimization and best arm identification settings. 

\subsection{Regret minimization with weight-based distortions}
\label{sec:algos-linear-regret}
\input{algos-regret-linear}

\subsection{Best arm identification with weight-based distortions}
\label{sec:algos-linear-bestarm}
\input{algos-bestarm-linear}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Numerical experiments for regret minimization setting}
\label{sec:experiments-regret}
\input{experiments-regret}

\section{Numerical experiments for best arm identification setting}
\label{sec:experiments-bestarm}
\input{experiments-bestarm}


\section{Proofs for K-armed bandits}
\label{sec:proofs-karmed}
In this section, we present the detailed proofs of analytical results presented in Section~\ref{sec:algos-karmed}. 
% \subsection{Proof for sample complexity of weight-distorted reward estimator}
% \subsubsection{Proof of Theorem \ref{thm:cpt-est}}
% \label{sec:appendix-cpt-est}

\subsection{Proofs for regret minimization setting}
\input{proofs-regret-karmed}

\subsection{Proofs for best arm identification setting: Fixed confidence}
\input{proofs-bestarm-karmed}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Proofs for linear bandits setting}
\label{sec:proofs-linear}
Now, we present the detailed proofs of all technical results presented in Section~\ref{sec:algos-linear}. 
\subsection{Proofs for regret minimization setting}
\label{sec:proofs-regert-linear}
\input{proofs-regret-linear}

\subsection{Proofs for best arm identification setting}
\input{proofs-bestarm-linear}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Conclusions}
\label{sec:conclusions}
We incorporated weight-distortion based criteria -- a generalization of expected value -- into regret minimization and best arm identification problems in $K$-armed and linear bandit settings. For both settings, we proposed algorithms and derived upper bounds on their performance. We also showed order-optimality of our proposed algorithms by providing matching lower bounds. In numerical experiments, we compared our algorithms with existing algorithms that deal with the expected reward setting, and showed that our algorithms outperform the existing algorithms. Therefore, our distortion-aware bandit algorithms are likely to be useful in aiding human decision making under uncertainty in real-life applications.  %Moving forward, it is of interest to study the general online reinforcement learning problem with weight-distorted reward metrics. Existing algorithms for expected value maximization such as UCRL \citep{JakschOA10} and PSRL \citep{OsbRusVan13:more} could be adapted for this purpose. Other interesting directions include considering contextual versions of the reward-distorted bandit problem, and perceived distortions in the observations.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \bibliographystyle{plainnat}
\bibliography{cpt_refs}

\end{document}

